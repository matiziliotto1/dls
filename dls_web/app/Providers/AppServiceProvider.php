<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    // public function register()
    // {
    //     $this->app->bind('path.public',function(){
    //         return 'http://dls.com.pe/app';
    //          return '/home1/dlscompe/public_html/app'; //Esta es la posta
    //     });
    // }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
