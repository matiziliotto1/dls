<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DeletePreferencial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'preferencial:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'El comando eliminara aquellos tipos de cambio preferenciales en los que ya hayan pasado mas de 30 minutos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $minutos_30 = 30*60;
        $preferenciales = DB::select('select * from tipocambio_aux');

        $hora_actual = date('H');
        $minuto_actual = date('i');
        $segundos_actual = date('s');
        
        $segundos_totales = $hora_actual*3600 + $minuto_actual*60 + $segundos_actual;

        foreach($preferenciales as $preferencial){
            $hora_seteado = substr($preferencial->hora_seteado, 0, 2);
            $minuto_seteado = substr($preferencial->hora_seteado, 3, 2);
            $segundo_seteado = substr($preferencial->hora_seteado, 6, 2);

            $segundos_totales_seteado = $hora_seteado*3600 + $minuto_seteado*60 + $segundo_seteado;

            $diferencia = $segundos_totales - $segundos_totales_seteado;

            if($diferencia > $minutos_30){
                $user = User::find($preferencial->id_user);
                $user->regdate = null;
                $user->timestamp = null;
                $user->previous_visit = null;
                $user->save();

                $deleted = DB::delete('delete from tipocambio_aux where id='.$preferencial->id);
            }
        }
    }
}
