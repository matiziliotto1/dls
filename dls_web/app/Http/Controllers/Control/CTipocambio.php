<?php

namespace App\Http\Controllers\Control;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CTipocambio extends Controller{
    public function ftipocambio() {
    	$tc=\App\Modelo\Tipocambio::all()->last();
    	return $tc;
    }
    public function ltipocambio() {
        $tc=\App\Modelo\Tipocambio::all();
        return $tc;
    }
    public function stipocambio(Request $r){
    	if(!is_null(Auth::User()->usuario_id)){
	    	$tc=new \App\Modelo\Tipocambio();
	    	$tc->compra=$r->Input('compra');
	    	$tc->venta=$r->Input('venta');
	    	$tc->created_by=Auth::User()->username;
	    	$tc->save();
    		return $tc;
    	}else{
    		return redirect('usuario');
    	}
    }
}
