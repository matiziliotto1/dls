<?php

namespace App\Http\Controllers\Control;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CCuentabancaria extends Controller{
    public function scuentasbancaria(Request $r){
    	if(!is_null(Auth::User()->usuario_id)){
        $cb=new \App\Modelo\Cuentabancaria();
        $cb->usuario_id=Auth::User()->usuario_id;
        $cb->banco_id=$r->input('ba');
        $cb->tipocuenta_id=$r->input('ti');
        $cb->moneda_id=$r->input('mo');
        $cb->nrocuenta=$r->input('nu');
        $cb->nrocuentacci=$r->input('nucci');
        $cb->alias=$r->input('al');
        $cb->cuentapropia=$r->input('cp');

        //Agregue estos nuevos para cuando la cuenta no es propia
        $cb->nombre=$r->input('nombre');
        $cb->tiposdocumento_id=$r->input('tipo_doc');
        $cb->nro_documento=$r->input('numero_doc');

        if($r->input('autorizo_deposito') == "on"){
          $cb->autorizo_deposito = 1;
        }

        $cb->save();
        return redirect('cuentasbancarias');

         //Antes se retornaba a una vista especifica para ver las cuentas bancarias
         // return redirect('lcuentasbancaria');
      
    	}else{
    		return redirect('usuario');
    	}
    }
    public function ucuentasbancaria(Request $r,$i){
      if(!is_null(Auth::User()->usuario_id)){
        $cb=\App\Modelo\Cuentabancaria::find($i);
        $cb->usuario_id=Auth::User()->usuario_id;
        $cb->banco_id=$r->input('ba');
        $cb->tipocuenta_id=$r->input('ti');
        $cb->moneda_id=$r->input('mo');
        $cb->nrocuenta=$r->input('nu');
        $cb->nrocuentacci=$r->input('nucci');
        $cb->alias=$r->input('al');
        $cb->cuentapropia=$r->input('cp');

        //Agregue estos nuevos para cuando la cuenta no es propia
        $cb->nombre=$r->input('nombre');
        $cb->tiposdocumento_id=$r->input('tipo_doc');
        $cb->nro_documento=$r->input('numero_doc');

        if($r->input('autorizo_deposito') == "on"){
          $cb->autorizo_deposito = 1;
        }
        else{
          $cb->autorizo_deposito = null;
        }

        $cb->save();
        return redirect('cuentasbancarias');

        //Antes se retornaba a una vista especifica para ver las cuentas bancarias
        // return redirect('lcuentasbancaria');
      }else{
        return redirect('usuario');
      }
    }
    public function lcuentasbancaria(){
    	$ls=\App\Modelo\Cuentabancaria::with("tipo","banco","moneda")->where("usuario_id",Auth::User()->usuario_id)->get();
    	return view('admin.lcuentasbancarias',compact("ls"));
    }
    public function dcuentasbancaria($i){
      $ls=\App\Modelo\Cuentabancaria::find($i);
      $ls->delete();
      return "Eliminado Correctamente";
    }
    public function fcuentasbancaria($i){
      $ls=\App\Modelo\Cuentabancaria::find($i);
      return view("admin.ucuentasbancarias",compact("ls"));
    }
}
