<?php

namespace App\Http\Controllers\Control;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;

class CUsuario extends Controller{
    public function susuario(Request $r) {
    	$us=new \App\Modelo\Usuario();
		$us->tiposdocumento_id=$r->input('td');
		$us->nrodocumento=$r->input('nd');
		$us->pais_id=$r->input('pa');
		$us->primernombre=$r->input('pn');
		$us->segundonombre=$r->input('sn');
		$us->primeroapellido=$r->input('ap');
		$us->segundoapellido=$r->input('am');
		$us->fecnacimiento=$r->input('fn');
		$us->paisdireccion_id=$r->input('pad');
		$us->departamento_id=$r->input('dep');
		$us->provincia_id=$r->input('pro');
		$us->distrito_id=$r->input('dis');
		$us->direccion=$r->input('dir');
		$us->ocupacion_id=$r->input('oc');
		$us->personaexpuesta=$r->input('pe');
		$us->personal=1;
		$us->empresa=0;
		$us->save();

		$user=\Auth::User();
		$user->userid=$r->input('cel');
		$user->actkey=$r->input('cel1');
		$user->user_home_path=$r->input('cel2');
		$user->userlevel=1;
		$user->firstname=$us->primernombre;
		$user->lastname=$us->primeroapellido;
		$user->usuario_id=$us->usuario_id;
		$user->save();

    	return redirect("home");
    }
    public function uusuario(Request $r,$i) {
    	$us=\App\Modelo\Usuario::find($i);
    	$us->tiposdocumento_id=$r->input('td');
		$us->nrodocumento=$r->input('nd');
		$us->primernombre=$r->input('pn');
		$us->segundonombre=$r->input('sn');
		$us->primeroapellido=$r->input('ap');
		$us->segundoapellido=$r->input('am');
		$us->fecnacimiento=$r->input('fn');
		$us->direccion=$r->input('dir');
		$us->departamento_id=$r->input('dep');
		$us->provincia_id=$r->input('pro');
		$us->distrito_id=$r->input('dis');
		$us->ocupacion_id=$r->input('oc');
		$us->personaexpuesta=$r->input('pe');
		$us->save();

		$user=\Auth::User();
		$user->userid=$r->input('cel');
		$user->actkey=$r->input('cel1');
		$user->user_home_path=$r->input('cel2');
		$user->firstname=$us->primernombre;
		$user->lastname=$us->primeroapellido;
		$user->save();
    	return "Actualizado Correctamente";
    }
    function lusuario(){
    	$ls=\App\Modelo\Usuario::with("tiposdocumento","pais","paisdireccion","ocupacion","cuenta","user")->where('usuario_id', "!=", 1)->get();
    	return view("admin.lusuario",compact("ls"));
    }
    function pusuario($id){
    	$us=\App\User::where("usuario_id",$id)->first();
    	if(!is_null($us)){
    		return view("admin.datos",compact("us"));
    	}else{
    		return "Usuario Eliminado";
    	}
    }
    function dusuario($id){
		$us=User::where("usuario_id",$id)->first();
		$us->userlevel=!$us->userlevel;
		$us->save();
		return $us->usuario_id;
    }
    function ausuario(Request $r,$id){
    	$us=\App\User::find($id);
		$us->password=bcrypt($r->ps);
		$us->save();
		return "Actualizado Correctamente";
    }
    function putipo($id){
    	$us=\App\User::where("usuario_id",$id)->first();
    	if(!is_null($us)){
    		return view("admin.putipo",compact("us"));
    	}else{
    		return "Usuario Eliminado";
    	}
    }
    function uutipo(Request $r,$id){
		$us=\App\User::find($id);
		$us->regdate=$r->Input("regdate");
		$us->timestamp=$r->Input("timestamp");
		$us->previous_visit=$r->Input("previous_visit");
		$us->save();
		
		DB::insert('insert into tipocambio_aux (compra, venta, id_user, hora_seteado) values ('.$r->Input("timestamp").', '.$r->Input("previous_visit").', '.$id.', "'.date('H:i:s').'")');

		return "Actualizado Correctamente";
	}
	
	
	public function susuario_empresa(Request $r) {
		$us=new \App\Modelo\Usuario();
		
		$us->ruc=$r->input('nr');
		$us->razon_social=$r->input('rz');
		$us->giro_negocio=$r->input('gn');

		$us->primernombre=$r->input('pn');
		$us->segundonombre=$r->input('sn');
		$us->primeroapellido=$r->input('ap');
		$us->segundoapellido=$r->input('am');
		$us->tiposdocumento_id=$r->input('td');
		$us->nrodocumento=$r->input('nd');

		$us->paisdireccion_id=$r->input('pad');
		$us->departamento_id=$r->input('dep');
		$us->provincia_id=$r->input('pro');
		$us->distrito_id=$r->input('dis');
		$us->direccion=$r->input('dirf');
		$us->telefono=$r->input('tel');
		$us->correo_electronico=$r->input('correo');

		$us->primernombre_c=$r->input('pnc');
		$us->segundonombre_c=$r->input('snc');
		$us->primerapellido_c=$r->input('apc');
		$us->segundoapellido_c=$r->input('amc');
		$us->tiposdocumentoc_id=$r->input('tdc');
		$us->nrodocumento_c=$r->input('ndc');
		$us->telefono_c=$r->input('telc');

		$us->ocupacion_id=11; //Sino que le pongo?
		$us->pais_id=1;//Se lo pongo por defecto a Peru
		$us->personal=0;
		$us->empresa=1;

		$us->save();

		$user=\Auth::User();
		/*
		$user->userid=$r->input('cel');
		$user->actkey=$r->input('cel1');
		$user->user_home_path=$r->input('cel2');
		*/

		$user->userlevel=1;
		$user->firstname=$us->primernombre;
		$user->lastname=$us->primeroapellido;
		$user->usuario_id=$us->usuario_id;

		$user->save();

    	return redirect("home");
	}
	
	public function uusuario_empresa(Request $r,$i) {
		$us=\App\Modelo\Usuario::find($i);

		$us->ruc=$r->input('nr');
		$us->razon_social=$r->input('rz');
		$us->giro_negocio=$r->input('gn');

		$us->primernombre=$r->input('pn');
		$us->segundonombre=$r->input('sn');
		$us->primeroapellido=$r->input('ap');
		$us->segundoapellido=$r->input('am');
		$us->tiposdocumento_id=$r->input('td');
		$us->nrodocumento=$r->input('nd');

		$us->departamento_id=$r->input('dep');
		$us->provincia_id=$r->input('pro');
		$us->distrito_id=$r->input('dis');
		$us->direccion=$r->input('dirf');
		$us->telefono=$r->input('tel');
		$us->correo_electronico=$r->input('correo');

		$us->primernombre_c=$r->input('pnc');
		$us->segundonombre_c=$r->input('snc');
		$us->primerapellido_c=$r->input('apc');
		$us->segundoapellido_c=$r->input('amc');
		$us->tiposdocumentoc_id=$r->input('tdc');
		$us->nrodocumento_c=$r->input('ndc');
		$us->telefono_c=$r->input('telc');

		$us->personal=0;
		$us->empresa=1;
		
		$us->save();

		$user=\Auth::User();
		/*
		$user->userid=$r->input('cel');
		$user->actkey=$r->input('cel1');
		$user->user_home_path=$r->input('cel2');
		*/
		$user->firstname=$us->primernombre;
		$user->lastname=$us->primeroapellido;
		$user->save();
    	return "Actualizado Correctamente";
    }
	
}
