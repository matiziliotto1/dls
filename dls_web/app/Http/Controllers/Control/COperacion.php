<?php

namespace App\Http\Controllers\Control;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Operacion;
use App\Modelo\Usuario;
use \App\Notifications\MOperacion;
use App\User;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class COperacion extends Controller{
    public function soperacion(Request $r){
        if(!is_null(Auth::User()->usuario_id)){
            $op=new \App\Modelo\Operacion();
			$op->cuentabancariae_id=$r->Input("baa");
			$op->cuentabancariad_id=$r->Input("cuenta");
			$op->monto=$r->Input("monto");
			$op->moneda_id=$r->Input("moe");
			$op->tmoneda_id=$r->Input("mor");
			$op->cuentabancariat_id=$r->Input("bar");
			
            
            if($r->hasFile("vou")){
				$op->voucher=rand(1000000, 100000000).$r->File('vou')->getClientOriginalName();
                $r->File('vou')->move(public_path().'/assets/voucher/',$op->voucher);;
            }
            
            if($r->hasFile("vou2")){
				$op->voucher2=rand(1000000, 100000000).$r->File('vou2')->getClientOriginalName();
                $r->File('vou2')->move(public_path().'/assets/voucher/',$op->voucher2);;
            }
            
            if($r->hasFile("vou3")){
				$op->voucher3=rand(1000000, 100000000).$r->File('vou3')->getClientOriginalName();
                $r->File('vou3')->move(public_path().'/assets/voucher/',$op->voucher3);;
            }
            
            if($r->hasFile("vou4")){
				$op->voucher4=rand(1000000, 100000000).$r->File('vou4')->getClientOriginalName();
                $r->File('vou4')->move(public_path().'/assets/voucher/',$op->voucher4);;
			}

			$op->cambio=$r->Input("cambio");
			$op->taza=$r->Input("compra");
			$op->usuario_id=Auth::User()->usuario_id;
			$op->save();

            $us=array('email' => Auth::User()->email,'name' => Auth::User()->username);

            $usuario = Usuario::find(Auth::User()->usuario_id);
            $dni = $usuario->nrodocumento;

            $data = array(
                'operacion' => \App\Modelo\Operacion::with("cuentabancariat","cuentabancariae","cuentabancariad","monedae","monedad","usuario")->find($op->operacion_id),
                'admin' => 1,
                'email_cliente' => Auth::User()->email,
                'dni' => $dni,
                'mensaje' => 'HA HECHO UNA TRANSACCIÓN'
            );
            $admin=\App\User::first();
            $rem=$admin->email;

            //TODO: se estan enviando mal los mails por lo que dijo
            // Lo que debe haber pasado es que se le enviaron los correos a katiusta@abejareina y lo esperaban en otro correo.
            Mail::send('mail.operacion', $data, function ($message) use($us,$rem) {
                $message->from($us['email'],$us['name']);
                $message->to($rem,'Operaciones DLS Perú')->subject('Operaciones DLS Perú');
            });

            /*
            $data['admin'] = 0;
            Mail::send('mail.operacion', $data, function ($message) use($us,$rem) {
                $message->from($rem,'Operaciones DLS Perú')->subject('Operaciones DLS Perú');
                $message->to($us['email'], $us['name']);
            });
            */

            return redirect('historial');
        }else{
            return redirect('usuario');
        }
    }
    public function uoperacion($i) {
        $op=\App\Modelo\Operacion::find($i);
        $op->estado=$op->estado+1;

        switch($op->estado){
            //Si paso a estado "Procesando"
            case 1:
                $user = User::where("usuario_id", $op->usuario_id)->first();
                $us=array('email' => $user->email,'name' => $user->username);

                $admin=\App\User::first();
                $rem=$admin->email;

                $data = array(
                    'operacion' => \App\Modelo\Operacion::with("cuentabancariat","cuentabancariae","cuentabancariad","monedae","monedad","usuario")->find($op->operacion_id),
                    'mensaje' => "En estos momentos tu operación con DLS Perú esta siendo PROCESADA."
                );
                Mail::send('mail.cambioEstado', $data, function ($message) use($us,$rem) {
                        $message->to($us['email'], $us['name']);
                        $message->from($rem,'Operaciones DLS Perú')->subject('Operaciones DLS Perú');
                    });
                break;
            //Si paso a estado "Terminado"
            case 2:
                $user = User::where("usuario_id", $op->usuario_id)->first();
                $us=array('email' => $user->email,'name' => $user->username);

                $admin=\App\User::first();
                $rem=$admin->email;

                $data = array(
                    'operacion' => \App\Modelo\Operacion::with("cuentabancariat","cuentabancariae","cuentabancariad","monedae","monedad","usuario")->find($op->operacion_id),
                    'mensaje' => "Ha finalizado tu transacción exitosamente.",
                    'admin' => 0
                );
                Mail::send('mail.operacion', $data, function ($message) use($us,$rem) {
                        $message->to($us['email'], $us['name']);
                        $message->from($rem,'Operaciones DLS Perú')->subject('Operaciones DLS Perú');
                    });
                break;
        }
        $op->last_user=Auth::User()->username;
        $op->save();
        return $op;
    }
    public function doperacion($i) {
        $op=\App\Modelo\Operacion::find($i);
        $op->estado=3;
        $op->last_user=Auth::User()->username;
        $op->save();
        return "Proceso Anulado";
    }
    public function foperacion($i) {
        $op=\App\Modelo\Operacion::with("cuentabancariat","cuentabancariae","cuentabancariad","monedae","monedad","usuario")->find($i);
        return view("admin.ficha",compact("op"));
    }

    public function svoucher(Request $request){

        try{
            $op=\App\Modelo\Operacion::find($request->id_operacion);
    
            if($request->hasFile("vou")){
                $op->voucher=rand(1000000, 100000000).$request->File('vou')->getClientOriginalName();
                $request->File('vou')->move(public_path().'/assets/voucher/',$op->voucher);;
            }

            if($request->hasFile("vou2")){
                $op->voucher2=rand(1000000, 100000000).$request->File('vou2')->getClientOriginalName();
                $request->File('vou2')->move(public_path().'/assets/voucher/',$op->voucher2);;
            }

            if($request->hasFile("vou3")){
                $op->voucher3=rand(1000000, 100000000).$request->File('vou3')->getClientOriginalName();
                $request->File('vou3')->move(public_path().'/assets/voucher/',$op->voucher3);;
            }

            if($request->hasFile("vou4")){
                $op->vouche4r=rand(1000000, 100000000).$request->File('vou4')->getClientOriginalName();
                $request->File('vou4')->move(public_path().'/assets/voucher/',$op->vouche4r);;
            }
    
            $op->num_ope = $request->Input("num_ope");
            $op->num_ope2 = $request->Input("num_ope2");
            $op->num_ope3 = $request->Input("num_ope3");
            $op->num_ope4 = $request->Input("num_ope4");
            $op->save();

            return response([
                "success"=>true,
                "mensaje"=>"Operación actualizada correctamente"
            ]);
        }catch(Exception $e){
            return response([
                "success"=>false,
                "mensaje"=>"Error al actualizar la operación"
            ]);
        }
    }
}
