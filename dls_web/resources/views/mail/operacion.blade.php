<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
		<meta name=GENERATOR content="MSHTML 11.00.10570.1001">
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	
		<style>
	
		</style>
	</head>
<body>

<div class="container-fluid">
	<center>
		<table class="voucher">
			<tr>
				<th colspan="2"><a href="#"><img src="http://dls.com.pe/app/img_mail/cabecera.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/cabecera.jpg" alt="DLS"></a></th>
			</tr>
			<tr class="headt">
				<th colspan="2">
					<span class="text-blue"><h1>¡FELICITACIONES! <BR>{{$mensaje}}</h1></span>
				</th>
			</tr>
			<tr>
				<th colspan="2"><img src="{{asset('assets/images/operacionheader.png')}}"></th>
			</tr>
			<tr>
				<th align="right">Fecha de Operación</th>
				<td>{{$operacion->created_at}}</td>
			</tr>
			<tr>
				<th align="right">Fecha de Actualización</th>
				<td>{{$operacion->updated_at}} {{$operacion->last_user}}</td>
			</tr>
			<tr>
				<th align="right">Nombres</th>
				<td>{{$operacion->usuario->primernombre}} {{$operacion->usuario->segundonombre}}</td>
			</tr>
			<tr>
				<th align="right">Apellidos</th>
				<td>{{$operacion->usuario->primeroapellido}} {{$operacion->usuario->segundoapellido}}</td>
			</tr>
			@if(isset($email_cliente))
				<tr>
					<th align="right">Email</th>
					<td>{{$email_cliente}}</td>
				</tr>
			@endif
			@if(isset($dni))
				<tr>
					<th align="right">DNI</th>
					<td>{{$dni}}</td>
				</tr>
			@endif
			<tr class="sep">
				<th align="right">Cuenta de Envío</th>
				<td>{{$operacion->cuentabancariae[0]->nrocuenta}} @if(isset($operacion->cuentabancariae[0]->nrocuentacci)) | {{$operacion->cuentabancariae[0]->nrocuentacci}} @endif</td>
			</tr>
			<tr>
				<th align="right">Banco de Envío</th>
				<td>{{$operacion->cuentabancariae[0]->banco->nombre}}</td>
			</tr>
			<tr>
				<th align="right">Monto Enviado</th>
				<td><strong>@if($operacion->monedae->moneda_id == 1) S/ @else $ @endif {{number_format($operacion->monto, 2, '.', ',')}}</strong> @if($operacion->monedae->moneda_id == 1) Soles @else Dólares @endif</td>
			</tr>
			<tr class="sep">
				<th align="right">Cuenta Destino</th>
				<td>{{$operacion->cuentabancariad[0]->nrocuenta}} @if(isset($operacion->cuentabancariad[0]->nrocuentacci)) | {{$operacion->cuentabancariad[0]->nrocuentacci}} @endif</td>
			</tr>
			<tr>
				<th align="right">Banco Destino</th>
				<td><strong>{{$operacion->cuentabancariad[0]->banco->nombre}}</strong></td>
			</tr>
			<tr>
				<th align="right">Tipo de Cuenta | Moneda</th>
				<td><strong>{{$operacion->cuentabancariad[0]->tipo->nombre}} | {{$operacion->cuentabancariad[0]->moneda->nombre}}</strong></td>
			</tr>
			<tr>
				<th align="right">Monto Esperado</th>
				<td><strong>@if($operacion->monedad->moneda_id == 1) S/ @else $ @endif {{number_format($operacion->cambio, 2, '.', ',')}}</strong> @if($operacion->monedad->moneda_id == 1) Soles @else Dólares @endif</td>
			</tr>
			<tr class="sep">
				<th align="right">Tipo de Cambio</th>
				<td><strong>{{$operacion->taza}}</strong></td>
			</tr>
			<tr>
				<th align="right">Cuenta de Depósito</th>
				<td>{{$operacion->cuentabancariat[0]->nrocuenta}} @if(isset($operacion->cuentabancariat[0]->nrocuentacci)) | {{$operacion->cuentabancariat[0]->nrocuentacci}} @endif</td>
			</tr>
			<tr>
				<th align="right">Banco de depósito</th>
				<td><strong>{{$operacion->cuentabancariat[0]->banco->nombre}}</strong></td>
			</tr>
			<tr>
				<th align="right">Tipo de Cuenta | Moneda</th>
				<td><strong>{{$operacion->cuentabancariat[0]->tipo->nombre}} | {{$operacion->cuentabancariat[0]->moneda->nombre}}</strong></td>
			</tr>
			
			<tr class="headt">
				<th colspan="2"></th>
				<td></td>
			</tr>

			@if($admin == 1)
				@if(isset($operacion->voucher))
					<tr>
						<th colspan="2">
							{{-- <img src="C:\AppServ\www\ProyectosEnLaravel\dls\dsl_web\public\assets\voucher\{{$operacion->voucher}}" alt="Voucher" width='500'> --}}
							<img src="{{asset('assets/voucher/'.$operacion->voucher)}}" alt="Voucher" width='500'>
						</th>
					</tr>
				@endif
				@if(isset($operacion->voucher2))
					<tr>
						<th colspan="2">
							{{-- <img src="C:\AppServ\www\ProyectosEnLaravel\dls\dsl_web\public\assets\voucher\{{$operacion->voucher2}}" alt="Voucher" width='500'> --}}
							<img src="{{asset('assets/voucher/'.$operacion->voucher2)}}" alt="Voucher" width='500'>
						</th>
					</tr>
				@endif
				@if(isset($operacion->voucher3))
					<tr>
						<th colspan="2">
							{{-- <img src="C:\AppServ\www\ProyectosEnLaravel\dls\dsl_web\public\assets\voucher\{{$operacion->voucher3}}" alt="Voucher" width='500'> --}}
							<img src="{{asset('assets/voucher/'.$operacion->voucher3)}}" alt="Voucher" width='500'>
						</th>
					</tr>
				@endif
				@if(isset($operacion->voucher4))
					<tr>
						<th colspan="2">
							{{-- <img src="C:\AppServ\www\ProyectosEnLaravel\dls\dsl_web\public\assets\voucher\{{$operacion->voucher4}}" alt="Voucher" width='500'> --}}
							<img src="{{asset('assets/voucher/'.$operacion->voucher4)}}" alt="Voucher" width='500'>
						</th>
					</tr>
				@endif
			@endif
			
			<tr>
				<th colspan="2"><img src="{{asset('assets/images/operacion-pie.png')}}"></th>
			</tr>
			
			<tr>
				<th colspan="2"><a href="#"><img src="http://dls.com.pe/app/img_mail/dlspie.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/dlspie.jpg" alt="DLS"></a></td>
			</tr>
			
		</table>
	</center>
</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
