<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div class="user-body">
            <span class="avatar avatar-lg brround text-center cover-image" data-image-src="{{asset('assets/images/users/female/25.jpg')}}"></span>
        </div>
        <div class="user-info">
            <a href="{{route('home')}}" class="ml-2"><span class="text-dark app-sidebar__user-name font-weight-semibold">{{\Auth::User()->firstname.' '.\Auth::User()->lastname}}</span><br>
                <span class="text-muted app-sidebar__user-name text-sm">{{Auth::User()->username}}</span>
            </a>
        </div>
    </div>

    <ul class="side-menu">
        <li class="slide">
            <a class="side-menu__item {{Request::is('home') ? 'active' : ''}}" href="{{route('home')}}"><i class="side-menu__icon si si-screen-desktop"></i><span class="side-menu__label">Inicio</span></a>
        </li>

        @if(\Auth::User()->hasRole('Administrators'))
            <li>
                <a class="side-menu__item {{Request::is('lusuario') ? 'active' : ''}}" href="{{route('lusuario')}}"><i class="side-menu__icon fas fa-user-cog"></i><span class="side-menu__label">Usuarios</span></a>
            </li>
        @endif

        <li>
            <a class="side-menu__item {{Request::is('lcuentasbancaria') ? 'active' : ''}}" href="{{route('cuentasbancarias')}}"><i class="side-menu__icon fas fa-dollar-sign"></i><span class="side-menu__label">Mis bancos</span></a>
        </li>

        @if(\Auth::User()->hasRole('Usuario'))
            <li>
                <a class="side-menu__item {{Request::is('operacion') ? 'active' : ''}}" href="{{route('operacion')}}"><i class="side-menu__icon fas fa-calculator"></i><span class="side-menu__label">Iniciar operación</span></a>
            </li>
        @endif

        @if(\Auth::User()->hasRole('Usuario'))
            <li>
                <a class="side-menu__item {{Request::is('historial') ? 'active' : ''}}" href="{{route('historial')}}"><i class="side-menu__icon fas fa-file-alt"></i><span class="side-menu__label">Movimientos</span></a>
            </li>

            <li>
                <a class="side-menu__item {{Request::is('ayuda') ? 'active' : ''}}" href="{{route('ayuda')}}"><i class="side-menu__icon far fa-life-ring"></i><span class="side-menu__label">Ayuda</span></a>
            </li>
        @endif

        {{-- //TODO ME FALTA ESTO, QUE NOSE QUE ES... --}}
        @if(\Auth::User()->hasRole('Administrators'))
            <li>
                <a class="side-menu__item {{Request::is('tipocambio') ? 'active' : ''}}" href="{{route('tipocambio')}}"><i class="side-menu__icon fas fa-comment-dollar"></i><span class="side-menu__label">Tipo de Cambio</span></a>
            </li>

            <li>
                <a class="side-menu__item {{Request::is('reporte') ? 'active' : ''}}" href="{{route('reporte')}}"><i class="side-menu__icon fas fa-receipt"></i><span class="side-menu__label">Transacciones</span></a>
            </li>

            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-pie"></i><span class="side-menu__label">Reportes</span><i class="angle fas fa-angle-right"></i></a>
                <ul class="slide-menu">
                    <li>
                        <a href="{{url('reportesbs')}}" class="slide-item">SBS</a>
                    </li>
                    {{-- <li>
                        <a href="#" class="slide-item">Reporte 01</a>
                    </li> --}}
                    
                </ul>
            </li>
        @endif

        {{--
        @if(\Auth::User()->hasRole('Usuario'))
            <li>
                <a class="side-menu__item" href="libro.html"><i class="side-menu__icon fas fa-comment-dollar"></i><span class="side-menu__label">Libro de reclamaciones</span></a>
            </li>
        @endif
        --}}
    </ul>
</aside>