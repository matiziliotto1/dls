<!-- Navbar-->
<header class="app-header header">
	<!-- Navbar Right Menu-->
	<div class="container-fluid">
		<div class="d-flex">
			<a class="header-brand" href="https://www.fortuneonline.com.pe">
				<img alt="logo" class="header-brand-img main-logo" src="{{url('assets/images/brand/logo1.png')}}">
				<img alt="logo" class="header-brand-img mobile-logo" src="{{url('assets/images/brand/icon.png')}}">
			</a>
			<div class="d-none d-sm-flex">
				<a href="#" data-toggle="search" class="icon navsearch">
					<i class="fe fe-search"></i>
				</a>
			</div>
			<!-- Sidebar toggle button-->
			<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
			<div class="header-searchinput">
				<form class="form-inline">
					<div class="search-element mr-3">
						<input class="form-control header-search" type="search" placeholder="Buscar" aria-label="Buscar">
						<span class="search-icon"><i class="fa fa-search"></i></span>
					</div>
				</form>
			</div>

			<div class="d-flex order-lg-2 ml-auto">
				<div class="d-sm-flex d-none">
					<a href="#" class="nav-link icon full-screen-link">
						<i class="fe fe-minimize fullscreen-button"></i>
					</a>
				</div>


				<button class="navbar-toggler navresponsive-toggler d-sm-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
					aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon fe fe-more-vertical text-white"></span>
				</button>
				<!--Navbar -->

				<div class="dropdown">
					<a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
						<span class="avatar avatar-md brround cover-image" data-image-src="{{url('assets/images/users/female/25.jpg')}}"></span>
						<span class="ml-2 d-none d-lg-block">
							<span class="text-dark">{{\Auth::User()->firstname.' '.\Auth::User()->lastname}}</span>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
						<a class="dropdown-item" href="{{url('perfil')}}"><i class="dropdown-icon fe fe-user"></i>Perfil</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="{{url('ayuda')}}"><i class="dropdown-icon fe fe-help-circle"></i>Ayuda</a>
						<div class="dropdown-divider"></div>
			            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="Cerrar sesión"><i class="dropdown-icon fe fe-power"></i> Salir</a>
			            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			                @csrf
			            </form>		         

					</div>
				</div>
				
			</div>
		</div>
	</div>
</header>