<!doctype html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-TileColor" content="#0061da">
        <meta name="theme-color" content="#1643a3">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <link rel="icon" href="{{url('assets/images/brand/favicon.ico')}}" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('assets/images/brand/favicon.ico')}}" />
        <title>Fortune Online - Tu mejor tipo de cambio online</title>
        <link href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('assets/plugins/fontawesome-free/css/all.css')}}" rel="stylesheet">
        <link href="{{url('assets/css/dashboard.css')}}" rel="stylesheet" />
        <link href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}" rel="stylesheet" />
        <link href="{{url('assets/plugins/toggle-sidebar/css/sidemenu-closed-light.css')}}" rel="stylesheet">
        <link href="{{url('assets/plugins/sidebar/sidebar.css')}}" rel="stylesheet">
        <link href="{{url('assets/plugins/iconfonts/plugin.css')}}" rel="stylesheet" />

        
        @stack('css')

    </head>
	
	

    <body class="app sidebar-mini rtl">

        <!-- Global Loader-->
        <div id="global-loader"><img src="{{url('assets/images/loader.svg')}}" alt="cargando"></div>
        <div class="page">
            <div class="page-main">

                @include('layouts.header')
                @include('layouts.siderbar')

                <div class=" app-content my-3 my-md-5">
                    <div class="side-app">
                        <!--Page Header-->
                        <div class="mb-5">
                            <div class="page-header  mb-0">
                                <h4 class="page-title">
                                    @if(is_null(\Auth::User()->usuario_id))
                                        <a href="{{url('personal_o_empresa')}}" class="btn btn-outline-danger">Aún no ha terminado su perfil</a>    
                                    @endif
                                    @stack('titulo')
                                </h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@stack('titulo')</li>
                                </ol>
                            </div>
                        </div>

                        @yield('content')

                    </div>
                    @include('layouts.notificacion')
                    @include('layouts.footer')
                </div>                
            </div>
        </div>





        <script src="{{url('assets/js/vendors/jquery-3.2.1.min.js')}}"></script>
	
        <script src="{{url('assets/js/vendors/jquery.sparkline.min.js')}}"></script>
        <script src="{{url('assets/js/vendors/selectize.min.js')}}"></script>
        <script src="{{url('assets/js/vendors/jquery.tablesorter.min.js')}}"></script>
        <script src="{{url('assets/js/vendors/circle-progress.min.js')}}"></script>

        <script src="{{url('assets/plugins/rating/jquery.rating-stars.js')}}"></script>
        <script src="{{url('assets/plugins/bootstrap/popper.min.js')}}"></script>
        <script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{url('assets/plugins/toggle-sidebar/js/sidemenu.js')}}"></script>
        <script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script src="{{url('assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
        <script src="{{url('assets/plugins/counters/counterup.min.js')}}"></script>
        <script src="{{url('assets/plugins/counters/waypoints.min.js')}}"></script>
        <script src="{{url('assets/plugins/sidebar/sidebar.js')}}"></script>

        @stack('js')
        <script src="{{url('assets/js/custom.js')}}"></script>
    </body>

</html>
