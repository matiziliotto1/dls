<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse text-white-50">
            <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                Copyright © 2019 <a href="#">Fortune Online</a>. Designed by <a href="http://www.bluecomunicadores.com">Blue Network</a> All rights reserved.
            </div>
        </div>
    </div>
</footer>