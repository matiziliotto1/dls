@extends('layouts.app')
@push('titulo')
Cuentas Bancarias
@endpush
@section('content')

<?php 
	
	$mos=\App\Modelo\Moneda::all();
	$ban=\App\Modelo\Banco::all();
	$tpo=\App\Modelo\Tipocuenta::all();

 ?>
<div class="card-body">
	<div class="btn-list">		
		<a href="{{url('lcuentasbancaria')}}" class="btn btn-outline-success">Lista de cuentas bancaria</a>		
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">Añade la cuenta bancaria en la cual deseas recibir el depósito. </div> 
				
				 &nbsp;Por resolución N 6426 - 2015 anexo 2 de la SBS, los campos con * son obligatorios:
			</div>
			<div class="card-body">				
				<form action="{{url('ucuentasbancaria/'.$ls->cuentabancaria_id)}}" method="post">
					@csrf
				<div class="form-group ">
					<label class="form-label">Banco</label>
					<select name="ba" id="ba" class="form-control select2 custom-select" data-placeholder="Elije uno">
						@foreach($ban as $ba)
						<option value="{{$ba->banco_id}}">{{$ba->nombre}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="form-label">Número de cuenta</label>
					<input type="number" id="nu" required class="form-control" name="nu" placeholder="Número de cuenta">	
				</div>

				<div class="form-group ">
					<label class="form-label">Moneda</label>
					<select class="form-control select2 custom-select" id="mo" name="mo" data-placeholder="Elije la moneda">
						@foreach($mos as $mo)
						<option value="{{$mo->moneda_id}}">{{$mo->nombre}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label class="form-label">Alias <span class="form-help bg-primary text-white" data-toggle="popover" data-placement="top"
						data-content="<p>Elige un alias para identidicar esta cuenta ejemplo: Cta Dolares BCP</a></p>
							">?</span></label>
					<input type="text" class="form-control" name="al" id="al" placeholder="Alias">
					
					<span class="col-auto align-self-center"></span>
				</div>
				<div class="form-group ">
					<label class="form-label">Tipo de cuenta</label>
					<select class="form-control select2 custom-select" id="ti" name="ti" data-placeholder="Elije la moneda">
						@foreach($tpo as $tc)
						<option value="{{$tc->tipocuenta_id}}">{{$tc->nombre}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group ">
					<div class="form-label">Cuenta Propia</div>
					<div class="custom-controls-stacked">
						<label class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" id="" name="cp" value="1">
							<span class="custom-control-label">Si</span>
						</label>
						<label class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" id="" name="cp" value="0">
							<span class="custom-control-label">No</span>
						</label>
					</div>
				</div>	
				<div class="card-body">
					<div class="btn-list">
						<input class="btn btn-success" type="submit" value="Guardar cuenta" />
						<a href="{{url('lcuentasbancaria')}}" class="btn btn-danger">Cancelar</a>	
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script>
	var data=<?php echo $ls ?>;
	$(document).ready(function(){ 
		$("#ba").val(data.banco_id);
		$("#nu").val(data.nrocuenta);
		$("#mo").val(data.moneda_id);
		$("#al").val(data.alias);
		$("#ti").val(data.tipocuenta_id);
		$("input[name=cp][value='"+data.cuentapropia+"']").prop("checked",true);
	});
</script>
@endpush