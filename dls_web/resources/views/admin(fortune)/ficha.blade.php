<div>
	<table class="table">
		<tr>
			<th>Fecha de Operación</th>
			<td>{{$op->created_at}}</td>
		</tr>
		<tr>
			<th>Fecha de Actualización</th>
			<td>{{$op->updated_at}} {{$op->last_user}}</td>
		</tr>
		<tr>
			<th>Nombres</th>
			<td>{{$op->usuario->primernombre}} {{$op->usuario->segundonombre}}</td>
		</tr>
		<tr>
			<th>Apellidos</th>
			<td>{{$op->usuario->primeroapellido}} {{$op->usuario->segundoapellido}}</td>								
		</tr>
		<tr>
			<th>Cuenta de Envio</th>
			<td>{{$op->cuentabancariae[0]->nrocuenta}}</td>
		</tr>
		<tr>
			<th>Monto Enviado</th>
			<td><strong>{{number_format($op->monto, 2, '.', ',')}}</strong> {{$op->monedae->nombre}}</td>
		</tr>
		<tr>
			<th>Cuenta Destino</th>
			<td>{{$op->cuentabancariad[0]->nrocuenta}}</td>
		</tr>
		<tr>
			<th>Monto Esperado</th>
			<td><strong>{{number_format($op->cambio, 2, '.', ',')}}</strong> {{$op->monedad->nombre}}</td>
		</tr>
		<tr>
			<th>Cuenta de Depósito</th>
			<td>{{$op->cuentabancariat[0]->nrocuenta}}</td>
		</tr>
		<tr>
			<th colspan="2">
				<img src="{{url('assets/voucher/'.$op->voucher)}}" alt="">
			</th>
		</tr>
	</table>
</div>