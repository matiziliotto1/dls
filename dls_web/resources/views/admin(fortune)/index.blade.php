@extends('layouts.app')

@push('titulo')
Bienvenido
@endpush

@push('css')
<link href="{{url('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{url('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{url('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<style>
    .ajs-ok{
        border: none;
        background-color: green;
        color: white;
        border-radius: 5px;
    }
    .ajs-cancel{
        border: none;
        background-color: red;
        color: white;
        border-radius: 5px;
    }
    .label{color: white;font-weight: bold; font-size: 0.75em;}
    .label-warning{background-color: #a98307;padding: 2px 4px;border-radius: 5px;}
    .label-primary{background-color: #2271b3;padding: 2px 4px;border-radius: 5px;}
    .label-success{background-color: #00bb2d;padding: 2px 4px;border-radius: 5px;}
    .label-danger{background-color: #dc3545;padding: 2px 4px;border-radius: 5px;}
</style>
@endpush
@section('content')
@if(\Auth::User()->hasRole('Administrators'))
<?php 
$ls=\App\Modelo\Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->get();
 ?>
@else
<?php
$ls=\App\Modelo\Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("usuario_id",\Auth::User()->usuario_id)->get();
 ?>
@endif
<div class="row">
    <div class="col-xl-9 col-md-12 col-lg-12">
        <div class="card overflow-hidden">
            <div class="card-header">
                <h3 class="card-title">Flujo del tipo de cambio por mes</h3>
            </div>
            <div class="card-body">
                <canvas id="chart" class="h-350"></canvas>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-12 col-lg-12">
        @include('layouts.cambio')
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Últimos movimientos</div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered key-buttons text-nowrap" >
                        <thead>
                            <tr>
                                <th class="border-bottom-0">Fecha</th>
                                <th class="border-bottom-0">Nombre</th>
                                <th class="border-bottom-0">Moneda origen</th>
                                <th class="border-bottom-0">Enviado</th>
                                <th class="border-bottom-0">Moneda destino</th>                             
                                <th class="border-bottom-0">Esperado</th>
                                <th class="border-bottom-0">Tasa</th>
                                <th class="border-bottom-0">Estado</th>
                                <th class="border-bottom-0">Actualizado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ls as $l)
                            <tr id="{{$l->operacion_id}}">
                                <td>{{$l->created_at}}</td>
                                <td>{{$l->usuario->primernombre}} {{$l->usuario->segundonombre}} {{$l->usuario->primerapellido}} {{$l->usuario->segundoapellido}}</td>
                                <td>{{$l->monedae->nombre}}</td>
                                <td>{{$l->monto}}</td>
                                <td>{{$l->monedad->nombre}}</td>                                
                                <td>{{$l->cambio}}</td>
                                <td>{{$l->taza}}</td>
                                <td>
                                    @if($l->estado==0) <span class="label label-warning">Enviado</span> @endif
                                    @if($l->estado==1) <span class="label label-primary">Procesando</span> @endif
                                    @if($l->estado==2) <span class="label label-success">Terminado</span> @endif
                                    @if($l->estado==3) <span class="label label-danger">Anulado</span> @endif
                                </td>
                                <td>{{$l->last_user}}</td>                   
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{url('assets/plugins/input-mask/jquery.mask.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/chart.js/chart.min.js')}}"></script>
<script src="{{url('assets/plugins/chart.js/chart.extension.js')}}"></script>
<script>
    "use strict";
    var tabla;    
    var dcompra=[];
    var dventa=[];
    var dlabel=[];
    $(document).ready(function() {
        tabla = $('#example').DataTable( {
            lengthChange: false,
            buttons: ['excel', 'pdf', 'colvis' ]
        });
        tabla.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
        tabla.column( '0:visible' ).order( 'desc' ).draw();

        function lista(){
            return $.get("{{url('ltipocambio')}}",function(r){
                for (var i = 1; i < r.length; i++) {
                    dcompra.push(r[i].compra);
                    dventa.push(r[i].venta);
                    dlabel.push(r[i].created_at);
                }
            });
        }
        async function data(){
             await lista();
             grafica();
                    
        }
        data();       
    });

    function grafica(){
        var ctx = document.getElementById( "chart" );
        var myChart = new Chart( ctx, {
            type: 'line',
            data: {
                labels: dlabel,
                datasets: [
                    {
                        label: "Compra",
                        data: dcompra,
                        borderColor: "blue",
                        borderWidth: "1",
                        pointRadius:"2",
                        pointBorderColor: 'transparent',
                        pointBackgroundColor: 'blue',
                        backgroundColor: "rgb(0,0,255,0.05)"
                    },{
                        label: "Venta",
                        data: dventa,
                        borderColor: "red",
                        borderWidth: "1",
                        pointRadius:"2",
                        pointBorderColor: 'transparent',
                        pointBackgroundColor: 'red',
                        backgroundColor: "rgb(255,0,0,0.05)"
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                barRoundness: 1,
                scales: {
                    yAxes: [ {
                        ticks: {
                            beginAtZero: true
                            }
                        }]
                }
            }
        });
    }
</script>
@endpush