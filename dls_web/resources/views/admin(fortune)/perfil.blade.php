@extends('layouts.app')
@push('titulo')
Editar Perfil
@endpush
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css">
<style>
	.ajs-ok{
		border: none;
		background-color: green;
		color: white;
		border-radius: 5px;
	}
	.ajs-cancel{
		border: none;
		background-color: red;
		color: white;
		border-radius: 5px;
	}
	.edita{
		color: green;
	}
	.ajs-message{color: white;}
</style>
@endpush
@section('content')
<?php 
	$us=\App\Modelo\Usuario::find(\Auth::User()->usuario_id);
	$td=\App\Modelo\Tiposdocumento::all();
	$ocs=\App\Modelo\Ocupacion::all();
	$doc=\App\Modelo\Tiposdocumento::all();
 ?>

<div class="row">
	<div class="col-xl-12  col-md-12">
		<form action="" id="fus" method="POST">
			@csrf
			<div class="card">
				<div class="card-header">
					<div class="card-title">Editar Datos de Perfil</div>
				</div>
				<div class="card-body">
					<div class="card-title font-weight-bold">Información Básica:</div>
					<div class="row">
						
						@if($us->empresa==1)
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Registro Único de Contribuyentes (RUC)</label>
									<input type="text" class="form-control" placeholder="Número de ruc" required  name="nr" value="{{$us->ruc}}" readonly>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Razón social</label>
									<input type="text" class="form-control" placeholder="Razón social" required  name="rz" value="{{$us->razon_social}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Giro de negocio</label>
									<input type="text" class="form-control" placeholder="Giro de negocio" required  name="gn" value="{{$us->giro_negocio}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								{{--Para que haya una separacion de la informacion en la vista--}}
							</div>

					
							<div class="col-sm-6 col-md-6">
							
							<div class="card-title font-weight-bold">Datos del Representante Legal:</div>
								<div class="form-group">
									
									
									<label class="form-label edita">Primer Nombre</label>
									<input type="text"  class="form-control" placeholder="First Name" required name="pn" value="{{$us->primernombre}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
							<div class="card-title font-weight-bold">&nbsp;</div>
								<div class="form-group">
									<label class="form-label edita">Segundo Nombre</label>
									<input type="text" class="form-control" placeholder="Second Name" name="sn" value="{{$us->segundonombre}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido Paterno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="ap" value="{{$us->primeroapellido}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido Materno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="am" value="{{$us->segundoapellido}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Tipo de Documento</label>
									<select class="form-control select2 custom-select" id="td" name="td" >
										@foreach($doc as $do)
										<option value="{{$do->tiposdocumento_id}}">{{$do->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Número de Documento</label>
									<input type="text" class="form-control" placeholder="Numero de Documento" required  name="nd" value="{{$us->nrodocumento}}">
								</div>
							</div>


							<div class="form-group col-md-6">
								<div class="card-title font-weight-bold">Dirección Fiscal:</div>
								<label class="form-label edita">Departamento</label>
								<select name="dep" class="form-control custom-select" id="dep" required></select>
							</div>
	
							<div class="form-group col-md-6">
							<div class="card-title font-weight-bold">&nbsp;</div>
								<label class="form-label edita">Provincia</label>
								<select name="pro" class="form-control custom-select" id="pro" required></select>
							</div>
	
							<div class="form-group col-md-6">
								<label class="form-label edita">Distrito</label>
								<select name="dis" class="form-control custom-select" id="dis" required></select>
							</div>

							<div class="form-group col-md-6">
								<label class="form-label edita">Dirección</label>
								<input type="text" class="form-control" placeholder="City" name="dirf" required value="{{$us->Direccion}}">
							</div>

							<div class="form-group col-md-6">
								<label class="form-label edita">Teléfono</label>
								<input type="text" class="form-control" name="tel" maxlength="50"  placeholder="Teléfono" value="{{$us->telefono}}">
							</div>

							<div class="form-group col-md-6">
								<label class="form-label edita">Correo electrónico</label>
								<input type="text" class="form-control" name="correo" maxlength="35"  placeholder="Correo electrónico" value="{{$us->correo_electronico}}">
							</div>


							<div class="col-sm-6 col-md-6">
							<div class="card-title font-weight-bold">Datos Personales del Contacto:</div>
								<div class="form-group">
									<label class="form-label edita">Primer nombre (contacto)</label>
									<input type="text"  class="form-control" placeholder="First Name" required name="pnc" value="{{$us->primernombre_c}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
							<div class="card-title font-weight-bold">&nbsp;</div>
								<div class="form-group">
									<label class="form-label edita">Segundo nombre (contacto)</label>
									<input type="text" class="form-control" placeholder="Second Name" name="snc" value="{{$us->segundonombre_c}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido paterno (contacto)</label>
									<input type="text" class="form-control" placeholder="Last Name" name="apc" value="{{$us->primerapellido_c}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido materno (contacto)</label>
									<input type="text" class="form-control" placeholder="Last Name" name="amc" value="{{$us->segundoapellido_c}}">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label edita">Tipo de Documento (Contacto)</label>
								<select class="form-control select2 custom-select" id="tdc" name="td" >
									@foreach($doc as $do)
									<option value="{{$do->tiposdocumento_id}}">{{$do->nombre}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label edita">Número de documento (contacto)</label>
								<input type="text" class="form-control" name="ndc" maxlength="35"  placeholder="Número de documento" value="{{$us->nrodocumento_c}}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label edita">Teléfono de contacto</label>
								<input type="text" class="form-control" name="telc" maxlength="35"  placeholder="Teléfono de contacto" value="{{$us->telefono_c}}">
							</div>

						@elseif($us->personal==1)
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Tipo de Documento</label>
									<select class="form-control select2 custom-select" id="td" name="td" >
										@foreach($doc as $do)
										<option value="{{$do->tiposdocumento_id}}">{{$do->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Número de Documento</label>
									<input type="text" class="form-control" placeholder="Numero de Documento" required  name="nd" value="{{$us->nrodocumento}}">
								</div>
							</div>
	
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Primer Nombre</label>
									<input type="text"  class="form-control" placeholder="First Name" required name="pn" value="{{$us->primernombre}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Segundo Nombre</label>
									<input type="text" class="form-control" placeholder="Second Name" name="sn" value="{{$us->segundonombre}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido Paterno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="ap" value="{{$us->primeroapellido}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Apellido Materno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="am" value="{{$us->segundoapellido}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Fecha de Nacimiento</label>
									<input type="date" class="form-control" value="{{$us->fecnacimiento}}" name="fn">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Celular</label>
								<input type="text" class="form-control" name="cel1" maxlength="35" value="{{\Auth::User()->actkey}}"  placeholder="Número de Celular" required>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Celular 2</label>
								<input type="text" class="form-control" name="cel2" maxlength="50" value="{{\Auth::User()->user_home_path}}"  placeholder="Otro Número">
							</div>
							<div class="form-group col-md-6">
								<div class="form-group">
									<label class="form-label edita">Teléfono</label>
								<input type="text" class="form-control" name="cel" maxlength="30"  placeholder="Teléfonos de contacto" value="{{\Auth::User()->userid}}">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label edita">Dirección</label>
									<input type="text" class="form-control" placeholder="City" name="dir" required value="{{$us->Direccion}}">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label edita">Departamento</label>
								<select name="dep" class="form-control custom-select" id="dep" required></select>
							</div>
	
							<div class="form-group col-md-6">
								<label class="form-label edita">Provincia</label>
								<select name="pro" class="form-control custom-select" id="pro" required></select>
							</div>
	
							<div class="form-group col-md-6">
								<label class="form-label edita">Distrito</label>
								<select name="dis" class="form-control custom-select" id="dis" required></select>
							</div>
	
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label">Usuario Alias</label>
									<input type="" class="form-control" value="{{\Auth::User()->username}}" readonly="true">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="form-label">Correo Electrónico</label>
									<input type="" class="form-control" value="{{\Auth::User()->email}}" readonly="true">
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<label class="form-label edita">Ocupación</label>
									<select class="form-control select2 custom-select" id="oc" name="oc">
										@foreach($ocs as $oc)
										<option value="{{$oc->ocupacion_id}}">{{$oc->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group ">
									<div class="form-label edita">¿Es Usted una persona expuesta políticamente?</div>
									<div class="custom-controls-stacked">
										<label class="custom-control custom-radio">
											<input required type="radio" class="custom-control-input required" name="pe" value="1" {{$us->personaexpuesta ? "checked":""}}>
											<span class="custom-control-label">Si</span>
										</label>
										<label class="custom-control custom-radio">
											<input required type="radio" class="custom-control-input required" name="pe" value="0" {{$us->personaexpuesta ? "":"checked"}}>
											<span class="custom-control-label">No</span>
										</label>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
				<div class="card-footer text-right">
					<button href="#" class="btn btn-primary">Cambiar</button>
					<a href="#" class="btn btn-danger">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
<script>
	var us=<?php echo $us; ?>;
	$(document).ready(function(){

		$("#oc").val(us.ocupacion_id);
		$("#td").val(us.tiposdocumento_id);

	    $.get("{{url('api/departamento')}}").then(function(ls){
	        for (var i =  1; i < ls.length; i++) {
	            $("#dep").append('<option value="'+ls[i].dDepartamento+'">'+ls[i].Descripcion+'</option>');
	        }
	        $("#dep").val(us.departamento_id);
	        provincia($("#dep").val());
	    });
	    function provincia(i) {
	        $("#pro option").remove();
	        $.get("{{url('api/provincia')}}/"+i).then(function(ls){
	            for (var i =  1; i < ls.length; i++) {
	                $("#pro").append('<option value="'+ls[i].codProvincia+'">'+ls[i].Descripcion+'</option>');
	            }
	            $("#pro").val(us.provincia_id);
	            distrito($("#dep").val(),$("#pro").val());
	        });
	    }
	    function distrito(i,j) {
	        $("#dis option").remove();
	        $.get("{{url('api/distrito')}}/"+i+"/"+j).then(function(ls){
	            for (var i =  1; i < ls.length; i++) {
	                $("#dis").append('<option value="'+ls[i].codDistrito+'">'+ls[i].Descripcion+'</option>');
	            }
	            $("#dis").val(us.distrito_id);
	        });
	    }
	    $('#dep').on('change', function(){
	        provincia($("#dep").val());
	    });
	    $('#pro').on('change', function(){
	        distrito($("#dep").val(),$("#pro").val());
	    });                    
	});
    $(document).on('submit', '#fus', function (e) {
		e.preventDefault();
		if({{$us->personal}}==1){
			$.post("uusuario/"+{{$us->usuario_id}},$("#fus").serialize(),function (msg) {
				alertify.success(msg);
				});
		}
		else if({{$us->empresa}}==1){
			$.post("uusuario_empresa/"+{{$us->usuario_id}},$("#fus").serialize(),function (msg) {
				alertify.success(msg);
				});
		}
	});

</script>
@endpush