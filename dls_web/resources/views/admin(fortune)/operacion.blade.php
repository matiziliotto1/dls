@extends('layouts.app')
@push('titulo')
Iniciar Operación
@endpush
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css">
<style>
    .label-danger{
        color: red;
        font-weight: bold;
    }
    .label-success{
        color: green;
        font-weight: bold;
    }
    .ajs-message {
        color: white;
    }
</style>
@endpush
@section('content')
<?php 
    $ban=\App\Modelo\Cuentabancaria::with("tipo","moneda","banco")->where("usuario_id",\Auth::User()->usuario_id)->get();
    $mos=\App\Modelo\Moneda::all();
    $cud=\App\Modelo\Cuentabancaria::with("tipo","moneda","banco")->where("usuario_id",1)->get();
?>
<div class="card">
    <div class="card-body">
        <form id="form" action="soperacion" method="post" enctype="multipart/form-data">
            @csrf
            <div id="basicwizard" class="border pt-0">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item"><a href="#tab1" data-toggle="tab" class="nav-link font-bold">Calcula</a></li>
                    <li class="nav-item"><a href="#tab2" data-toggle="tab" class="nav-link font-bold">Transfiere</a></li>
                    <li class="nav-item"><a href="#tab3" data-toggle="tab" class="nav-link font-bold">Finalizado</a></li>
                </ul>
                <div class="tab-content card-body  b-0 mb-0">
                    <div class="tab-pane fade" id="tab1">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group clearfix">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="control-label form-label" for="userName">¿Desde qué cuenta nos envias tu dinero?</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-control select2 custom-select" required name="baa">
                                                @foreach($ban as $ba)
                                                <option value="{{$ba->cuentabancaria_id}}">({{$ba->tipo->nombre}} | {{$ba->moneda->nombre}} | {{$ba->banco->nombre}}) {{$ba->nrocuenta}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            <label class="control-label form-label " for="password"> ¿En qué cuenta deseas recibir tu dinero?</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select name="cuenta" class="form-control select2 custom-select" required>
                                                @foreach($ban as $ba)
                                                <option value="{{$ba->cuentabancaria_id}}">({{$ba->tipo->nombre}} | {{$ba->moneda->nombre}} | {{$ba->banco->nombre}}) {{$ba->nrocuenta}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            <label class="control-label form-label" for="confirm">TÚ NOS ENVÍAS</label> 
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="dash-widget text-center">
                                                        <p>Actualización de Tipo de Cambio</p>
                                                        <div class="col">
                                                            <p class="mt-1 mb-1"><i class="fas fa-cronometer text-danger"></i> <h3 class="font-weight-extrabold cronometro">0.00</h3> Segundos</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="custom-controls-stacked">
                                                @foreach($mos as $mo)
                                                <label class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" name="moe" onchange="Calcular()" value="{{$mo->moneda_id}}" required>
                                                    <span class="custom-control-label">{{$mo->nombre}}</span>
                                                </label>
                                                @endforeach
                                                <div class="col-lg-10">
                                                    <input type="number" required class="form-control" id="monto" name="monto" onkeyup="Calcular()" value="0.00" placeholder="Ingresa un monto" /></br>
                                                    <span onclick="Calcular()" class="btn btn-pill btn-info"><i class="fas fa-calculator"></i> Calcular</span>
                                                </div>
                                                <div id="alerta" class="label label-success" style="visibility: hidden;">
                                                    Si vas a transferir más de U$5 000 (cinco mil dólares americanos), por favor comunícate con nosotros por Whatsapp <a href="https://wa.me/51946091321">AQUÍ</a>. Tenemos un cambio preferencial para ti. 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            @if(\Auth::User()->regdate==1) @include('layouts.preferencial') @else @include('layouts.cambio')  @endif
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card"><br>
                                                <div class="dash-widget text-center">
                                                    <h4>TÚ RECIBES</h4>
                                                     <h4 class="counter p-4 bg-gradient-success text-white br-7 text-center" id="reci"></h4>
                                                    <p class="mb-0 text-muted"><span id="dine"></span></p>
                                                </div>
                                                <div class="card-body">
                                                    <div class="dash-widget text-center">
                                                        
                                                        <h4><i class="side-menu__icon far fa-money-bill-alt"></i>Tipo de cambio usado <span id="tcus"></span></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                        <div class="row">
                            <div class="col-12">
                                <div class="card"><br>
                                    <div class="dash-widget text-center">
                                        <h4>Transfiérenos</h4>
                                        <h4 class="counter p-4 bg-info text-white br-7 text-center" id="btra"></h4>
                                        <p class="mb-0 text-muted" id="bmon"></p>
                                    </div>
                                </div>
                                <div class="card"><br>
                                    <div class="dash-widget text-center">
                                        <h4>y recibirás</h4>
                                        <h4 class="counter p-4 bg-gradient-success text-white br-7 text-center" id="atra"></h4>
                                        <p class="mb-0 text-muted" id="amon"></p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            <label class="control-label form-label" for="name">Nuestras Cuentas</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <input type="text" name="mor" id="mor" hidden>
                                            <select id="bar" class="form-control select2 custom-select" name="bar">
                                                @foreach($cud as $ba)
                                                <option value="{{$ba->cuentabancaria_id}}">({{$ba->tipo->nombre}} | {{$ba->moneda->nombre}} | {{$ba->banco->nombre}}) {{$ba->nrocuenta}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            <label class="control-label form-label" for="surname"> Cuenta Seleccionada:</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="dash-widget text-center">
                                                <h5>
                                                    <span id="tcse"></span>            <span id="ncse"></span></br>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="row ">
                                        <div class="col-lg-3">
                                            <div class="form-label">Si hizo la transferencia, adjunte el voucher aquí</div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <div class="custom-file">
                                                    <input type="file" required class="form-control file-input" id="vou" name="vou" accept="image/png, .jpeg, .jpg, image/gif"><br><br>
													
													
										
										
										<ul class="list-group">
											<li class="list-group-item"><i class="fas fa-exclamation-triangle text-warning" aria-hidden="true"></i>  Recuerda que <strong>NO ACEPTAMOS DEPÓSITOS EN EFECTIVO</strong>, solo transferencias</li>
											<li class="list-group-item"><i class="fas fa-cog text-info" aria-hidden="true"></i> Si no puedes subir tu voucher, comunicate con nosotros mediante whatsapp <a href="https://wa.me/51946091321">aquí</a> o correo electrónico <a href="mailto:tesoreria@fortuneonline.com.pe">tesoreria@fortuneonline.com.pe.</a></li>
											
										</ul>
										
													<br><br>
                                                </div>
                                                <div id="imagen" class="label label-danger" style="visibility: hidden;">
                                                    Archivo demasiado grande, máximo 8MB
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group clearfix">
                                    <div class="checkbox checkbox-info text-center">
                                        <label>
                                            <div class="dash-widget text-center"><p class="lead"> ¡Muchas gracias! Para procesar tu operación darle clic al botón "Enviar mi comprobante" <br>
											para que esta pueda ser guardada. En Breve u asesor se comunicará con usted.</p> </div>
                                            <p class="lead">Recuerde que si falta indicar el monto y la cuenta de depósito, no se enviará el formulario.</p>  <br>    <br>                                     
                                            <button class="btn btn-success mb-0 waves-effect waves-light" onclick="valida()">Enviar mi comprobante</button>
                                            <input type="text" id="cambio" name="cambio" hidden="true">
                                            <input type="text" id="compra" name="compra" hidden="true">
                                        </label>
										
										<br><br>
										
									
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-inline wizard mb-0">
                        <li class="previous list-inline-item"><a href="#" class="btn btn-primary mb-0 waves-effect waves-light">Anterior</a></li>
                        <li class="next list-inline-item float-right"><a href="#" class="btn btn-primary mb-0 waves-effect waves-light">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
<script src="{{url('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
<script src="{{url('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{url('assets/js/wizard.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
<script>

    function Calcular(){
        var m=$("#monto").val();
        var d=$('input[name=moe]:checked').val();
        if(d==2){
            if(!pref && eval(m)>=eval(5000)){
                $("#alerta").css("visibility","visible");
            }
            $("#bar option").remove();
            bd.forEach(cuenta=>$("#bar").append('<option value="'+cuenta.cuentabancaria_id+'">('+cuenta.tipo.nombre+' | '+cuenta.moneda.nombre+' | '+cuenta.banco.nombre+') '+cuenta.nrocuenta+'</option>'));
            $("#dine").html("Soles");
            cam=(eval(m)*eval(dc)).toFixed(2);
            $("#reci").html(new Intl.NumberFormat("en-US").format(cam));
            $("#cambio").val(cam);
            $("#compra").val(dc);
            $("#tcus").html(dc);
            $("#atra").html(cam);
            $("#bmon").html("Dólares");
            $("#btra").html(m);
            $("#amon").html("Soles");
            $("#mor").val(1);
        }else{
            $("#bar option").remove();
            bs.forEach(cuenta=>$("#bar").append('<option value="'+cuenta.cuentabancaria_id+'">('+cuenta.tipo.nombre+' | '+cuenta.moneda.nombre+' | '+cuenta.banco.nombre+') '+cuenta.nrocuenta+'</option>'));
            $("#dine").html("Dólares");
            cam=(eval(m)/eval(dv)).toFixed(2);
            $("#reci").html(new Intl.NumberFormat("en-US").format(cam));
            $("#cambio").val(cam);
            $("#tcus").html(dv);
            $("#atra").html(cam);
            $("#bmon").html("Soles");
            $("#btra").html(m);
            $("#amon").html("Dólares");
            $("#compra").val(dv);
            $("#mor").val(2);
        }
    }
    var banco=<?php echo $cud ?>;
    var bs=banco.filter(cuenta => cuenta.moneda.moneda_id==1);
    var bd=banco.filter(cuenta => cuenta.moneda.moneda_id==2);

    $(document).ready(function(){
        $("input[name=moe][value='1']").prop("checked",true);
        function selecte() {
            var select=$("#bar").val();
            var objeto = banco.find(function(element) {
                if(element.cuentabancaria_id==select){
                    return element;
                }else{
                    return false;
                }
            });
            $("#tcse").html(objeto.banco.nombre+", "+objeto.tipo.nombre+" "+objeto.moneda.nombre);
            $("#ncse").html(objeto.nrocuenta); 
        }
        $("#bar").change(function(){
              selecte();
        });
        selecte();
    });
    function valida(){
        if($("#monto").val()==""){
            alertify.error("Ingresa el monto");
        }
        if($("#vou").val()==""){
            alertify.error("Sube una captura del comprobante");
        }
    }
    $(document).on('submit', '#form', function (e) {        
        var input = ($("#vou"))[0];
        var file = input.files[0];
        var zz=eval(cam)<1;
        var yy=eval(file.size>8000000);
        if(zz){
            alertify.error("Aplique la taza de Cambio");
            e.preventDefault();
        } 
        if(yy){
            $("#imagen").css("visibility","visible");
            alertify.error("Error al cargar archivo");
            e.preventDefault();
        }
        if(!zz&&!yy){
            $("#global-loader").show();
        }
    });

</script>
@endpush