@extends('layouts.app')
@push('titulo')
Cuentas Bancarias
@endpush
@section('content')

<?php 
	
	$mos=\App\Modelo\Moneda::all();
	$ban=\App\Modelo\Banco::all();
	$tpo=\App\Modelo\Tipocuenta::all();

 ?>
<div class="card-body">
	<div class="btn-list">		
		<a href="{{url('lcuentasbancaria')}}" class="btn btn-outline-success"><i class="fas fa-piggy-bank"></i> LISTA DE MIS CUENTAS BANCARIAS</a>		
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">Añade la cuenta bancaria en la cual &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> deseas recibir el depósito. </div> 
				
				Por resolución N 6426 - 2015<br>anexo 2 de la SBS<br>los campos con * son obligatorios:
			</div>
			<div class="card-body">				
				<form action="{{url('scuentasbancaria')}}" method="post">
					@csrf
				<div class="form-group ">
					<label class="form-label">*Banco</label>
					<select name="ba" class="form-control select2 custom-select" data-placeholder="Elije uno">
						@foreach($ban as $ba)
						<option value="{{$ba->banco_id}}">{{$ba->nombre}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="form-label">*Número de cuenta</label>
					<input type="number" required class="form-control" name="nu" placeholder="Número de cuenta">	
				</div>

				<div class="form-group ">
					<label class="form-label">*Moneda</label>
					<select class="form-control select2 custom-select" name="mo" data-placeholder="Elije la moneda">
						@foreach($mos as $mo)
						<option value="{{$mo->moneda_id}}">{{$mo->nombre}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label class="form-label">Alias <span class="form-help bg-primary text-white" data-toggle="popover" data-placement="top"
						data-content="<p>Elige un alias para identidicar esta cuenta ejemplo: Cta Dolares BCP</a></p>
							">?</span></label>
					<input type="text" class="form-control" name="al" placeholder="Alias">
					
					<span class="col-auto align-self-center"></span>
				</div>
				<div class="form-group ">
					<label class="form-label">*Tipo de cuenta</label>
					<select class="form-control select2 custom-select" name="ti" data-placeholder="Elije la moneda">
						@foreach($tpo as $tc)
						<option value="{{$tc->tipocuenta_id}}">{{$tc->nombre}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group ">
					<div class="form-label">*Cuenta Propia</div>
					<div class="custom-controls-stacked">
						<label class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" name="cp" value="1" checked>
							<span class="custom-control-label">Si</span>
						</label>
						<label class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" name="cp" value="0">
							<span class="custom-control-label">No</span>
						</label>
						
						
					</div>
				</div>	
				<div class="card-body">
					<div class="btn-list">
						<input class="btn btn-success" type="submit" value="Guardar cuenta" />
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection