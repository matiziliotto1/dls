@extends('layouts.app')
@push('titulo')
Historial
@endpush
@push('css')
<link href="{{url('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{url('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{url('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css">
<style>
	.ajs-ok{
		border: none;
		background-color: green;
		color: white;
		border-radius: 5px;
	}
	.ajs-cancel{
		border: none;
		background-color: red;
		color: white;
		border-radius: 5px;
	}
	.ajs-message{color: white;}
	.label{color: white;font-weight: bold;font-size: 0.75em;}
	.label-warning{background-color: #a98307;padding: 2px 4px;border-radius: 5px;}
	.label-primary{background-color: #2271b3;padding: 2px 4px;border-radius: 5px;}
	.label-success{background-color: #00bb2d;padding: 2px 4px;border-radius: 5px;}
	.label-danger{background-color: #dc3545;padding: 2px 4px;border-radius: 5px;}
</style>
@endpush
@section('content')
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">Movimientos</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="example" class="table table-bordered key-buttons text-nowrap" >
						<thead>
							<tr>
								<th class="border-bottom-0">Fecha</th>
								<th class="border-bottom-0">Nombre</th>
								<th class="border-bottom-0">Moneda origen</th>
								<th class="border-bottom-0">Enviado</th>
								<th class="border-bottom-0">Moneda destino</th>								
								<th class="border-bottom-0">Esperado</th>
								<th class="border-bottom-0">T/C</th>
								<th class="border-bottom-0">Estado</th>
								<th class="border-bottom-0">Ver Todo</th>
							</tr>
						</thead>
						<tbody>
							@foreach($ls as $l)
							<tr id="{{$l->operacion_id}}">
								<td>{{$l->created_at}}</td>
								<td>{{$l->usuario->primernombre}} {{$l->usuario->segundonombre}} {{$l->usuario->primeroapellido}} {{$l->usuario->segundoapellido}}</td>
								<td>{{$l->monedae->nombre}}</td>
								<td>{{number_format($l->monto, 2, '.', ',')}}</td>
								<td>{{$l->monedad->nombre}}</td>
								<td>{{number_format($l->cambio, 2, '.', ',')}}</td>
								<td>{{$l->taza}}</td>
								@if(\Auth::User()->hasRole('Usuario'))
								<td>
									@if($l->estado==0) <span class="label label-warning">Enviado</span> @endif
									@if($l->estado==1) <span class="label label-primary">Procesando</span> @endif
									@if($l->estado==2) <span class="label label-success">Terminado</span> @endif
									@if($l->estado==3) <span class="label label-danger">Anulado</span> @endif
								</td>
								@endif
								@if(\Auth::User()->hasRole('Administrators'))
								<td>
									@if($l->estado==0) <a href="#" onclick="update({{$l->operacion_id}})" class="btn btn-warning">Enviado</a>@endif
									@if($l->estado==1) <a href="#" onclick="update({{$l->operacion_id}})" class="btn btn-primary">Procesando</a> @endif
									@if($l->estado==2) <a href="#" class="btn btn-success">Terminado</a> @endif
									@if($l->estado==3) <a href="#" class="btn btn-danger">Anulado</a> @endif
								</td>
								@endif
								<td>
									<a href="#" class="btn btn-warning" onclick="contenido({{$l->operacion_id}})" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i></a>
									@if(\Auth::User()->hasRole('Administrators'))
									
										@if($l->estado!=2)<a href="#" onclick="anular({{$l->operacion_id}})" class="btn btn-danger">Anular</a>@endif
									
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <div id="contenido"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>      
    </div>
  </div>

@endsection
@push('js')
<script src="{{url('assets/plugins/input-mask/jquery.mask.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
<script>
	var tabla;
	$(document).ready(function() {
		tabla = $('#example').DataTable( {
			lengthChange: false,
			buttons: [ 'copy', 'excel','pdf', 'colvis' ]
		});
		tabla.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
		tabla.column( '0:visible' ).order( 'desc' ).draw();
    });
    function contenido(i) {
		$("#contenido").load("foperacion/"+i);
	}
</script>
@if(\Auth::User()->hasRole('Administrators'))
<script>		
    function update(i) {
		alertify.confirm("Procesar Cuenta","¿Esta seguro con procesar esta cuenta?",
			function(){
				$.get("uoperacion/"+i,function(msg){
					var index = tabla.row($("tr[id=" + i + "]")).index();
					tabla.cell(index, 7).data(function (data, type, row, meta) {
	                    if (msg.estado === 0) { return '<a href="#" onclick="update('+msg.operacion_id+')" class="btn btn-warning">Enviado</a>' }
	                    if (msg.estado === 1) { return '<a href="#" onclick="update('+msg.operacion_id+')" class="btn btn-primary">Procesando</a>' }
	                    if (msg.estado === 2) { return '<a href="#" class="btn btn-success">Terminado</a>' }
	                });
					alertify.success("Proceso Terminado");
				});
			},
			function(){
				alertify.error("Cancelado la Eliminacion");
			}
		);
	}
	function anular(i) {
		alertify.confirm("Anular Transaccion","¿Esta seguro con anular esta transacción? No se podrá revertir esta acción",
			function(){
				$.get("doperacion/"+i,function(msg){
					var index = tabla.row($("tr[id=" + i + "]")).index();
					tabla.cell(index, 7).data(function (data, type, row, meta) {
	                    return '<a href="#" class="btn btn-danger">Anulado</a>';
	                });
					alertify.success(msg);
				});
			},
			function(){
				alertify.error("Cancelado la Anulación");
			}
		);
	}
</script>
@endif
@endpush