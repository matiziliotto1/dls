@extends('layouts.app')
@push('css')
<link href="{{url('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{url('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{url('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
@endpush

@push('titulo')
Lista de Usuarios
@endpush
@push('css')
<style>
	.label{color: white;font-weight: bold;font-size: 0.75em;}
	.label-warning{background-color: #a98307;padding: 2px 4px;border-radius: 5px;}
	.label-primary{background-color: #2271b3;padding: 2px 4px;border-radius: 5px;}
	.label-success{background-color: #00bb2d;padding: 2px 4px;border-radius: 5px;}
	.label-danger{background-color: #dc3545;padding: 2px 4px;border-radius: 5px;}
</style>
@endpush
@section('content')
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">Usuarios completos</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="example" class="table table-bordered key-buttons text-nowrap" >
						<thead>
							<tr>
								<th>Estado</th>
								<th class="border-bottom-0">Nombre</th>
								<th class="border-bottom-0">Apellido</th>
								<th class="border-bottom-0">Tipo</th>
								<th class="border-bottom-0">Número</th>
								<th class="border-bottom-0">Ocupación</th>
								<th class="border-bottom-0"></th>
								<th class="border-bottom-0">Dirección</th>
								<th></th>
								<th>Pref</th>
								<th class="border-bottom-0">Cuentas</th>
							</tr>
						</thead>
						<tbody>
							@foreach($ls as $l)
							<tr id="{{$l->usuario_id}}">
								<td>
									@if($l->user)										
										<span class="label label-{{$l->user->userlevel ? 'success':'warning'}}">{{$l->user->userlevel ? 'A':'I'}}</span>
									@else
										<span class="label label-danger">E</span>
									@endif
								</td>
								<td>{{$l->primernombre.' '.$l->segundonombre}}</td>
								<td>{{$l->primeroapellido.' '.$l->segundoapellido}}</td>
								<td>{{$l->tiposdocumento->nombre}}</td>
								<td>{{$l->nrodocumento}}</td>
								<td>{{$l->ocupacion->nombre}}</td>
								<td><a href="#" data-toggle="modal" data-target="#modal" onclick="Update({{$l->usuario_id}})"><i class="fas fa-plus-circle fa-2x"></i></a></td>
								<td>{{$l->Direccion}}</td>
								<td><a href="#" data-toggle="modal" data-target="#pref" onclick="Prefencia({{$l->usuario_id}})"> <i class="fas fa-hand-holding-usd fa-2x"></i> </a></td>
								<td>
									@if($l->user)										
										<span class="label label-{{$l->user->regdate ? 'success':'warning'}}">{{$l->user->regdate ? 'S':'N'}}</span>
									@else
										<span class="label label-danger">NE</span>
									@endif
								</td>
								<td> {{$l->cuenta->count()}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				Leyenda Estado Usuario: <span class="label label-success">A</span>: Activo, <span class="label label-warning">I</span>: Inactivo, <span class="label label-danger">E</span>: Eliminado <br>
				Leyenda Preferencial: <span class="label label-success">S</span>: Si, <span class="label label-warning">N</span>: No, <span class="label label-danger">NE</span>: No Existe <br>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="example-Modal3">Cambiar Datos Importantes</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="data">
					
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="pref" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="example-Modal3">Tipo de Cambio Prefencial</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="tipo">
					
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script src="{{url('assets/plugins/input-mask/jquery.mask.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
<script>
	var table = $('#example').DataTable( {
		lengthChange: false,
		buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
	});
	table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
	function Update(i){
		$("#data").load("{{url('pusuario')}}/"+i);
	}
	function dusuario(i) {
		$.get("{{url('dusuario')}}/"+i,function(e){
			$("#data").load("{{url('pusuario')}}/"+e);
		});
	}
	function Actuliza(i){
		$.get("{{url('ausuario')}}/"+i,{ps:$("#ps").val()},function(r){
			$("#msg").html(r);
			$("#msg").css('background-color','green');
		});
	}
	function Prefencia(i) {
		$("#tipo").load("{{url('putipo')}}/"+i);
	}
	$(document).on("submit","#tc",function (e) {
		e.preventDefault();
		$.get("{{url('uutipo')}}/"+$("#id").val(),$("#tc").serialize(),function(r){
			$("#msg").html("Actualizado Correctamente");
			$("#msg").css('background-color','green');
		});
	});
</script>
@endpush