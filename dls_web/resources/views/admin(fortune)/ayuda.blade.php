@extends('layouts.app')
@push('titulo')
Ayuda
@endpush
@push('css')
<link href="{{url('assets/plugins/accordion/accordion.css')}}" rel="stylesheet" />
@endpush
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Preguntas Frecuentes</h3>
			</div>
			<div class="card-body">
			
			<div id="accordion">
			
				<ul class="accordionjs m-0">
					<li>
						<div><h4>¿Cómo iniciar transacciones con nosotros?</h4></div>
						<div>
					Toda la información necesaria se encuentra aquí: <a href="http://sistema.fortuneonline.com.pe/">http://sistema.fortuneonline.com.pe/</a>
						</div>
					</li><br>
					<li>
						<div><h4>¿Qué requisitos necesito para operar con FORTUNE ONLINE?</h4></div>
						<div>
							Es necesario estar registrado en la plataforma de FORTUNE ONLINE y cumplir con todo lo necesario para la activación de la cuenta.
						</div>
					</li><br>
					
					<li>
						<div><h4>¿Cómo solicitar el cambio de divisas?</h4></div>
						<div>
							Toda la información necesaria se encuentra en el siguiente enlace: <a href="http://sistema.fortuneonline.com.pe/">http://sistema.fortuneonline.com.pe/</a>
						</div>
					</li><br>
					
					
					<li>
						<div><h4>¿Cuánto cuesta usar FORTUNE ONLINE?</h4></div>
						<div>
							Nosotros no cobramos ninguna comisión por el uso de nuestros servicios. Sin embargo, se debe tener en cuenta que la entidad financiera puede realizar cobros por comisiones (ej. Comisión interbancaria) o impuestos (ej. ITF), ello le será comunicado oportunamente. Estos cargos adicionales, en caso de existir, serán asumidos por usted adicionalmente al tipo de cambio aplicado a la operación. Para mayor información ver los Términos y Condiciones o consultar con un asesor a través de los medios de comunicación establecidos.
						</div>
					</li><br>
					
					<li>
						<div><h4>¿Cómo cancelo una operación?</h4></div>
						<div>
							En caso quiera cancelar una solicitud de cambio de divisas antes de que se efectúe la transferencia de sus fondos a nuestras cuentas bancarias, lo puede hacer a través de la misma plataforma o a través de nuestros asesores sin costo alguno. En caso quiera cancelar la operación después de transferidos sus fondos pero antes de que el dinero cambiado se transfiera a la cuenta de destino indicado por usted, lo podrá hacer pero es posible que se le aplique un cargo adicional por la cancelación de la transferencia y/o devolución del monto abonado, conforme con lo señalado en los Términos y Condiciones. Dichos cargos adicionales le serán comunicados de manera oportuna.
						</div>
					</li><br>
				
				
				<li>
						<div><h4>¿Cómo realizo seguimiento a mi operación?</h4></div>
						<div>
							Todas las operaciones que haya realizado quedarán registradas en su cuenta, también podrá consultar con algunos de nuestros asesores sobre el estado de la operación. Además, será notificado vía correo electrónico cuando se realice con éxito cada transacción.
						</div>
					</li><br>
				
				
					<li>
						<div><h4>¿Aceptan efectivo o cheques?</h4></div>
						<div>
							Las transferencias sólo se realizan entre cuentas bancarias y vía online. En caso quiera utilizar otros medios de pago para acceder al servicio de cambio de divisas, deberá comunicarse previamente con alguno de nuestros asesores.
						</div>
					</li><br>
					
					
					<li>
						<div><h4>¿Es seguro Fortune online?</h4></div>
						<div>
							¿Qué tan seguro es usar FORTUNE ONLINE para cambiar dinero online?
							La seguridad de las transacciones está respaldada por los mecanismos de seguridad que las entidades financieras utilizan en la prestación de sus servicios de depósitos y transferencia de fondos. Asimismo, somos una empresa constituida formalmente según consta en la Partida Registral N° 14202676 y estamos obligados a reportar a la Unidad de Inteligencia Financiera (UIF) cualquier operación sospechosa que pueda constituir o estar relacionada al lavado de activos y financiamiento del terrorismo, habiendo implementado mecanismos para prevenir la comisión de dichos delitos a través de nuestra plataforma. Finalmente, FORTUNE ONLINE realiza constantemente el seguimiento de las operaciones realizadas y mantiene su plataforma actualizada y efectúa monitoreos permanentemente.

						</div>
					</li><br>
					
					<li>
						<div><h4>¿Qué significa que FORTUNE ONLINE esté registrada en la SBS y que cuente con un Oficial de Cumplimiento?</h4></div>
						<div>
							FORTUNE ONLINE se encuentra registrada como empresa de cambio de divisas ante la Superintendencia de Banca, Seguros y AFP (SBS), así como cuenta con un Oficial de Cumplimiento para la prevención de delitos de lavado de activos y financiamiento del terrorismo. Esto significa que FORTUNE ONLINE es una empresa que cumple con todas las disposiciones regulatorias que le son aplicables, preocupándose por el respeto a las leyes y por su buena reputación en el mercado.

						</div>
					</li><br>
					
					<li>
						<div><h4>¿Mi información personal está protegida?</h4></div>
						<div>
							La información personal a la que tiene acceso FORTUNE ONLINE se encuentra protegida en cumplimiento de la normativa sobre protección de datos personales y la Política de Privacidad de FORTUNE ONLINE. Para más información, puede leer la Política de Privacidad aquí.

						</div>
					</li><br>
				
				<li>
						<div><h4>¿Cómo modifico los datos ingresados?</h4></div>
						<div>
							En caso requiera realizar algún cambio o precisión respecto a los datos ingresados al momento de su registro, lo puede realizar directamente por medio de la plataforma o también puede comunicarse con el equipo de soporte de FORTUNE ONLINE para que realicen el cambio. Para mayor información comuníquese enviando un correo a  registros@fortuneonline.com.pe

						</div>
					</li><br>
					
					
				<li>
						<div><h4>¿Cómo cambio mi correo electrónico?</h4></div>
						<div>
							Lo puede realizar directamente a través de la plataforma o también puede comunicarse con el equipo de soporte de FORTUNE ONLINE para que realicen el cambio.

						</div>
					</li><br>	
					
					
					<li>
						<div><h4>¿Cómo cambio los datos del destinatario?</h4></div>
						<div>
							Si ha ingresado erróneamente los datos del destinatario, puede modificarlo directamente a través de la plataforma o también puede comunicarse con alguno de nuestros asesores a fin de que pueda modificarlos por usted.

						</div>
					</li><br>	
					
					<li>
						<div><h4>¿Olvidó su contraseña?</h4></div>
						<div>
							Puede crear una nueva contraseña dándole clic a “olvidé mi contraseña”. Recibirá un correo electrónico con las instrucciones a seguir para restablecer su contraseña.

						</div>
					</li><br>	
					
					<li>
						<div><h4>¿Qué es mi historial de operaciones?</h4></div>
						<div>
							Es el registro de todas las operaciones de cambio de divisas que ha realizado usando su cuenta. Puede verificarlo a través de la plataforma y, en caso sea necesario, requerir mayor información a nuestros asesores: registros@fortuneonline.com.pe

						</div>
					</li><br>	
					
					<li>
						<div><h4>¿Cuál es el tiempo estimado de transferencia?</h4></div>
						<div>
							La transferencia a la cuenta bancaria de destino puede variar dependiendo de la entidad financiera, si se trata de una transferencia interbancaria o interbancaria, y/o si se trata de una transferencia a cuentas bancarias de destino que se encuentran en otras ciudades del Perú. El tiempo estimado de demora le será informado oportunamente o puede consultarlo directamente con uno de nuestros asesores. Usted recibirá una comunicación por parte de FORTUNE ONLINE una vez que la transferencia bancaria a la cuenta de destino se haya realizado con éxito o si la transferencia ha sido rechazada por la entidad financiera de destino.

						</div>
					</li><br>	
					
					
						<li>
						<div><h4>¿Cuál es la prueba de que se realizó la transferencia a la cuenta de destino?</h4></div>
						<div>
						FORTUNE ONLINE le enviará por correo electrónico el número de operación mediante el cual se realizó la transferencia a la cuenta de destino o mediante  whatsapp, así como otro tipo de constancia de ser necesario.

						</div>
					</li><br>	
					
					
					<li>
						<div><h4>¿Puedo saber más sobre la transferencia de fondos?</h4></div>
						<div>
						Sobre las tarifas interbancarias
					FORTUNE ONLINE  podrá cargar adicionalmente los costos por transferencias interbancarias, en caso sea aplicado por la entidad financiera. Estos se detallarán automáticamente al momento de realizar la transacción y deberán ser asumidos por el usuario.<br>
					Sobre el tiempo estimado de los bancos<br>
					El tiempo estimado que demora la transferencia está sujeto al tiempo que demoren en llegar los fondos transferidos a las cuentas bancarias de FORTUNE ONLINE. Cabe mencionar que FORTUNE ONLINE no realizará ninguna transferencia sin antes haber recibido los fondos por parte del usuario. En la mayoría de los casos, el tiempo total para completar una operación en la plataforma de FORTUNE ONLINE no es mayor a dos días hábiles.<br>
					
					Sobre la confirmación del depósito<br>
					La confirmación del depósito se dará en cuanto recibamos los fondos en nuestras cuentas bancarias mediante el envío de un correo electrónico o por medio de un aviso a través de la plataforma o a través de nuestros asesores.


						</div>
					</li><br>


	<li>
						<div><h4>¿Cómo manejan el tipo de cambio?</h4></div>
						<div>
						¿Qué es el tipo de cambio garantizado?
Es el precio  pactado por cada divisa, con el cual podremos  definir el monto que se transferirá a la cuenta bancaria de destino. Este tipo de cambio está garantizado por un tiempo de 15 minutos luego de colocada la solicitud de cambio de divisas a través de la plataforma, tiempo durante el cual el usuario deberá realizar la transferencia a las cuentas de FORTUNE ONLINE y enviar el imagen o fotografía de la confirmación de la transacción bancaria y/o el código de operación asignado para la transferencia bancaria efectuada.



						</div>
					</li><br>						
					
					
					<li>
						<div><h4>¿Cuál es el mejor momento para la compra y/o venta de dólares?</h4></div>
						<div>
						En FORTUNE ONLINE contamos con alertas de tipo de cambio que notifican cuál es el mejor momento para la compra y/o venta de dólares. Cada usuario podrá personalizar estas alertas a su gusto. Asimismo, también puede consultarse esta información a nuestros asesores.



						</div>
					</li><br>	
					
					<li>
						<div><h4>¿Qué es el tipo de cambio preferencial?</h4></div>
						<div>
						FORTUNE ONLINE podrá aplicar un tipo de cambio preferencial a operaciones de cambio de divisas por montos considerables. Para más información sobre el tipo de cambio preferencial aplicable, deberá realizar una consulta a alguno de nuestros asesores.



						</div>
					</li><br>	
					
					
						<li>
						<div><h4>¿Cómo me contacto con ustedes?</h4></div>
						<div>
						Para cualquier información adicional, duda o comentario, comunicarse con alguno de nuestros asesores a través de los siguientes medios de comunicación:<br>
						Whatsapp: +51 946 091 321<br>
						Correo electrónico: cambios@fortuneonline.com.pe<br>
						Teléfono: +51 946 091 321<br>
						Asimismo, puede acceder y leer a los Términos y Condiciones y la Política de Privacidad para complementar la información contenida en estas Preguntas Frecuentes.




						</div>
					</li><br>	
					
				</ul>

			</div>
			
			
		</div>
	</div>
</div>
@endsection
@push('js')
<script src="{{url('assets/plugins/accordion/accordion.min.js')}}"></script>
<script src="{{url('assets/plugins/accordion/accor.js')}}"></script>
@endpush