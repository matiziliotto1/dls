@extends('layouts.app')

@push('titulo_completo')
	Bienvenido a DLS Perú
@endpush

@push('css')
	{{-- <link href="{{url('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
	<link href="{{url('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
	<link href="{{url('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
	
	<style>
		.ajs-ok{
			border: none;
			background-color: green;
			color: white;
			border-radius: 5px;
		}
		.ajs-cancel{
			border: none;
			background-color: red;
			color: white;
			border-radius: 5px;
		}
		.label{color: white;font-weight: bold; font-size: 0.75em;}
		.label-warning{background-color: #a98307;padding: 2px 4px;border-radius: 5px;}
		.label-primary{background-color: #2271b3;padding: 2px 4px;border-radius: 5px;}
		.label-success{background-color: #00bb2d;padding: 2px 4px;border-radius: 5px;}
		.label-danger{background-color: #dc3545;padding: 2px 4px;border-radius: 5px;}
	</style> --}}

	<link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
@endpush

@section('content')
	@if(\Auth::User()->hasRole('Administrators'))
		@php 
			$ls=\App\Modelo\Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->get();
		@endphp
	@else
		@php
			$ls=\App\Modelo\Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("usuario_id",\Auth::User()->usuario_id)->get();
		@endphp
	@endif
	<!--page header-->
	<div class="row">
		<div class="col-xl-3 col-lg-6 col-md-12 col-sm-6">
			<div class="card">
				<div class="card-body d-flex">
					<img src="{{asset('assets/images/svgs/png/cambio.png')}}" alt="img" class="w-8 h-8">
					<div class="svg-icons float-right text-right ml-auto">
						<h3 class="mb-3 font-weight-extrabold">Tipo de cambio</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-xl-3 col-lg-6">
			<div class="card bg-success text-center">
				<div class="card-body">
					<p class="mb-0 text-white-50">COMPRA</p>
					<i class="fas fa-arrow-alt-circle-up text-green mr-1"></i><h2 class=" mb-0 pure">0.00</h2>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-xl-3 col-lg-6">
			<div class="card bg-info text-centerblanco">
				<div class="card-body">
					<p class="mb-0 text-whiteblanco-50">VENTA</p>
					<i class="fas fa-arrow-alt-circle-down text-red mr-1"></i><h2 class=" mb-0 sale">0.00</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-12 col-lg-12">
			<div class="card overflow-hidden">
				<div class="card-header">
					<h4> Tipo de cambio por mes</h4>
				</div>
				<div class="card-body">
					{{-- //TODO: esto queda asi?? --}}
					<div id="chartArea2" class="w-100 overflow-hidden"></div>
					{{-- <canvas id="chart" class="h-350"></canvas> --}}
				</div>
			</div>
		</div>
</div>

@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
	@if(\Auth::User()->regdate==1)
		@php
			$dc = \Auth::User()->timestamp;
			$dv = \Auth::User()->previous_visit;
		@endphp
		<script>
			var dc = eval({{$dc}});
			var dv= eval({{$dv}});
			var cam=0.00;
			var cron=900;
			var pref=true;
			$(".pure").html(dc);
			$(".sale").html(dv);
			//timestamp dc
			//previous_visit dv
			//regdate 1,0
		</script>
	@else
		<script>
			var dc=0.00;
			var dv=0.00;
			var cam=0.00;
			var cron=900;
			var pref=false;
			$(document).ready(function(){
				$.get("ftipocambio",function (msg) {
					dc=eval(msg.compra);
					dv=eval(msg.venta);
					$(".pure").html(dc);
					$(".sale").html(dv);
				});
				function tipocambio() {
					if(cron===0){
						$.get("ftipocambio",function (msg) {
							dc=eval(msg.compra);
							dv=eval(msg.venta);
							$(".pure").html(dc);
							$(".sale").html(dv);                    
							let img = "{{asset('assets/images/tiempo.jpg')}}";
							alertify.alert('', '<img src="'+ img +'">', function(){
								alertify.success('Ok');
								location.reload();
							});
							clearInterval(intervalo);
						});
					}else{
						cron-=1;
						var tim=parseInt(cron/60)+":"+(cron%60);
						$('.cronometro').html(tim);
					}
				}
				var intervalo = setInterval(tipocambio, 1000);
			});
		</script>
	@endif

	<script src="{{url('assets/plugins/chart.js/chart.min.js')}}"></script>
	<script src="{{url('assets/plugins/chart.js/chart.extension.js')}}"></script>
	<script>
		"use strict";

		function grafica(){
			var ctx = document.getElementById( "chart" );
			var myChart = new Chart( ctx, {
				type: 'line',
				data: {
					labels: dlabel,
					datasets: [
						{
							label: "Compra",
							data: dcompra,
							borderColor: "blue",
							borderWidth: "1",
							pointRadius:"2",
							pointBorderColor: 'transparent',
							pointBackgroundColor: 'blue',
							backgroundColor: "rgb(0,0,255,0.05)"
						},{
							label: "Venta",
							data: dventa,
							borderColor: "red",
							borderWidth: "1",
							pointRadius:"2",
							pointBorderColor: 'transparent',
							pointBackgroundColor: 'red',
							backgroundColor: "rgb(255,0,0,0.05)"
						}]
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					barRoundness: 1,
					scales: {
						yAxes: [ {
							ticks: {
								beginAtZero: true
								}
							}]
					}
				}
			});
		}
	</script>
@endsection