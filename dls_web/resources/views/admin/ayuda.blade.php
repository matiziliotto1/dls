@extends('layouts.app')

@push('css')
    <!-- select2 Plugin -->
    <link href="{{asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
    <!--mutipleselect css-->
    <link href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    ¿Necesitas ayuda?
@endpush

@push('titulo')
    Ayuda
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Preguntas Frecuentes</h3>
                </div>

                <div class="card-body">
                    <div id="accordion" class="w-100 ">
                        <div class="card mb-0 border">
                            <div class="accor bg-primary" id="headingOne">
                                <h5 class="m-0">
                                    <a href="#collapseOne" class="text-white" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
                                    Pregunta #1
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful
                                </div>
                            </div>
                        </div>

                        <div class="card mb-0 border">
                            <div class="accor  bg-primary" id="headingTwo">
                                <h5 class="m-0">
                                    <a href="#collapseTwo" class="collapsed text-white" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                                        Pregunta #2
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse b-b0" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae
                                </div>
                            </div>
                        </div>

                        <div class="card border mb-0">
                            <div class="accor  bg-primary" id="headingThree">
                                <h5 class="m-0">
                                    <a href="#collapseThree" class="collapsed text-white" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
                                        Pregunta #3
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse b-b0" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

@endsection