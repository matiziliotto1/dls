@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else

@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)

<p style="font-size: 20px; font-family: Lato; color: #4d4d4d; margin: 0px; line-height: 1.5em; text-align:center;">
    {{ $line }}
</p>
@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
<p style="font-size: 16px; font-family: Lato; color: #4d4d4d; margin: 0px; line-height: 1.5em; text-align:center;">
    {{ $line }}
</p>
@endforeach

<a href="#"><img src="http://dls.com.pe/app/img_mail/presentacion.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/presentacion.jpg" alt="DLS"></a>
{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Si no puede verificar con el bot贸n \":actionText\",".
    'copia y pega la siguiente ruta e ingreselo en su navegador: [:actionURL](:actionURL)',
    [
        'actionText' => $actionText,
        'actionURL' => $actionUrl,
    ]
)
@endslot
@endisset

@endcomponent
