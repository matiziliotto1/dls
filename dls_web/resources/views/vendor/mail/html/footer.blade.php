<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <TR>
                <TD bgColor=#ffffff vAlign=top width=240>&nbsp;</TD>
                <TD bgColor=#ffffff vAlign=top width=248>
                    <SPAN style="PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px"><SPAN style="FONT-SIZE: 27px; FONT-FAMILY: Lato; COLOR: #000; MARGIN: 0px"><A href="https://dls.com.pe/"><IMG border=0 hspace=0 alt=web src="https://www.dls.com.pe/newsletter/junio/16/images/web.png" align=baseline></A></SPAN></SPAN>
                </TD>
                <TD bgColor=#ffffff vAlign=top width=187>
                    <TABLE cellSpacing=0 cellPadding=5 align=center>
                        <TR vAlign=middle>
                            <TD align=right>
                                <P style="FONT-FAMILY: Lato; COLOR: #666; LINE-HEIGHT: 14px; MARGIN-RIGHT: 5px">Síguenos en:</P>
                            </TD>
                            <TD>
                                <A href="https://www.facebook.com/dlsperuoficial/">
                                    <IMG border=0 hspace=0 alt="" src="https://www.dls.com.pe/newsletter/junio/16/images/facebook.png">
                                </A>
                            </TD>
                            <TD>
                                <A href="https://www.instagram.com/dlsperuoficial/">
                                    <IMG border=0 hspace=0 alt="" src="https://www.dls.com.pe/newsletter/junio/16/images/instagram.png">
                                </A>
                            </TD>
                        </TR>
                    </TABLE>
                </TD>
            </TR>
        </table>
    </td>
</tr>

<a href="#"><img src="http://dls.com.pe/app/img_mail/dlspie.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/dlspie.jpg" alt="DLS"></a>