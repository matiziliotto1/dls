
<!DOCTYPE HTML>
<html lang="en">

<head>
	<title>DLS - Casa de cambio Online</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="{{asset('assets/images/brand/favicon.ico')}}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/brand/favicon.ico')}}" />

	<!-- Dashboard Css -->
	<link href="{{asset('assets/css/registro.css')}}" rel="stylesheet" />
	<!--Font Awesome-->
    <link href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome-free/css/font-awesome.min.css')}}" rel="stylesheet">

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext" rel="stylesheet">
	<!-- //web-fonts -->
	<link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
</head>

<body>
	<div class="main-bg" style="background: url({{asset('assets/images/b4.jpg')}}) no-repeat center;">
		<!-- title -->
		<h1>&nbsp; </h1>
		<!-- //title -->
		<div class="sub-main-w3">
			<div class="image-style" style="background: url({{asset('assets/images/m.jpg')}}) no-repeat center;">

            </div>

			<!-- vertical tabs -->
			<div class="vertical-tab">
				<div id="section1" class="section-w3ls">
					<input type="radio" name="sections" id="option1" checked>
					<label for="option1" class="icon-left-w3pvt"><span class="fa fa-user-circle" aria-hidden="true"></span>Ingreso</label>
					<article>
						<form method="POST" action="{{ route('login') }}">
                            @csrf
							<h3 class="legend">Usa tu cuenta para iniciar sesión</h3>
							<div class="input">
                                <span class="fa fa-envelope-o" aria-hidden="true"></span>
                                <input  type="email" class="@error('email') is-invalid @enderror" id="email_ingreso" value="{{ old('email') }}" autocomplete="email" placeholder="Email" autofocus>
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="input">
                                <span class="fa fa-key" aria-hidden="true"></span>
                                <input type="password" class="@error('password') is-invalid @enderror" id="password_ingreso" autocomplete="current-password" placeholder="Contraseña">
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<button type="submit" class="btn submit">Ingresar</button>
							<a class="bottom-text-w3ls" style="cursor: pointer;" onclick="goToSeccion3();">¿No recuerda su contraseña?</a>
						</form>
					</article>
                </div>

				<div id="section2" class="section-w3ls">
					<input type="radio" name="sections" id="option2">
					<label for="option2" class="icon-left-w3pvt"><span class="fa fa-pencil-square" aria-hidden="true"></span>Registro</label>
					<article>
						<form method="POST" action="{{ route('register') }}">
                            @csrf
							<h3 class="legend">Crear una cuenta</h3>

							<div class="input">
                                <span class="fa fa-envelope-o" aria-hidden="true"></span>
                                <input type="email" class="@error('email') is-invalid @enderror" id="email_registro" value="{{ old('email') }}" autocomplete="email" placeholder="Email" autofocus>
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>

							<div class="input">
                                <span class="fa fa-user-circle" aria-hidden="true"></span>
                                <input type="text" class="@error('username') is-invalid @enderror" id="username_registro" value="{{ old('username') }}" autocomplete="name" placeholder="Alias" autofocus>
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>

							<div class="input">
                                <span class="fa fa-key" aria-hidden="true"></span>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password_registro" autocomplete="password" placeholder="Contraseña">
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="input">
                                <span class="fa fa-key" aria-hidden="true"></span>
                                <input id="password-confirm_registro" type="password" class="form-control" autocomplete="new-password" placeholder="Confirmar Contraseña">
							</div>
							<button type="submit" class="btn submit">Crear cuenta</button>
						</form>
					</article>
                </div>

				<div id="section3" class="section-w3ls">
					<input type="radio" name="sections" id="option3">
					<label for="option3" class="icon-left-w3pvt"><span class="fa fa-lock" aria-hidden="true"></span>¿Perdió su contraseña?</label>
					<article>
						<form method="POST" action="{{ route('password.email') }}" id="form_reset_password">
                            @csrf
							<h3 class="legend last">Restablecer contraseña</h3>
							<p class="para-style">Por favor, ingrese su correo electrónico y le enviaremos un correo que le permitirá restablecer su contraseña.</p>
							<p class="para-style-2"><strong>Recuerde</strong> ingresar el correo con el cual creo la cuenta en nuestro sistema.</p>
							<div class="input">
                                <span class="fa fa-envelope-o" aria-hidden="true"></span>
                                <input id="email_reset" type="email" class="@error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email">
                                
                                {{-- //TODO: preguntar por los mensajes de error. --}}
                                @error('email_reset')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                {{-- <span class="valid-feedback" role="alert" id="alert_recuperacion" style="display:none;">
                                    <strong>Hemos enviado un link de recuperación a su correo electrónico</strong>
                                </span> --}}
							</div>
							
							<button type="submit" class="btn submit last-btn">Restablecer contraseña</button>
						</form>
					</article>
				</div>
            </div>

			<!-- //vertical tabs -->
			<div class="clear">

            </div>
		</div>
	</div>


	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
	<script type="text/javascript">

		$( document ).ready(function() {
			habilitarSeccion1();
		});

		$("input[name='sections'").change(function() {

			//this.id = option1(2|3) -> id_section = 1 || 2 || 3
			let id_section = this.id.substring(6);

			switch (id_section){
				case "1":
					habilitarSeccion1();
					deshabilitarSeccion2();
					deshabilitarSeccion3();
					break;
				case "2":
					habilitarSeccion2();
					deshabilitarSeccion1();
					deshabilitarSeccion3();
					break;
				case "3":
					habilitarSeccion3();
					deshabilitarSeccion1();
					deshabilitarSeccion2();
					break;
			}
		});

		function deshabilitarSeccion1(){
			$("#option1").removeAttr("checked");

			$('#email_ingreso').removeAttr("required");
			$('#password_ingreso').removeAttr("required");

			$("#email_ingreso").removeAttr("name");
			$("#password_ingreso").removeAttr("name");
		}
		function deshabilitarSeccion2(){
			$("#option2").removeAttr("checked");

			$('#email_registro').removeAttr("required");
			$('#username_registro').removeAttr("required");
			$('#password_registro').removeAttr("required");
			$('#password-confirm_registro').removeAttr("required");

			$("#email_registro").removeAttr("name");
			$("#username_registro").removeAttr("name");
			$("#password_registro").removeAttr("name");
			$("#password-confirm_registro").removeAttr("name");
		}
		function deshabilitarSeccion3(){
			$("#option3").removeAttr("checked");
			$('#email_reset').removeAttr("required");

			$("#email_reset").removeAttr("name");
		}

		function habilitarSeccion1(){
			$("#option1").attr("checked", 'checked');

			$("#email_ingreso").attr("required", true);
			$("#password_ingreso").attr("required", true);

			$("#email_ingreso").attr("name", "email");
			$("#password_ingreso").attr("name", "password");
		}
		function habilitarSeccion2(){
			$("#option2").attr("checked", 'checked');

			$("#email_registro").attr("required", true);
			$("#username_registro").attr("required", true);
			$("#password_registro").attr("required", true);
			$("#password-confirm_registro").attr("required", true);

			$("#email_registro").attr("name", "email");
			$("#username_registro").attr("name", "username");
			$("#password_registro").attr("name", "password");
			$("#password-confirm_registro").attr("name", "password_confirmation");
		}
		function habilitarSeccion3(){
			$("#option3").attr("checked", 'checked');

			$("#email_reset").attr("required", true);

			$("#email_reset").attr("name", "email");
		}

		function goToSeccion3(){
			$("#option1").removeAttr("checked");

			$("#option3").attr("checked", 'checked');

			habilitarSeccion3();
			deshabilitarSeccion1();
			deshabilitarSeccion2();
		}
		
		$( "#form_reset_password" ).submit(function( event ) {
			event.preventDefault();
            token = "{{csrf_token()}}";
            url = "{{ route('password.email') }}";
			$.ajax({
				type:"POST",
				url: url,
				data:{
					_token: token,
					email: $("#email_reset").val()
				},
				beforeSend: function(data){
					alertify.success("Hemos enviado un link de recuperación a su correo electrónico");
					// $("#alert_recuperacion").removeAttr("style");
				},
			});
		});
	</script>
</body>
</html>