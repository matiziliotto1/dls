<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth','usuario','verified','empresa'])->group(function () {
	Route::post('/soperacion', 'Control\COperacion@soperacion')->name('soperacion');
});

Route::middleware(['auth','admin','verified','empresa'])->group(function () {
	Route::get('/uoperacion/{i}', 'Control\COperacion@uoperacion')->name('uoperacion');
	Route::get('/doperacion/{i}', 'Control\COperacion@doperacion')->name('doperacion');
});

Route::middleware(['auth','verified','empresa'])->group(function () {
	Route::get('/foperacion/{i}', 'Control\COperacion@foperacion')->name('foperacion');
});

Route::middleware(['auth','verified','admin'])->group(function () {
	Route::post('/svoucher', 'Control\COperacion@svoucher')->name('svoucher');
});
?>