<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Modelo\Operacion;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
	if(Auth::check()){
		return redirect('home');
	}
	else{
		return redirect('auth');
	}
});

Route::get("/auth",function(){
	if(Auth::check()){
		return redirect('home');
	}
	else{
		return view('auth.auth');
	}
});

require_once('ruta/usuario.php');
require_once('ruta/cambio.php');
require_once('ruta/cuenta.php');
require_once('ruta/operacion.php');

Route::middleware(['auth','empresa'])->group(function () {
	Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
	Route::get('/ayuda', 'Control\Vista@ayuda')->name('ayuda')->middleware('verified');
	Route::get('/perfil', 'Control\Vista@perfil')->name('perfil');
	Route::get('/cuentasbancarias', 'Control\Vista@cuentasbancarias')->name('cuentasbancarias')->middleware('verified');
	Route::get('/usuario', 'Control\Vista@usuario')->name('usuario');
	Route::get('/personal_o_empresa', 'Control\Vista@personal_o_empresa')->name('personal_o_empresa');
	Route::get('/usuario_empresa', 'Control\Vista@usuario_empresa')->name('usuario_empresa');
});
Route::middleware(['auth','admin','verified','empresa'])->group(function () {
	Route::get('/reportesbs', 'Control\Vista@reportesbs')->name('reportesbs');
	Route::get('/reporte', 'Control\Vista@reporte')->name('reporte');
	Route::get('/empresa', 'Control\Vista@empresa')->name('empresa');
	Route::get('/tipocambio', 'Control\Vista@tipocambio')->name('tipocambio');
});
Route::middleware(['auth','usuario','verified','empresa'])->group(function () {		
	Route::get('/operacion', 'Control\Vista@operacion')->name('operacion');
	Route::get('/historial', 'Control\Vista@historial')->name('historial');
});
Auth::routes(['verify' => true]);

Route::get("/sbs",function(){
	$ls=\App\Modelo\Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("estado",2)->get();
	return $ls;
});

Route::middleware(['auth','verified','admin'])->group(function () {
	Route::get('/noperacionyvouchers/{id}', function($id){
		$ope = Operacion::select('voucher', 'voucher2', 'voucher3', 'voucher4', 'num_ope', 'num_ope2', 'num_ope3', 'num_ope4')->find($id);

		return view("admin.noperacionesyvouchers", compact("ope"));
	})->name('noperacionyvouchers');
});