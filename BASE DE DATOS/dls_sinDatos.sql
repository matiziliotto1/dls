-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 04-08-2020 a las 10:38:33
-- Versión del servidor: 5.6.41-84.1
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dlscompe_app`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `active_guests`
--

CREATE TABLE `active_guests` (
  `ip` varchar(15) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `active_guests`
--

INSERT INTO `active_guests` (`ip`, `timestamp`) VALUES
('190.236.197.122', 1568230037);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `active_users`
--

CREATE TABLE `active_users` (
  `username` varchar(30) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `banco_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`banco_id`, `nombre`) VALUES
(1, 'Banco de Comercio'),
(2, 'BCP'),
(3, 'Banco Pichincha'),
(4, 'BBVA'),
(5, 'Interbank'),
(6, 'MiBanco'),
(7, 'Scotiabank Perú'),
(8, 'Banco GNB Perú'),
(9, 'Banco Falabella'),
(10, 'Banco Ripley'),
(11, 'BanBif'),
(12, 'Citibank Perú'),
(13, 'Banco Santander Perú'),
(14, 'Banco Azteca'),
(15, 'Caja Arequipa'),
(16, 'Cusco'),
(17, 'Caja Trujillo'),
(19, 'Caja Huancayo'),
(20, 'Caja Ica'),
(21, 'Caja Maynas'),
(22, 'Caja Paita'),
(23, 'Caja Tacna'),
(24, 'Caja Sullana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banlist`
--

CREATE TABLE `banlist` (
  `ban_id` mediumint(8) UNSIGNED NOT NULL,
  `ban_username` varchar(255) NOT NULL,
  `ban_userid` mediumint(8) UNSIGNED NOT NULL,
  `ban_ip` varchar(40) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE `configuration` (
  `config_name` varchar(20) NOT NULL,
  `config_value` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuration`
--

INSERT INTO `configuration` (`config_name`, `config_value`) VALUES
('ACCOUNT_ACTIVATION', '2'),
('ALL_LOWERCASE', '0'),
('COOKIE_EXPIRE', '100'),
('COOKIE_PATH', '/'),
('DATE_FORMAT', 'M j, Y, g:i a'),
('EMAIL_FROM_NAME', 'DLS Perú'),
('EMAIL_FROM_ADDR', 'operaciones@dls.com.pe'),
('EMAIL_WELCOME', '1'),
('ENABLE_CAPTCHA', '0'),
('GUEST_TIMEOUT', '5'),
('HASH', 'sha256'),
('home_page', 'index.php'),
('login_page', '../usuario/ingreso.php'),
('max_user_chars', '36'),
('min_user_chars', '5'),
('max_pass_chars', '120'),
('min_pass_chars', '8'),
('NO_ADMIN_REDIRECT', '1'),
('record_online_date', '1567089258'),
('record_online_users', '7'),
('SITE_DESC', 'DLS Perú'),
('SITE_NAME', 'DLS Perú'),
('TRACK_VISITORS', '1'),
('TURN_ON_INDIVIDUAL', '1'),
('USER_HOME_PATH', '/'),
('HOME_SETBYADMIN', '0'),
('USERNAME_REGEX', 'letter_num_spaces'),
('USER_TIMEOUT', '10'),
('Version', '2.4'),
('WEB_ROOT', 'http://fortuneonline.com.pe/test/admin/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentabancaria`
--

CREATE TABLE `cuentabancaria` (
  `cuentabancaria_id` int(11) NOT NULL,
  `usuario_id` int(255) DEFAULT NULL,
  `banco_id` int(255) DEFAULT NULL,
  `tipocuenta_id` int(255) DEFAULT NULL,
  `moneda_id` int(255) DEFAULT NULL,
  `nrocuenta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nrocuentacci` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuentapropia` tinyint(1) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiposdocumento_id` int(11) DEFAULT NULL,
  `nro_documento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autorizo_deposito` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `departamento_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE `distrito` (
  `distrito_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `group_id` smallint(5) UNSIGNED NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `group_level` tinyint(3) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `group_level`) VALUES
(1, 'Administrators', 1),
(3, 'Usuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_table`
--

CREATE TABLE `log_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` int(11) UNSIGNED NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `ip` varchar(15) NOT NULL,
  `log_operation` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mes`
--

CREATE TABLE `mes` (
  `mes_id` int(11) NOT NULL,
  `Enero` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mes`
--

INSERT INTO `mes` (`mes_id`, `Enero`) VALUES
(1, 'Febrero'),
(2, 'Marzo'),
(3, 'Abril'),
(4, 'Mayo'),
(5, 'Junio'),
(6, 'Julio'),
(7, 'Agosto'),
(8, 'Septiembre'),
(9, 'Octubre'),
(10, 'Noviembre'),
(11, 'Diciembre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `moneda_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `moneda`
--

INSERT INTO `moneda` (`moneda_id`, `nombre`) VALUES
(1, 'S/ soles'),
(2, '$ dólares');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupacion`
--

CREATE TABLE `ocupacion` (
  `ocupacion_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ocupacion`
--

INSERT INTO `ocupacion` (`ocupacion_id`, `nombre`) VALUES
(1, 'Trabajador(a) Independiente'),
(3, 'Estudiante'),
(4, 'Jubilado(a)'),
(5, 'Obrero(a)'),
(6, 'Miembro de las fuerzas armadas/ miembro del clero'),
(2, 'Dependiente'),
(7, 'Ama de Casa'),
(8, 'Empleador(a)'),
(9, 'Desempleado'),
(10, 'Trabajador(a) del hogar'),
(11, 'No Declara');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacion`
--

CREATE TABLE `operacion` (
  `operacion_id` int(11) NOT NULL,
  `cuentabancariae_id` int(11) NOT NULL,
  `cuentabancariad_id` int(11) NOT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `moneda_id` int(11) NOT NULL,
  `tmoneda_id` int(11) NOT NULL,
  `cuentabancariat_id` int(11) DEFAULT NULL,
  `num_ope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_ope2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_ope3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_ope4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cambio` decimal(10,2) DEFAULT NULL,
  `taza` decimal(10,4) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` int(11) DEFAULT '0',
  `last_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `pais_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`pais_id`, `nombre`) VALUES
(1, 'Perú'),
(2, 'Argentina'),
(3, 'Brasil'),
(4, 'Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('yurac@hotmail.com', '$2y$10$Qo2GDbPpeymkW7U9rdVIIOpapg4ja.yPHHVzzz7n8l3G.VaCxZCHq', '2019-09-13 02:37:30'),
('man@man.com', '$2y$10$Al242nsBPGRfM0eASyuID.D02DUn1Nh6JPUbvCo47R.U7U9Cbzj1q', '2019-09-12 21:40:35'),
('cvalerarios@hotmail.com', '$2y$10$x3YCsnuUQhDX81v/4sfvEuRFAvkqNjxrC7F4/d0hkF069A9OWvDxu', '2019-09-25 20:21:05'),
('cvnetoficial@gmail.com', '$2y$10$GxSxnQ3Z8piuvIo//CGOEexbFUEYwyOBVnvGIgy9wjUuUCz9897U.', '2019-09-25 20:22:21'),
('fortuna7777@hotmail.com', '$2y$10$RmIR97s/P6S5hOY/SKCeRuuyqnwp8fjj5J7P35SqumIejF.dloAHa', '2019-10-16 16:56:09'),
('tesoreria@fortuneonline.com.pe', '$2y$10$qR0NZN.Hzk8RZlj4VET5u.2P1TaYXSt3gfTAbR6sTaAvUjG8uK52W', '2019-10-30 20:18:05'),
('zaraarka@gmail.com', '$2y$10$zvn4tv.t1oUN9g//eV1nEuaoNYc6jOuvsblam.IPYPV4klAlmllyC', '2020-01-11 00:32:11'),
('edolar623@gmail.com', '$2y$10$GP2X7L8oAohu4PdqZkIhruY.J7HSJHvy3txSaGweX6gh5.RM/Ljq.', '2020-01-27 22:19:31'),
('mati_97_ziliotto@hotmail.com', '$2y$10$9R3ge/gT7ZGOhXfa3MUF6OO.D0uJ/tzOoW7RJb20buWyol11cRU.6', '2020-07-18 16:11:48'),
('matiziliotto1@gmail.com', '$2y$10$OFR1qTwFmmQu6odVUmapGOBdok1F6fpZa91xeTqMjsKGTuoDktsKW', '2020-07-29 02:40:31'),
('anita@abejareinaperu.com', '$2y$10$krNUmITfSMs/VTvkynPnS.IvmfAB2CZVmt39rvts5udn.dUC9fG2u', '2020-07-25 23:36:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocambio`
--

CREATE TABLE `tipocambio` (
  `tipocambio_id` int(11) NOT NULL,
  `compra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `venta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipocambio`
--

INSERT INTO `tipocambio` (`tipocambio_id`, `compra`, `venta`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '3.540', '3.553', 'Administrador', '2020-08-04 10:23:58', '2020-08-04 10:23:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuenta`
--

CREATE TABLE `tipocuenta` (
  `tipocuenta_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipocuenta`
--

INSERT INTO `tipocuenta` (`tipocuenta_id`, `nombre`) VALUES
(1, 'Ahorro'),
(2, 'Corriente'),
(3, 'Cuentas nómina'),
(4, 'Cuentas bancaria para empresas o negocio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposdocumento`
--

CREATE TABLE `tiposdocumento` (
  `tiposdocumento_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tiposdocumento`
--

INSERT INTO `tiposdocumento` (`tiposdocumento_id`, `nombre`) VALUES
(1, 'DNI'),
(2, 'PASAPORTE'),
(3, 'RUC 10'),
(4, 'RUC 20'),
(5, 'Carnet de Extranjería');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubigeo`
--

CREATE TABLE `ubigeo` (
  `ubigeo_id` int(11) NOT NULL,
  `dDepartamento` int(2) DEFAULT NULL,
  `codProvincia` int(2) DEFAULT NULL,
  `codDistrito` int(2) DEFAULT NULL,
  `Descripcion` varchar(36) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ubigeo`
--

INSERT INTO `ubigeo` (`ubigeo_id`, `dDepartamento`, `codProvincia`, `codDistrito`, `Descripcion`) VALUES
(1, 1, 0, 0, 'AMAZONAS'),
(2, 1, 1, 0, 'CHACHAPOYAS'),
(3, 1, 1, 1, 'CHACHAPOYAS'),
(4, 1, 1, 2, 'ASUNCION'),
(5, 1, 1, 3, 'BALSAS'),
(6, 1, 1, 4, 'CHETO'),
(7, 1, 1, 5, 'CHILIQUIN'),
(8, 1, 1, 6, 'CHUQUIBAMBA'),
(9, 1, 1, 7, 'GRANADA'),
(10, 1, 1, 8, 'HUANCAS'),
(11, 1, 1, 9, 'LA JALCA'),
(12, 1, 1, 10, 'LEIMEBAMBA'),
(13, 1, 1, 11, 'LEVANTO'),
(14, 1, 1, 12, 'MAGDALENA'),
(15, 1, 1, 13, 'MARISCAL CASTILLA'),
(16, 1, 1, 14, 'MOLINOPAMPA'),
(17, 1, 1, 15, 'MONTEVIDEO'),
(18, 1, 1, 16, 'OLLEROS'),
(19, 1, 1, 17, 'QUINJALCA'),
(20, 1, 1, 18, 'SAN FRANCISCO DE DAGUAS'),
(21, 1, 1, 19, 'SAN ISIDRO DE MAINO'),
(22, 1, 1, 20, 'SOLOCO'),
(23, 1, 1, 21, 'SONCHE'),
(24, 1, 2, 0, 'BAGUA'),
(25, 1, 2, 1, 'BAGUA'),
(26, 1, 2, 2, 'ARAMANGO'),
(27, 1, 2, 3, 'COPALLIN'),
(28, 1, 2, 4, 'EL PARCO'),
(29, 1, 2, 5, 'IMAZA'),
(30, 1, 2, 6, 'LA PECA'),
(31, 1, 3, 0, 'BONGARA'),
(32, 1, 3, 1, 'JUMBILLA'),
(33, 1, 3, 2, 'CHISQUILLA'),
(34, 1, 3, 3, 'CHURUJA'),
(35, 1, 3, 4, 'COROSHA'),
(36, 1, 3, 5, 'CUISPES'),
(37, 1, 3, 6, 'FLORIDA'),
(38, 1, 3, 7, 'JAZAN'),
(39, 1, 3, 8, 'RECTA'),
(40, 1, 3, 9, 'SAN CARLOS'),
(41, 1, 3, 10, 'SHIPASBAMBA'),
(42, 1, 3, 11, 'VALERA'),
(43, 1, 3, 12, 'YAMBRASBAMBA'),
(44, 1, 4, 0, 'CONDORCANQUI'),
(45, 1, 4, 1, 'NIEVA'),
(46, 1, 4, 2, 'EL CENEPA'),
(47, 1, 4, 3, 'RIO SANTIAGO'),
(48, 1, 5, 0, 'LUYA'),
(49, 1, 5, 1, 'LAMUD'),
(50, 1, 5, 2, 'CAMPORREDONDO'),
(51, 1, 5, 3, 'COCABAMBA'),
(52, 1, 5, 4, 'COLCAMAR'),
(53, 1, 5, 5, 'CONILA'),
(54, 1, 5, 6, 'INGUILPATA'),
(55, 1, 5, 7, 'LONGUITA'),
(56, 1, 5, 8, 'LONYA CHICO'),
(57, 1, 5, 9, 'LUYA'),
(58, 1, 5, 10, 'LUYA VIEJO'),
(59, 1, 5, 11, 'MARIA'),
(60, 1, 5, 12, 'OCALLI'),
(61, 1, 5, 13, 'OCUMAL'),
(62, 1, 5, 14, 'PISUQUIA'),
(63, 1, 5, 15, 'PROVIDENCIA'),
(64, 1, 5, 16, 'SAN CRISTOBAL'),
(65, 1, 5, 17, 'SAN FRANCISCO DEL YESO'),
(66, 1, 5, 18, 'SAN JERONIMO'),
(67, 1, 5, 19, 'SAN JUAN DE LOPECANCHA'),
(68, 1, 5, 20, 'SANTA CATALINA'),
(69, 1, 5, 21, 'SANTO TOMAS'),
(70, 1, 5, 22, 'TINGO'),
(71, 1, 5, 23, 'TRITA'),
(72, 1, 6, 0, 'RODRIGUEZ DE MENDOZA'),
(73, 1, 6, 1, 'SAN NICOLAS'),
(74, 1, 6, 2, 'CHIRIMOTO'),
(75, 1, 6, 3, 'COCHAMAL'),
(76, 1, 6, 4, 'HUAMBO'),
(77, 1, 6, 5, 'LIMABAMBA'),
(78, 1, 6, 6, 'LONGAR'),
(79, 1, 6, 7, 'MARISCAL BENAVIDES'),
(80, 1, 6, 8, 'MILPUC'),
(81, 1, 6, 9, 'OMIA'),
(82, 1, 6, 10, 'SANTA ROSA'),
(83, 1, 6, 11, 'TOTORA'),
(84, 1, 6, 12, 'VISTA ALEGRE'),
(85, 1, 7, 0, 'UTCUBAMBA'),
(86, 1, 7, 1, 'BAGUA GRANDE'),
(87, 1, 7, 2, 'CAJARURO'),
(88, 1, 7, 3, 'CUMBA'),
(89, 1, 7, 4, 'EL MILAGRO'),
(90, 1, 7, 5, 'JAMALCA'),
(91, 1, 7, 6, 'LONYA GRANDE'),
(92, 1, 7, 7, 'YAMON'),
(93, 2, 0, 0, 'ANCASH'),
(94, 2, 1, 0, 'HUARAZ'),
(95, 2, 1, 1, 'HUARAZ'),
(96, 2, 1, 2, 'COCHABAMBA'),
(97, 2, 1, 3, 'COLCABAMBA'),
(98, 2, 1, 4, 'HUANCHAY'),
(99, 2, 1, 5, 'INDEPENDENCIA'),
(100, 2, 1, 6, 'JANGAS'),
(101, 2, 1, 7, 'LA LIBERTAD'),
(102, 2, 1, 8, 'OLLEROS'),
(103, 2, 1, 9, 'PAMPAS'),
(104, 2, 1, 10, 'PARIACOTO'),
(105, 2, 1, 11, 'PIRA'),
(106, 2, 1, 12, 'TARICA'),
(107, 2, 2, 0, 'AIJA'),
(108, 2, 2, 1, 'AIJA'),
(109, 2, 2, 2, 'CORIS'),
(110, 2, 2, 3, 'HUACLLAN'),
(111, 2, 2, 4, 'LA MERCED'),
(112, 2, 2, 5, 'SUCCHA'),
(113, 2, 3, 0, 'ANTONIO RAYMONDI'),
(114, 2, 3, 1, 'LLAMELLIN'),
(115, 2, 3, 2, 'ACZO'),
(116, 2, 3, 3, 'CHACCHO'),
(117, 2, 3, 4, 'CHINGAS'),
(118, 2, 3, 5, 'MIRGAS'),
(119, 2, 3, 6, 'SAN JUAN DE RONTOY'),
(120, 2, 4, 0, 'ASUNCION'),
(121, 2, 4, 1, 'CHACAS'),
(122, 2, 4, 2, 'ACOCHACA'),
(123, 2, 5, 0, 'BOLOGNESI'),
(124, 2, 5, 1, 'CHIQUIAN'),
(125, 2, 5, 2, 'ABELARDO PARDO LEZAMETA'),
(126, 2, 5, 3, 'ANTONIO RAYMONDI'),
(127, 2, 5, 4, 'AQUIA'),
(128, 2, 5, 5, 'CAJACAY'),
(129, 2, 5, 6, 'CANIS'),
(130, 2, 5, 7, 'COLQUIOC'),
(131, 2, 5, 8, 'HUALLANCA'),
(132, 2, 5, 9, 'HUASTA'),
(133, 2, 5, 10, 'HUAYLLACAYAN'),
(134, 2, 5, 11, 'LA PRIMAVERA'),
(135, 2, 5, 12, 'MANGAS'),
(136, 2, 5, 13, 'PACLLON'),
(137, 2, 5, 14, 'SAN MIGUEL DE CORPANQUI'),
(138, 2, 5, 15, 'TICLLOS'),
(139, 2, 6, 0, 'CARHUAZ'),
(140, 2, 6, 1, 'CARHUAZ'),
(141, 2, 6, 2, 'ACOPAMPA'),
(142, 2, 6, 3, 'AMASHCA'),
(143, 2, 6, 4, 'ANTA'),
(144, 2, 6, 5, 'ATAQUERO'),
(145, 2, 6, 6, 'MARCARA'),
(146, 2, 6, 7, 'PARIAHUANCA'),
(147, 2, 6, 8, 'SAN MIGUEL DE ACO'),
(148, 2, 6, 9, 'SHILLA'),
(149, 2, 6, 10, 'TINCO'),
(150, 2, 6, 11, 'YUNGAR'),
(151, 2, 7, 0, 'CARLOS FERMIN FITZCARRALD'),
(152, 2, 7, 1, 'SAN LUIS'),
(153, 2, 7, 2, 'SAN NICOLAS'),
(154, 2, 7, 3, 'YAUYA'),
(155, 2, 8, 0, 'CASMA'),
(156, 2, 8, 1, 'CASMA'),
(157, 2, 8, 2, 'BUENA VISTA ALTA'),
(158, 2, 8, 3, 'COMANDANTE NOEL'),
(159, 2, 8, 4, 'YAUTAN'),
(160, 2, 9, 0, 'CORONGO'),
(161, 2, 9, 1, 'CORONGO'),
(162, 2, 9, 2, 'ACO'),
(163, 2, 9, 3, 'BAMBAS'),
(164, 2, 9, 4, 'CUSCA'),
(165, 2, 9, 5, 'LA PAMPA'),
(166, 2, 9, 6, 'YANAC'),
(167, 2, 9, 7, 'YUPAN'),
(168, 2, 10, 0, 'HUARI'),
(169, 2, 10, 1, 'HUARI'),
(170, 2, 10, 2, 'ANRA'),
(171, 2, 10, 3, 'CAJAY'),
(172, 2, 10, 4, 'CHAVIN DE HUANTAR'),
(173, 2, 10, 5, 'HUACACHI'),
(174, 2, 10, 6, 'HUACCHIS'),
(175, 2, 10, 7, 'HUACHIS'),
(176, 2, 10, 8, 'HUANTAR'),
(177, 2, 10, 9, 'MASIN'),
(178, 2, 10, 10, 'PAUCAS'),
(179, 2, 10, 11, 'PONTO'),
(180, 2, 10, 12, 'RAHUAPAMPA'),
(181, 2, 10, 13, 'RAPAYAN'),
(182, 2, 10, 14, 'SAN MARCOS'),
(183, 2, 10, 15, 'SAN PEDRO DE CHANA'),
(184, 2, 10, 16, 'UCO'),
(185, 2, 11, 0, 'HUARMEY'),
(186, 2, 11, 1, 'HUARMEY'),
(187, 2, 11, 2, 'COCHAPETI'),
(188, 2, 11, 3, 'CULEBRAS'),
(189, 2, 11, 4, 'HUAYAN'),
(190, 2, 11, 5, 'MALVAS'),
(191, 2, 12, 0, 'HUAYLAS'),
(192, 2, 12, 1, 'CARAZ'),
(193, 2, 12, 2, 'HUALLANCA'),
(194, 2, 12, 3, 'HUATA'),
(195, 2, 12, 4, 'HUAYLAS'),
(196, 2, 12, 5, 'MATO'),
(197, 2, 12, 6, 'PAMPAROMAS'),
(198, 2, 12, 7, 'PUEBLO LIBRE'),
(199, 2, 12, 8, 'SANTA CRUZ'),
(200, 2, 12, 9, 'SANTO TORIBIO'),
(201, 2, 12, 10, 'YURACMARCA'),
(202, 2, 13, 0, 'MARISCAL LUZURIAGA'),
(203, 2, 13, 1, 'PISCOBAMBA'),
(204, 2, 13, 2, 'CASCA'),
(205, 2, 13, 3, 'ELEAZAR GUZMAN BARRON'),
(206, 2, 13, 4, 'FIDEL OLIVAS ESCUDERO'),
(207, 2, 13, 5, 'LLAMA'),
(208, 2, 13, 6, 'LLUMPA'),
(209, 2, 13, 7, 'LUCMA'),
(210, 2, 13, 8, 'MUSGA'),
(211, 2, 14, 0, 'OCROS'),
(212, 2, 14, 1, 'OCROS'),
(213, 2, 14, 2, 'ACAS'),
(214, 2, 14, 3, 'CAJAMARQUILLA'),
(215, 2, 14, 4, 'CARHUAPAMPA'),
(216, 2, 14, 5, 'COCHAS'),
(217, 2, 14, 6, 'CONGAS'),
(218, 2, 14, 7, 'LLIPA'),
(219, 2, 14, 8, 'SAN CRISTOBAL DE RAJAN'),
(220, 2, 14, 9, 'SAN PEDRO'),
(221, 2, 14, 10, 'SANTIAGO DE CHILCAS'),
(222, 2, 15, 0, 'PALLASCA'),
(223, 2, 15, 1, 'CABANA'),
(224, 2, 15, 2, 'BOLOGNESI'),
(225, 2, 15, 3, 'CONCHUCOS'),
(226, 2, 15, 4, 'HUACASCHUQUE'),
(227, 2, 15, 5, 'HUANDOVAL'),
(228, 2, 15, 6, 'LACABAMBA'),
(229, 2, 15, 7, 'LLAPO'),
(230, 2, 15, 8, 'PALLASCA'),
(231, 2, 15, 9, 'PAMPAS'),
(232, 2, 15, 10, 'SANTA ROSA'),
(233, 2, 15, 11, 'TAUCA'),
(234, 2, 16, 0, 'POMABAMBA'),
(235, 2, 16, 1, 'POMABAMBA'),
(236, 2, 16, 2, 'HUAYLLAN'),
(237, 2, 16, 3, 'PAROBAMBA'),
(238, 2, 16, 4, 'QUINUABAMBA'),
(239, 2, 17, 0, 'RECUAY'),
(240, 2, 17, 1, 'RECUAY'),
(241, 2, 17, 2, 'CATAC'),
(242, 2, 17, 3, 'COTAPARACO'),
(243, 2, 17, 4, 'HUAYLLAPAMPA'),
(244, 2, 17, 5, 'LLACLLIN'),
(245, 2, 17, 6, 'MARCA'),
(246, 2, 17, 7, 'PAMPAS CHICO'),
(247, 2, 17, 8, 'PARARIN'),
(248, 2, 17, 9, 'TAPACOCHA'),
(249, 2, 17, 10, 'TICAPAMPA'),
(250, 2, 18, 0, 'SANTA'),
(251, 2, 18, 1, 'CHIMBOTE'),
(252, 2, 18, 2, 'CACERES DEL PERU'),
(253, 2, 18, 3, 'COISHCO'),
(254, 2, 18, 4, 'MACATE'),
(255, 2, 18, 5, 'MORO'),
(256, 2, 18, 6, 'NEPEÑA'),
(257, 2, 18, 7, 'SAMANCO'),
(258, 2, 18, 8, 'SANTA'),
(259, 2, 18, 9, 'NUEVO CHIMBOTE'),
(260, 2, 19, 0, 'SIHUAS'),
(261, 2, 19, 1, 'SIHUAS'),
(262, 2, 19, 2, 'ACOBAMBA'),
(263, 2, 19, 3, 'ALFONSO UGARTE'),
(264, 2, 19, 4, 'CASHAPAMPA'),
(265, 2, 19, 5, 'CHINGALPO'),
(266, 2, 19, 6, 'HUAYLLABAMBA'),
(267, 2, 19, 7, 'QUICHES'),
(268, 2, 19, 8, 'RAGASH'),
(269, 2, 19, 9, 'SAN JUAN'),
(270, 2, 19, 10, 'SICSIBAMBA'),
(271, 2, 20, 0, 'YUNGAY'),
(272, 2, 20, 1, 'YUNGAY'),
(273, 2, 20, 2, 'CASCAPARA'),
(274, 2, 20, 3, 'MANCOS'),
(275, 2, 20, 4, 'MATACOTO'),
(276, 2, 20, 5, 'QUILLO'),
(277, 2, 20, 6, 'RANRAHIRCA'),
(278, 2, 20, 7, 'SHUPLUY'),
(279, 2, 20, 8, 'YANAMA'),
(280, 3, 0, 0, 'APURIMAC'),
(281, 3, 1, 0, 'ABANCAY'),
(282, 3, 1, 1, 'ABANCAY'),
(283, 3, 1, 2, 'CHACOCHE'),
(284, 3, 1, 3, 'CIRCA'),
(285, 3, 1, 4, 'CURAHUASI'),
(286, 3, 1, 5, 'HUANIPACA'),
(287, 3, 1, 6, 'LAMBRAMA'),
(288, 3, 1, 7, 'PICHIRHUA'),
(289, 3, 1, 8, 'SAN PEDRO DE CACHORA'),
(290, 3, 1, 9, 'TAMBURCO'),
(291, 3, 2, 0, 'ANDAHUAYLAS'),
(292, 3, 2, 1, 'ANDAHUAYLAS'),
(293, 3, 2, 2, 'ANDARAPA'),
(294, 3, 2, 3, 'CHIARA'),
(295, 3, 2, 4, 'HUANCARAMA'),
(296, 3, 2, 5, 'HUANCARAY'),
(297, 3, 2, 6, 'HUAYANA'),
(298, 3, 2, 7, 'KISHUARA'),
(299, 3, 2, 8, 'PACOBAMBA'),
(300, 3, 2, 9, 'PACUCHA'),
(301, 3, 2, 10, 'PAMPACHIRI'),
(302, 3, 2, 11, 'POMACOCHA'),
(303, 3, 2, 12, 'SAN ANTONIO DE CACHI'),
(304, 3, 2, 13, 'SAN JERONIMO'),
(305, 3, 2, 14, 'SAN MIGUEL DE CHACCRAMPA'),
(306, 3, 2, 15, 'SANTA MARIA DE CHICMO'),
(307, 3, 2, 16, 'TALAVERA'),
(308, 3, 2, 17, 'TUMAY HUARACA'),
(309, 3, 2, 18, 'TURPO'),
(310, 3, 2, 19, 'KAQUIABAMBA'),
(311, 3, 3, 0, 'ANTABAMBA'),
(312, 3, 3, 1, 'ANTABAMBA'),
(313, 3, 3, 2, 'EL ORO'),
(314, 3, 3, 3, 'HUAQUIRCA'),
(315, 3, 3, 4, 'JUAN ESPINOZA MEDRANO'),
(316, 3, 3, 5, 'OROPESA'),
(317, 3, 3, 6, 'PACHACONAS'),
(318, 3, 3, 7, 'SABAINO'),
(319, 3, 4, 0, 'AYMARAES'),
(320, 3, 4, 1, 'CHALHUANCA'),
(321, 3, 4, 2, 'CAPAYA'),
(322, 3, 4, 3, 'CARAYBAMBA'),
(323, 3, 4, 4, 'CHAPIMARCA'),
(324, 3, 4, 5, 'COLCABAMBA'),
(325, 3, 4, 6, 'COTARUSE'),
(326, 3, 4, 7, 'HUAYLLO'),
(327, 3, 4, 8, 'JUSTO APU SAHUARAURA'),
(328, 3, 4, 9, 'LUCRE'),
(329, 3, 4, 10, 'POCOHUANCA'),
(330, 3, 4, 11, 'SAN JUAN DE CHACÑA'),
(331, 3, 4, 12, 'SAÑAYCA'),
(332, 3, 4, 13, 'SORAYA'),
(333, 3, 4, 14, 'TAPAIRIHUA'),
(334, 3, 4, 15, 'TINTAY'),
(335, 3, 4, 16, 'TORAYA'),
(336, 3, 4, 17, 'YANACA'),
(337, 3, 5, 0, 'COTABAMBAS'),
(338, 3, 5, 1, 'TAMBOBAMBA'),
(339, 3, 5, 2, 'COTABAMBAS'),
(340, 3, 5, 3, 'COYLLURQUI'),
(341, 3, 5, 4, 'HAQUIRA'),
(342, 3, 5, 5, 'MARA'),
(343, 3, 5, 6, 'CHALLHUAHUACHO'),
(344, 3, 6, 0, 'CHINCHEROS'),
(345, 3, 6, 1, 'CHINCHEROS'),
(346, 3, 6, 2, 'ANCO_HUALLO'),
(347, 3, 6, 3, 'COCHARCAS'),
(348, 3, 6, 4, 'HUACCANA'),
(349, 3, 6, 5, 'OCOBAMBA'),
(350, 3, 6, 6, 'ONGOY'),
(351, 3, 6, 7, 'URANMARCA'),
(352, 3, 6, 8, 'RANRACANCHA'),
(353, 3, 7, 0, 'GRAU'),
(354, 3, 7, 1, 'CHUQUIBAMBILLA'),
(355, 3, 7, 2, 'CURPAHUASI'),
(356, 3, 7, 3, 'GAMARRA'),
(357, 3, 7, 4, 'HUAYLLATI'),
(358, 3, 7, 5, 'MAMARA'),
(359, 3, 7, 6, 'MICAELA BASTIDAS'),
(360, 3, 7, 7, 'PATAYPAMPA'),
(361, 3, 7, 8, 'PROGRESO'),
(362, 3, 7, 9, 'SAN ANTONIO'),
(363, 3, 7, 10, 'SANTA ROSA'),
(364, 3, 7, 11, 'TURPAY'),
(365, 3, 7, 12, 'VILCABAMBA'),
(366, 3, 7, 13, 'VIRUNDO'),
(367, 3, 7, 14, 'CURASCO'),
(368, 4, 0, 0, 'AREQUIPA'),
(369, 4, 1, 0, 'AREQUIPA'),
(370, 4, 1, 1, 'AREQUIPA'),
(371, 4, 1, 2, 'ALTO SELVA ALEGRE'),
(372, 4, 1, 3, 'CAYMA'),
(373, 4, 1, 4, 'CERRO COLORADO'),
(374, 4, 1, 5, 'CHARACATO'),
(375, 4, 1, 6, 'CHIGUATA'),
(376, 4, 1, 7, 'JACOBO HUNTER'),
(377, 4, 1, 8, 'LA JOYA'),
(378, 4, 1, 9, 'MARIANO MELGAR'),
(379, 4, 1, 10, 'MIRAFLORES'),
(380, 4, 1, 11, 'MOLLEBAYA'),
(381, 4, 1, 12, 'PAUCARPATA'),
(382, 4, 1, 13, 'POCSI'),
(383, 4, 1, 14, 'POLOBAYA'),
(384, 4, 1, 15, 'QUEQUEÑA'),
(385, 4, 1, 16, 'SABANDIA'),
(386, 4, 1, 17, 'SACHACA'),
(387, 4, 1, 18, 'SAN JUAN DE SIGUAS'),
(388, 4, 1, 19, 'SAN JUAN DE TARUCANI'),
(389, 4, 1, 20, 'SANTA ISABEL DE SIGUAS'),
(390, 4, 1, 21, 'SANTA RITA DE SIGUAS'),
(391, 4, 1, 22, 'SOCABAYA'),
(392, 4, 1, 23, 'TIABAYA'),
(393, 4, 1, 24, 'UCHUMAYO'),
(394, 4, 1, 25, 'VITOR'),
(395, 4, 1, 26, 'YANAHUARA'),
(396, 4, 1, 27, 'YARABAMBA'),
(397, 4, 1, 28, 'YURA'),
(398, 4, 1, 29, 'JOSE LUIS BUSTAMANTE Y RIVERO'),
(399, 4, 2, 0, 'CAMANA'),
(400, 4, 2, 1, 'CAMANA'),
(401, 4, 2, 2, 'JOSE MARIA QUIMPER'),
(402, 4, 2, 3, 'MARIANO NICOLAS VALCARCEL'),
(403, 4, 2, 4, 'MARISCAL CACERES'),
(404, 4, 2, 5, 'NICOLAS DE PIEROLA'),
(405, 4, 2, 6, 'OCOÑA'),
(406, 4, 2, 7, 'QUILCA'),
(407, 4, 2, 8, 'SAMUEL PASTOR'),
(408, 4, 3, 0, 'CARAVELI'),
(409, 4, 3, 1, 'CARAVELI'),
(410, 4, 3, 2, 'ACARI'),
(411, 4, 3, 3, 'ATICO'),
(412, 4, 3, 4, 'ATIQUIPA'),
(413, 4, 3, 5, 'BELLA UNION'),
(414, 4, 3, 6, 'CAHUACHO'),
(415, 4, 3, 7, 'CHALA'),
(416, 4, 3, 8, 'CHAPARRA'),
(417, 4, 3, 9, 'HUANUHUANU'),
(418, 4, 3, 10, 'JAQUI'),
(419, 4, 3, 11, 'LOMAS'),
(420, 4, 3, 12, 'QUICACHA'),
(421, 4, 3, 13, 'YAUCA'),
(422, 4, 4, 0, 'CASTILLA'),
(423, 4, 4, 1, 'APLAO'),
(424, 4, 4, 2, 'ANDAGUA'),
(425, 4, 4, 3, 'AYO'),
(426, 4, 4, 4, 'CHACHAS'),
(427, 4, 4, 5, 'CHILCAYMARCA'),
(428, 4, 4, 6, 'CHOCO'),
(429, 4, 4, 7, 'HUANCARQUI'),
(430, 4, 4, 8, 'MACHAGUAY'),
(431, 4, 4, 9, 'ORCOPAMPA'),
(432, 4, 4, 10, 'PAMPACOLCA'),
(433, 4, 4, 11, 'TIPAN'),
(434, 4, 4, 12, 'UÑON'),
(435, 4, 4, 13, 'URACA'),
(436, 4, 4, 14, 'VIRACO'),
(437, 4, 5, 0, 'CAYLLOMA'),
(438, 4, 5, 1, 'CHIVAY'),
(439, 4, 5, 2, 'ACHOMA'),
(440, 4, 5, 3, 'CABANACONDE'),
(441, 4, 5, 4, 'CALLALLI'),
(442, 4, 5, 5, 'CAYLLOMA'),
(443, 4, 5, 6, 'COPORAQUE'),
(444, 4, 5, 7, 'HUAMBO'),
(445, 4, 5, 8, 'HUANCA'),
(446, 4, 5, 9, 'ICHUPAMPA'),
(447, 4, 5, 10, 'LARI'),
(448, 4, 5, 11, 'LLUTA'),
(449, 4, 5, 12, 'MACA'),
(450, 4, 5, 13, 'MADRIGAL'),
(451, 4, 5, 14, 'SAN ANTONIO DE CHUCA'),
(452, 4, 5, 15, 'SIBAYO'),
(453, 4, 5, 16, 'TAPAY'),
(454, 4, 5, 17, 'TISCO'),
(455, 4, 5, 18, 'TUTI'),
(456, 4, 5, 19, 'YANQUE'),
(457, 4, 5, 20, 'MAJES'),
(458, 4, 6, 0, 'CONDESUYOS'),
(459, 4, 6, 1, 'CHUQUIBAMBA'),
(460, 4, 6, 2, 'ANDARAY'),
(461, 4, 6, 3, 'CAYARANI'),
(462, 4, 6, 4, 'CHICHAS'),
(463, 4, 6, 5, 'IRAY'),
(464, 4, 6, 6, 'RIO GRANDE'),
(465, 4, 6, 7, 'SALAMANCA'),
(466, 4, 6, 8, 'YANAQUIHUA'),
(467, 4, 7, 0, 'ISLAY'),
(468, 4, 7, 1, 'MOLLENDO'),
(469, 4, 7, 2, 'COCACHACRA'),
(470, 4, 7, 3, 'DEAN VALDIVIA'),
(471, 4, 7, 4, 'ISLAY'),
(472, 4, 7, 5, 'MEJIA'),
(473, 4, 7, 6, 'PUNTA DE BOMBON'),
(474, 4, 8, 0, 'LA UNION'),
(475, 4, 8, 1, 'COTAHUASI'),
(476, 4, 8, 2, 'ALCA'),
(477, 4, 8, 3, 'CHARCANA'),
(478, 4, 8, 4, 'HUAYNACOTAS'),
(479, 4, 8, 5, 'PAMPAMARCA'),
(480, 4, 8, 6, 'PUYCA'),
(481, 4, 8, 7, 'QUECHUALLA'),
(482, 4, 8, 8, 'SAYLA'),
(483, 4, 8, 9, 'TAURIA'),
(484, 4, 8, 10, 'TOMEPAMPA'),
(485, 4, 8, 11, 'TORO'),
(486, 5, 0, 0, 'AYACUCHO'),
(487, 5, 1, 0, 'HUAMANGA'),
(488, 5, 1, 1, 'AYACUCHO'),
(489, 5, 1, 2, 'ACOCRO'),
(490, 5, 1, 3, 'ACOS VINCHOS'),
(491, 5, 1, 4, 'CARMEN ALTO'),
(492, 5, 1, 5, 'CHIARA'),
(493, 5, 1, 6, 'OCROS'),
(494, 5, 1, 7, 'PACAYCASA'),
(495, 5, 1, 8, 'QUINUA'),
(496, 5, 1, 9, 'SAN JOSE DE TICLLAS'),
(497, 5, 1, 10, 'SAN JUAN BAUTISTA'),
(498, 5, 1, 11, 'SANTIAGO DE PISCHA'),
(499, 5, 1, 12, 'SOCOS'),
(500, 5, 1, 13, 'TAMBILLO'),
(501, 5, 1, 14, 'VINCHOS'),
(502, 5, 1, 15, 'JESUS NAZARENO'),
(503, 5, 2, 0, 'CANGALLO'),
(504, 5, 2, 1, 'CANGALLO'),
(505, 5, 2, 2, 'CHUSCHI'),
(506, 5, 2, 3, 'LOS MOROCHUCOS'),
(507, 5, 2, 4, 'MARIA PARADO DE BELLIDO'),
(508, 5, 2, 5, 'PARAS'),
(509, 5, 2, 6, 'TOTOS'),
(510, 5, 3, 0, 'HUANCA SANCOS'),
(511, 5, 3, 1, 'SANCOS'),
(512, 5, 3, 2, 'CARAPO'),
(513, 5, 3, 3, 'SACSAMARCA'),
(514, 5, 3, 4, 'SANTIAGO DE LUCANAMARCA'),
(515, 5, 4, 0, 'HUANTA'),
(516, 5, 4, 1, 'HUANTA'),
(517, 5, 4, 2, 'AYAHUANCO'),
(518, 5, 4, 3, 'HUAMANGUILLA'),
(519, 5, 4, 4, 'IGUAIN'),
(520, 5, 4, 5, 'LURICOCHA'),
(521, 5, 4, 6, 'SANTILLANA'),
(522, 5, 4, 7, 'SIVIA'),
(523, 5, 4, 8, 'LLOCHEGUA'),
(524, 5, 5, 0, 'LA MAR'),
(525, 5, 5, 1, 'SAN MIGUEL'),
(526, 5, 5, 2, 'ANCO'),
(527, 5, 5, 3, 'AYNA'),
(528, 5, 5, 4, 'CHILCAS'),
(529, 5, 5, 5, 'CHUNGUI'),
(530, 5, 5, 6, 'LUIS CARRANZA'),
(531, 5, 5, 7, 'SANTA ROSA'),
(532, 5, 5, 8, 'TAMBO'),
(533, 5, 6, 0, 'LUCANAS'),
(534, 5, 6, 1, 'PUQUIO'),
(535, 5, 6, 2, 'AUCARA'),
(536, 5, 6, 3, 'CABANA'),
(537, 5, 6, 4, 'CARMEN SALCEDO'),
(538, 5, 6, 5, 'CHAVIÑA'),
(539, 5, 6, 6, 'CHIPAO'),
(540, 5, 6, 7, 'HUAC-HUAS'),
(541, 5, 6, 8, 'LARAMATE'),
(542, 5, 6, 9, 'LEONCIO PRADO'),
(543, 5, 6, 10, 'LLAUTA'),
(544, 5, 6, 11, 'LUCANAS'),
(545, 5, 6, 12, 'OCAÑA'),
(546, 5, 6, 13, 'OTOCA'),
(547, 5, 6, 14, 'SAISA'),
(548, 5, 6, 15, 'SAN CRISTOBAL'),
(549, 5, 6, 16, 'SAN JUAN'),
(550, 5, 6, 17, 'SAN PEDRO'),
(551, 5, 6, 18, 'SAN PEDRO DE PALCO'),
(552, 5, 6, 19, 'SANCOS'),
(553, 5, 6, 20, 'SANTA ANA DE HUAYCAHUACHO'),
(554, 5, 6, 21, 'SANTA LUCIA'),
(555, 5, 7, 0, 'PARINACOCHAS'),
(556, 5, 7, 1, 'CORACORA'),
(557, 5, 7, 2, 'CHUMPI'),
(558, 5, 7, 3, 'CORONEL CASTAÑEDA'),
(559, 5, 7, 4, 'PACAPAUSA'),
(560, 5, 7, 5, 'PULLO'),
(561, 5, 7, 6, 'PUYUSCA'),
(562, 5, 7, 7, 'SAN FRANCISCO DE RAVACAYCO'),
(563, 5, 7, 8, 'UPAHUACHO'),
(564, 5, 8, 0, 'PAUCAR DEL SARA SARA'),
(565, 5, 8, 1, 'PAUSA'),
(566, 5, 8, 2, 'COLTA'),
(567, 5, 8, 3, 'CORCULLA'),
(568, 5, 8, 4, 'LAMPA'),
(569, 5, 8, 5, 'MARCABAMBA'),
(570, 5, 8, 6, 'OYOLO'),
(571, 5, 8, 7, 'PARARCA'),
(572, 5, 8, 8, 'SAN JAVIER DE ALPABAMBA'),
(573, 5, 8, 9, 'SAN JOSE DE USHUA'),
(574, 5, 8, 10, 'SARA SARA'),
(575, 5, 9, 0, 'SUCRE'),
(576, 5, 9, 1, 'QUEROBAMBA'),
(577, 5, 9, 2, 'BELEN'),
(578, 5, 9, 3, 'CHALCOS'),
(579, 5, 9, 4, 'CHILCAYOC'),
(580, 5, 9, 5, 'HUACAÑA'),
(581, 5, 9, 6, 'MORCOLLA'),
(582, 5, 9, 7, 'PAICO'),
(583, 5, 9, 8, 'SAN PEDRO DE LARCAY'),
(584, 5, 9, 9, 'SAN SALVADOR DE QUIJE'),
(585, 5, 9, 10, 'SANTIAGO DE PAUCARAY'),
(586, 5, 9, 11, 'SORAS'),
(587, 5, 10, 0, 'VICTOR FAJARDO'),
(588, 5, 10, 1, 'HUANCAPI'),
(589, 5, 10, 2, 'ALCAMENCA'),
(590, 5, 10, 3, 'APONGO'),
(591, 5, 10, 4, 'ASQUIPATA'),
(592, 5, 10, 5, 'CANARIA'),
(593, 5, 10, 6, 'CAYARA'),
(594, 5, 10, 7, 'COLCA'),
(595, 5, 10, 8, 'HUAMANQUIQUIA'),
(596, 5, 10, 9, 'HUANCARAYLLA'),
(597, 5, 10, 10, 'HUAYA'),
(598, 5, 10, 11, 'SARHUA'),
(599, 5, 10, 12, 'VILCANCHOS'),
(600, 5, 11, 0, 'VILCAS HUAMAN'),
(601, 5, 11, 1, 'VILCAS HUAMAN'),
(602, 5, 11, 2, 'ACCOMARCA'),
(603, 5, 11, 3, 'CARHUANCA'),
(604, 5, 11, 4, 'CONCEPCION'),
(605, 5, 11, 5, 'HUAMBALPA'),
(606, 5, 11, 6, 'INDEPENDENCIA'),
(607, 5, 11, 7, 'SAURAMA'),
(608, 5, 11, 8, 'VISCHONGO'),
(609, 6, 0, 0, 'CAJAMARCA'),
(610, 6, 1, 0, 'CAJAMARCA'),
(611, 6, 1, 1, 'CAJAMARCA'),
(612, 6, 1, 2, 'ASUNCION'),
(613, 6, 1, 3, 'CHETILLA'),
(614, 6, 1, 4, 'COSPAN'),
(615, 6, 1, 5, 'ENCAÑADA'),
(616, 6, 1, 6, 'JESUS'),
(617, 6, 1, 7, 'LLACANORA'),
(618, 6, 1, 8, 'LOS BAÑOS DEL INCA'),
(619, 6, 1, 9, 'MAGDALENA'),
(620, 6, 1, 10, 'MATARA'),
(621, 6, 1, 11, 'NAMORA'),
(622, 6, 1, 12, 'SAN JUAN'),
(623, 6, 2, 0, 'CAJABAMBA'),
(624, 6, 2, 1, 'CAJABAMBA'),
(625, 6, 2, 2, 'CACHACHI'),
(626, 6, 2, 3, 'CONDEBAMBA'),
(627, 6, 2, 4, 'SITACOCHA'),
(628, 6, 3, 0, 'CELENDIN'),
(629, 6, 3, 1, 'CELENDIN'),
(630, 6, 3, 2, 'CHUMUCH'),
(631, 6, 3, 3, 'CORTEGANA'),
(632, 6, 3, 4, 'HUASMIN'),
(633, 6, 3, 5, 'JORGE CHAVEZ'),
(634, 6, 3, 6, 'JOSE GALVEZ'),
(635, 6, 3, 7, 'MIGUEL IGLESIAS'),
(636, 6, 3, 8, 'OXAMARCA'),
(637, 6, 3, 9, 'SOROCHUCO'),
(638, 6, 3, 10, 'SUCRE'),
(639, 6, 3, 11, 'UTCO'),
(640, 6, 3, 12, 'LA LIBERTAD DE PALLAN'),
(641, 6, 4, 0, 'CHOTA'),
(642, 6, 4, 1, 'CHOTA'),
(643, 6, 4, 2, 'ANGUIA'),
(644, 6, 4, 3, 'CHADIN'),
(645, 6, 4, 4, 'CHIGUIRIP'),
(646, 6, 4, 5, 'CHIMBAN'),
(647, 6, 4, 6, 'CHOROPAMPA'),
(648, 6, 4, 7, 'COCHABAMBA'),
(649, 6, 4, 8, 'CONCHAN'),
(650, 6, 4, 9, 'HUAMBOS'),
(651, 6, 4, 10, 'LAJAS'),
(652, 6, 4, 11, 'LLAMA'),
(653, 6, 4, 12, 'MIRACOSTA'),
(654, 6, 4, 13, 'PACCHA'),
(655, 6, 4, 14, 'PION'),
(656, 6, 4, 15, 'QUEROCOTO'),
(657, 6, 4, 16, 'SAN JUAN DE LICUPIS'),
(658, 6, 4, 17, 'TACABAMBA'),
(659, 6, 4, 18, 'TOCMOCHE'),
(660, 6, 4, 19, 'CHALAMARCA'),
(661, 6, 5, 0, 'CONTUMAZA'),
(662, 6, 5, 1, 'CONTUMAZA'),
(663, 6, 5, 2, 'CHILETE'),
(664, 6, 5, 3, 'CUPISNIQUE'),
(665, 6, 5, 4, 'GUZMANGO'),
(666, 6, 5, 5, 'SAN BENITO'),
(667, 6, 5, 6, 'SANTA CRUZ DE TOLED'),
(668, 6, 5, 7, 'TANTARICA'),
(669, 6, 5, 8, 'YONAN'),
(670, 6, 6, 0, 'CUTERVO'),
(671, 6, 6, 1, 'CUTERVO'),
(672, 6, 6, 2, 'CALLAYUC'),
(673, 6, 6, 3, 'CHOROS'),
(674, 6, 6, 4, 'CUJILLO'),
(675, 6, 6, 5, 'LA RAMADA'),
(676, 6, 6, 6, 'PIMPINGOS'),
(677, 6, 6, 7, 'QUEROCOTILLO'),
(678, 6, 6, 8, 'SAN ANDRES DE CUTERVO'),
(679, 6, 6, 9, 'SAN JUAN DE CUTERVO'),
(680, 6, 6, 10, 'SAN LUIS DE LUCMA'),
(681, 6, 6, 11, 'SANTA CRUZ'),
(682, 6, 6, 12, 'SANTO DOMINGO DE LA CAPILLA'),
(683, 6, 6, 13, 'SANTO TOMAS'),
(684, 6, 6, 14, 'SOCOTA'),
(685, 6, 6, 15, 'TORIBIO CASANOVA'),
(686, 6, 7, 0, 'HUALGAYOC'),
(687, 6, 7, 1, 'BAMBAMARCA'),
(688, 6, 7, 2, 'CHUGUR'),
(689, 6, 7, 3, 'HUALGAYOC'),
(690, 6, 8, 0, 'JAEN'),
(691, 6, 8, 1, 'JAEN'),
(692, 6, 8, 2, 'BELLAVISTA'),
(693, 6, 8, 3, 'CHONTALI'),
(694, 6, 8, 4, 'COLASAY'),
(695, 6, 8, 5, 'HUABAL'),
(696, 6, 8, 6, 'LAS PIRIAS'),
(697, 6, 8, 7, 'POMAHUACA'),
(698, 6, 8, 8, 'PUCARA'),
(699, 6, 8, 9, 'SALLIQUE'),
(700, 6, 8, 10, 'SAN FELIPE'),
(701, 6, 8, 11, 'SAN JOSE DEL ALTO'),
(702, 6, 8, 12, 'SANTA ROSA'),
(703, 6, 9, 0, 'SAN IGNACIO'),
(704, 6, 9, 1, 'SAN IGNACIO'),
(705, 6, 9, 2, 'CHIRINOS'),
(706, 6, 9, 3, 'HUARANGO'),
(707, 6, 9, 4, 'LA COIPA'),
(708, 6, 9, 5, 'NAMBALLE'),
(709, 6, 9, 6, 'SAN JOSE DE LOURDES'),
(710, 6, 9, 7, 'TABACONAS'),
(711, 6, 10, 0, 'SAN MARCOS'),
(712, 6, 10, 1, 'PEDRO GALVEZ'),
(713, 6, 10, 2, 'CHANCAY'),
(714, 6, 10, 3, 'EDUARDO VILLANUEVA'),
(715, 6, 10, 4, 'GREGORIO PITA'),
(716, 6, 10, 5, 'ICHOCAN'),
(717, 6, 10, 6, 'JOSE MANUEL QUIROZ'),
(718, 6, 10, 7, 'JOSE SABOGAL'),
(719, 6, 11, 0, 'SAN MIGUEL'),
(720, 6, 11, 1, 'SAN MIGUEL'),
(721, 6, 11, 2, 'BOLIVAR'),
(722, 6, 11, 3, 'CALQUIS'),
(723, 6, 11, 4, 'CATILLUC'),
(724, 6, 11, 5, 'EL PRADO'),
(725, 6, 11, 6, 'LA FLORIDA'),
(726, 6, 11, 7, 'LLAPA'),
(727, 6, 11, 8, 'NANCHOC'),
(728, 6, 11, 9, 'NIEPOS'),
(729, 6, 11, 10, 'SAN GREGORIO'),
(730, 6, 11, 11, 'SAN SILVESTRE DE COCHAN'),
(731, 6, 11, 12, 'TONGOD'),
(732, 6, 11, 13, 'UNION AGUA BLANCA'),
(733, 6, 12, 0, 'SAN PABLO'),
(734, 6, 12, 1, 'SAN PABLO'),
(735, 6, 12, 2, 'SAN BERNARDINO'),
(736, 6, 12, 3, 'SAN LUIS'),
(737, 6, 12, 4, 'TUMBADEN'),
(738, 6, 13, 0, 'SANTA CRUZ'),
(739, 6, 13, 1, 'SANTA CRUZ'),
(740, 6, 13, 2, 'ANDABAMBA'),
(741, 6, 13, 3, 'CATACHE'),
(742, 6, 13, 4, 'CHANCAYBAÑOS'),
(743, 6, 13, 5, 'LA ESPERANZA'),
(744, 6, 13, 6, 'NINABAMBA'),
(745, 6, 13, 7, 'PULAN'),
(746, 6, 13, 8, 'SAUCEPAMPA'),
(747, 6, 13, 9, 'SEXI'),
(748, 6, 13, 10, 'UTICYACU'),
(749, 6, 13, 11, 'YAUYUCAN'),
(750, 7, 0, 0, 'CALLAO'),
(751, 7, 1, 0, 'CALLAO'),
(752, 7, 1, 1, 'CALLAO'),
(753, 7, 1, 2, 'BELLAVISTA'),
(754, 7, 1, 3, 'CARMEN DE LA LEGUA REYNOSO'),
(755, 7, 1, 4, 'LA PERLA'),
(756, 7, 1, 5, 'LA PUNTA'),
(757, 7, 1, 6, 'VENTANILLA'),
(758, 8, 0, 0, 'CUSCO'),
(759, 8, 1, 0, 'CUSCO'),
(760, 8, 1, 1, 'CUSCO'),
(761, 8, 1, 2, 'CCORCA'),
(762, 8, 1, 3, 'POROY'),
(763, 8, 1, 4, 'SAN JERONIMO'),
(764, 8, 1, 5, 'SAN SEBASTIAN'),
(765, 8, 1, 6, 'SANTIAGO'),
(766, 8, 1, 7, 'SAYLLA'),
(767, 8, 1, 8, 'WANCHAQ'),
(768, 8, 2, 0, 'ACOMAYO'),
(769, 8, 2, 1, 'ACOMAYO'),
(770, 8, 2, 2, 'ACOPIA'),
(771, 8, 2, 3, 'ACOS'),
(772, 8, 2, 4, 'MOSOC LLACTA'),
(773, 8, 2, 5, 'POMACANCHI'),
(774, 8, 2, 6, 'RONDOCAN'),
(775, 8, 2, 7, 'SANGARARA'),
(776, 8, 3, 0, 'ANTA'),
(777, 8, 3, 1, 'ANTA'),
(778, 8, 3, 2, 'ANCAHUASI'),
(779, 8, 3, 3, 'CACHIMAYO'),
(780, 8, 3, 4, 'CHINCHAYPUJIO'),
(781, 8, 3, 5, 'HUAROCONDO'),
(782, 8, 3, 6, 'LIMATAMBO'),
(783, 8, 3, 7, 'MOLLEPATA'),
(784, 8, 3, 8, 'PUCYURA'),
(785, 8, 3, 9, 'ZURITE'),
(786, 8, 4, 0, 'CALCA'),
(787, 8, 4, 1, 'CALCA'),
(788, 8, 4, 2, 'COYA'),
(789, 8, 4, 3, 'LAMAY'),
(790, 8, 4, 4, 'LARES'),
(791, 8, 4, 5, 'PISAC'),
(792, 8, 4, 6, 'SAN SALVADOR'),
(793, 8, 4, 7, 'TARAY'),
(794, 8, 4, 8, 'YANATILE'),
(795, 8, 5, 0, 'CANAS'),
(796, 8, 5, 1, 'YANAOCA'),
(797, 8, 5, 2, 'CHECCA'),
(798, 8, 5, 3, 'KUNTURKANKI'),
(799, 8, 5, 4, 'LANGUI'),
(800, 8, 5, 5, 'LAYO'),
(801, 8, 5, 6, 'PAMPAMARCA'),
(802, 8, 5, 7, 'QUEHUE'),
(803, 8, 5, 8, 'TUPAC AMARU'),
(804, 8, 6, 0, 'CANCHIS'),
(805, 8, 6, 1, 'SICUANI'),
(806, 8, 6, 2, 'CHECACUPE'),
(807, 8, 6, 3, 'COMBAPATA'),
(808, 8, 6, 4, 'MARANGANI'),
(809, 8, 6, 5, 'PITUMARCA'),
(810, 8, 6, 6, 'SAN PABLO'),
(811, 8, 6, 7, 'SAN PEDRO'),
(812, 8, 6, 8, 'TINTA'),
(813, 8, 7, 0, 'CHUMBIVILCAS'),
(814, 8, 7, 1, 'SANTO TOMAS'),
(815, 8, 7, 2, 'CAPACMARCA'),
(816, 8, 7, 3, 'CHAMACA'),
(817, 8, 7, 4, 'COLQUEMARCA'),
(818, 8, 7, 5, 'LIVITACA'),
(819, 8, 7, 6, 'LLUSCO'),
(820, 8, 7, 7, 'QUIÑOTA'),
(821, 8, 7, 8, 'VELILLE'),
(822, 8, 8, 0, 'ESPINAR'),
(823, 8, 8, 1, 'ESPINAR'),
(824, 8, 8, 2, 'CONDOROMA'),
(825, 8, 8, 3, 'COPORAQUE'),
(826, 8, 8, 4, 'OCORURO'),
(827, 8, 8, 5, 'PALLPATA'),
(828, 8, 8, 6, 'PICHIGUA'),
(829, 8, 8, 7, 'SUYCKUTAMBO'),
(830, 8, 8, 8, 'ALTO PICHIGUA'),
(831, 8, 9, 0, 'LA CONVENCION'),
(832, 8, 9, 1, 'SANTA ANA'),
(833, 8, 9, 2, 'ECHARATE'),
(834, 8, 9, 3, 'HUAYOPATA'),
(835, 8, 9, 4, 'MARANURA'),
(836, 8, 9, 5, 'OCOBAMBA'),
(837, 8, 9, 6, 'QUELLOUNO'),
(838, 8, 9, 7, 'KIMBIRI'),
(839, 8, 9, 8, 'SANTA TERESA'),
(840, 8, 9, 9, 'VILCABAMBA'),
(841, 8, 9, 10, 'PICHARI'),
(842, 8, 10, 0, 'PARURO'),
(843, 8, 10, 1, 'PARURO'),
(844, 8, 10, 2, 'ACCHA'),
(845, 8, 10, 3, 'CCAPI'),
(846, 8, 10, 4, 'COLCHA'),
(847, 8, 10, 5, 'HUANOQUITE'),
(848, 8, 10, 6, 'OMACHA'),
(849, 8, 10, 7, 'PACCARITAMBO'),
(850, 8, 10, 8, 'PILLPINTO'),
(851, 8, 10, 9, 'YAURISQUE'),
(852, 8, 11, 0, 'PAUCARTAMBO'),
(853, 8, 11, 1, 'PAUCARTAMBO'),
(854, 8, 11, 2, 'CAICAY'),
(855, 8, 11, 3, 'CHALLABAMBA'),
(856, 8, 11, 4, 'COLQUEPATA'),
(857, 8, 11, 5, 'HUANCARANI'),
(858, 8, 11, 6, 'KOSÑIPATA'),
(859, 8, 12, 0, 'QUISPICANCHI'),
(860, 8, 12, 1, 'URCOS'),
(861, 8, 12, 2, 'ANDAHUAYLILLAS'),
(862, 8, 12, 3, 'CAMANTI'),
(863, 8, 12, 4, 'CCARHUAYO'),
(864, 8, 12, 5, 'CCATCA'),
(865, 8, 12, 6, 'CUSIPATA'),
(866, 8, 12, 7, 'HUARO'),
(867, 8, 12, 8, 'LUCRE'),
(868, 8, 12, 9, 'MARCAPATA'),
(869, 8, 12, 10, 'OCONGATE'),
(870, 8, 12, 11, 'OROPESA'),
(871, 8, 12, 12, 'QUIQUIJANA'),
(872, 8, 13, 0, 'URUBAMBA'),
(873, 8, 13, 1, 'URUBAMBA'),
(874, 8, 13, 2, 'CHINCHERO'),
(875, 8, 13, 3, 'HUAYLLABAMBA'),
(876, 8, 13, 4, 'MACHUPICCHU'),
(877, 8, 13, 5, 'MARAS'),
(878, 8, 13, 6, 'OLLANTAYTAMBO'),
(879, 8, 13, 7, 'YUCAY'),
(880, 9, 0, 0, 'HUANCAVELICA'),
(881, 9, 1, 0, 'HUANCAVELICA'),
(882, 9, 1, 1, 'HUANCAVELICA'),
(883, 9, 1, 2, 'ACOBAMBILLA'),
(884, 9, 1, 3, 'ACORIA'),
(885, 9, 1, 4, 'CONAYCA'),
(886, 9, 1, 5, 'CUENCA'),
(887, 9, 1, 6, 'HUACHOCOLPA'),
(888, 9, 1, 7, 'HUAYLLAHUARA'),
(889, 9, 1, 8, 'IZCUCHACA'),
(890, 9, 1, 9, 'LARIA'),
(891, 9, 1, 10, 'MANTA'),
(892, 9, 1, 11, 'MARISCAL CACERES'),
(893, 9, 1, 12, 'MOYA'),
(894, 9, 1, 13, 'NUEVO OCCORO'),
(895, 9, 1, 14, 'PALCA'),
(896, 9, 1, 15, 'PILCHACA'),
(897, 9, 1, 16, 'VILCA'),
(898, 9, 1, 17, 'YAULI'),
(899, 9, 1, 18, 'ASCENSION'),
(900, 9, 1, 19, 'HUANDO'),
(901, 9, 2, 0, 'ACOBAMBA'),
(902, 9, 2, 1, 'ACOBAMBA'),
(903, 9, 2, 2, 'ANDABAMBA'),
(904, 9, 2, 3, 'ANTA'),
(905, 9, 2, 4, 'CAJA'),
(906, 9, 2, 5, 'MARCAS'),
(907, 9, 2, 6, 'PAUCARA'),
(908, 9, 2, 7, 'POMACOCHA'),
(909, 9, 2, 8, 'ROSARIO'),
(910, 9, 3, 0, 'ANGARAES'),
(911, 9, 3, 1, 'LIRCAY'),
(912, 9, 3, 2, 'ANCHONGA'),
(913, 9, 3, 3, 'CALLANMARCA'),
(914, 9, 3, 4, 'CCOCHACCASA'),
(915, 9, 3, 5, 'CHINCHO'),
(916, 9, 3, 6, 'CONGALLA'),
(917, 9, 3, 7, 'HUANCA-HUANCA'),
(918, 9, 3, 8, 'HUAYLLAY GRANDE'),
(919, 9, 3, 9, 'JULCAMARCA'),
(920, 9, 3, 10, 'SAN ANTONIO DE ANTAPARCO'),
(921, 9, 3, 11, 'SANTO TOMAS DE PATA'),
(922, 9, 3, 12, 'SECCLLA'),
(923, 9, 4, 0, 'CASTROVIRREYNA'),
(924, 9, 4, 1, 'CASTROVIRREYNA'),
(925, 9, 4, 2, 'ARMA'),
(926, 9, 4, 3, 'AURAHUA'),
(927, 9, 4, 4, 'CAPILLAS'),
(928, 9, 4, 5, 'CHUPAMARCA'),
(929, 9, 4, 6, 'COCAS'),
(930, 9, 4, 7, 'HUACHOS'),
(931, 9, 4, 8, 'HUAMATAMBO'),
(932, 9, 4, 9, 'MOLLEPAMPA'),
(933, 9, 4, 10, 'SAN JUAN'),
(934, 9, 4, 11, 'SANTA ANA'),
(935, 9, 4, 12, 'TANTARA'),
(936, 9, 4, 13, 'TICRAPO'),
(937, 9, 5, 0, 'CHURCAMPA'),
(938, 9, 5, 1, 'CHURCAMPA'),
(939, 9, 5, 2, 'ANCO'),
(940, 9, 5, 3, 'CHINCHIHUASI'),
(941, 9, 5, 4, 'EL CARMEN'),
(942, 9, 5, 5, 'LA MERCED'),
(943, 9, 5, 6, 'LOCROJA'),
(944, 9, 5, 7, 'PAUCARBAMBA'),
(945, 9, 5, 8, 'SAN MIGUEL DE MAYOCC'),
(946, 9, 5, 9, 'SAN PEDRO DE CORIS'),
(947, 9, 5, 10, 'PACHAMARCA'),
(948, 9, 6, 0, 'HUAYTARA'),
(949, 9, 6, 1, 'HUAYTARA'),
(950, 9, 6, 2, 'AYAVI'),
(951, 9, 6, 3, 'CORDOVA'),
(952, 9, 6, 4, 'HUAYACUNDO ARMA'),
(953, 9, 6, 5, 'LARAMARCA'),
(954, 9, 6, 6, 'OCOYO'),
(955, 9, 6, 7, 'PILPICHACA'),
(956, 9, 6, 8, 'QUERCO'),
(957, 9, 6, 9, 'QUITO-ARMA'),
(958, 9, 6, 10, 'SAN ANTONIO DE CUSICANCHA'),
(959, 9, 6, 11, 'SAN FRANCISCO DE SANGAYAICO'),
(960, 9, 6, 12, 'SAN ISIDRO'),
(961, 9, 6, 13, 'SANTIAGO DE CHOCORVOS'),
(962, 9, 6, 14, 'SANTIAGO DE QUIRAHUARA'),
(963, 9, 6, 15, 'SANTO DOMINGO DE CAPILLAS'),
(964, 9, 6, 16, 'TAMBO'),
(965, 9, 7, 0, 'TAYACAJA'),
(966, 9, 7, 1, 'PAMPAS'),
(967, 9, 7, 2, 'ACOSTAMBO'),
(968, 9, 7, 3, 'ACRAQUIA'),
(969, 9, 7, 4, 'AHUAYCHA'),
(970, 9, 7, 5, 'COLCABAMBA'),
(971, 9, 7, 6, 'DANIEL HERNANDEZ'),
(972, 9, 7, 7, 'HUACHOCOLPA'),
(973, 9, 7, 9, 'HUARIBAMBA'),
(974, 9, 7, 10, 'ÑAHUIMPUQUIO'),
(975, 9, 7, 11, 'PAZOS'),
(976, 9, 7, 13, 'QUISHUAR'),
(977, 9, 7, 14, 'SALCABAMBA'),
(978, 9, 7, 15, 'SALCAHUASI'),
(979, 9, 7, 16, 'SAN MARCOS DE ROCCHAC'),
(980, 9, 7, 17, 'SURCUBAMBA'),
(981, 9, 7, 18, 'TINTAY PUNCU'),
(982, 10, 0, 0, 'HUANUCO'),
(983, 10, 1, 0, 'HUANUCO'),
(984, 10, 1, 1, 'HUANUCO'),
(985, 10, 1, 2, 'AMARILIS'),
(986, 10, 1, 3, 'CHINCHAO'),
(987, 10, 1, 4, 'CHURUBAMBA'),
(988, 10, 1, 5, 'MARGOS'),
(989, 10, 1, 6, 'QUISQUI'),
(990, 10, 1, 7, 'SAN FRANCISCO DE CAYRAN'),
(991, 10, 1, 8, 'SAN PEDRO DE CHAULAN'),
(992, 10, 1, 9, 'SANTA MARIA DEL VALLE'),
(993, 10, 1, 10, 'YARUMAYO'),
(994, 10, 1, 11, 'PILLCO MARCA'),
(995, 10, 2, 0, 'AMBO'),
(996, 10, 2, 1, 'AMBO'),
(997, 10, 2, 2, 'CAYNA'),
(998, 10, 2, 3, 'COLPAS'),
(999, 10, 2, 4, 'CONCHAMARCA'),
(1000, 10, 2, 5, 'HUACAR'),
(1001, 10, 2, 6, 'SAN FRANCISCO'),
(1002, 10, 2, 7, 'SAN RAFAEL'),
(1003, 10, 2, 8, 'TOMAY KICHWA'),
(1004, 10, 3, 0, 'DOS DE MAYO'),
(1005, 10, 3, 1, 'LA UNION'),
(1006, 10, 3, 7, 'CHUQUIS'),
(1007, 10, 3, 11, 'MARIAS'),
(1008, 10, 3, 13, 'PACHAS'),
(1009, 10, 3, 16, 'QUIVILLA'),
(1010, 10, 3, 17, 'RIPAN'),
(1011, 10, 3, 21, 'SHUNQUI'),
(1012, 10, 3, 22, 'SILLAPATA'),
(1013, 10, 3, 23, 'YANAS'),
(1014, 10, 4, 0, 'HUACAYBAMBA'),
(1015, 10, 4, 1, 'HUACAYBAMBA'),
(1016, 10, 4, 2, 'CANCHABAMBA'),
(1017, 10, 4, 3, 'COCHABAMBA'),
(1018, 10, 4, 4, 'PINRA'),
(1019, 10, 5, 0, 'HUAMALIES'),
(1020, 10, 5, 1, 'LLATA'),
(1021, 10, 5, 2, 'ARANCAY'),
(1022, 10, 5, 3, 'CHAVIN DE PARIARCA'),
(1023, 10, 5, 4, 'JACAS GRANDE'),
(1024, 10, 5, 5, 'JIRCAN'),
(1025, 10, 5, 6, 'MIRAFLORES'),
(1026, 10, 5, 7, 'MONZON'),
(1027, 10, 5, 8, 'PUNCHAO'),
(1028, 10, 5, 9, 'PUÑOS'),
(1029, 10, 5, 10, 'SINGA'),
(1030, 10, 5, 11, 'TANTAMAYO'),
(1031, 10, 6, 0, 'LEONCIO PRADO'),
(1032, 10, 6, 1, 'RUPA-RUPA'),
(1033, 10, 6, 2, 'DANIEL ALOMIAS ROBLES'),
(1034, 10, 6, 3, 'HERMILIO VALDIZAN'),
(1035, 10, 6, 4, 'JOSE CRESPO Y CASTILLO'),
(1036, 10, 6, 5, 'LUYANDO'),
(1037, 10, 6, 6, 'MARIANO DAMASO BERAUN'),
(1038, 10, 7, 0, 'MARAÑON'),
(1039, 10, 7, 1, 'HUACRACHUCO'),
(1040, 10, 7, 2, 'CHOLON'),
(1041, 10, 7, 3, 'SAN BUENAVENTURA'),
(1042, 10, 8, 0, 'PACHITEA'),
(1043, 10, 8, 1, 'PANAO'),
(1044, 10, 8, 2, 'CHAGLLA'),
(1045, 10, 8, 3, 'MOLINO'),
(1046, 10, 8, 4, 'UMARI'),
(1047, 10, 9, 0, 'PUERTO INCA'),
(1048, 10, 9, 1, 'PUERTO INCA'),
(1049, 10, 9, 2, 'CODO DEL POZUZO'),
(1050, 10, 9, 3, 'HONORIA'),
(1051, 10, 9, 4, 'TOURNAVISTA'),
(1052, 10, 9, 5, 'YUYAPICHIS'),
(1053, 10, 10, 0, 'LAURICOCHA'),
(1054, 10, 10, 1, 'JESUS'),
(1055, 10, 10, 2, 'BAÑOS'),
(1056, 10, 10, 3, 'JIVIA'),
(1057, 10, 10, 4, 'QUEROPALCA'),
(1058, 10, 10, 5, 'RONDOS'),
(1059, 10, 10, 6, 'SAN FRANCISCO DE ASIS'),
(1060, 10, 10, 7, 'SAN MIGUEL DE CAURI'),
(1061, 10, 11, 0, 'YAROWILCA'),
(1062, 10, 11, 1, 'CHAVINILLO'),
(1063, 10, 11, 2, 'CAHUAC'),
(1064, 10, 11, 3, 'CHACABAMBA'),
(1065, 10, 11, 4, 'APARICIO POMARES'),
(1066, 10, 11, 5, 'JACAS CHICO'),
(1067, 10, 11, 6, 'OBAS'),
(1068, 10, 11, 7, 'PAMPAMARCA'),
(1069, 10, 11, 8, 'CHORAS'),
(1070, 11, 0, 0, 'ICA'),
(1071, 11, 1, 0, 'ICA'),
(1072, 11, 1, 1, 'ICA'),
(1073, 11, 1, 2, 'LA TINGUIÑA'),
(1074, 11, 1, 3, 'LOS AQUIJES'),
(1075, 11, 1, 4, 'OCUCAJE'),
(1076, 11, 1, 5, 'PACHACUTEC'),
(1077, 11, 1, 6, 'PARCONA'),
(1078, 11, 1, 7, 'PUEBLO NUEVO'),
(1079, 11, 1, 8, 'SALAS'),
(1080, 11, 1, 9, 'SAN JOSE DE LOS MOLINOS'),
(1081, 11, 1, 10, 'SAN JUAN BAUTISTA'),
(1082, 11, 1, 11, 'SANTIAGO'),
(1083, 11, 1, 12, 'SUBTANJALLA'),
(1084, 11, 1, 13, 'TATE'),
(1085, 11, 1, 14, 'YAUCA DEL ROSARIO'),
(1086, 11, 2, 0, 'CHINCHA'),
(1087, 11, 2, 1, 'CHINCHA ALTA'),
(1088, 11, 2, 2, 'ALTO LARAN'),
(1089, 11, 2, 3, 'CHAVIN'),
(1090, 11, 2, 4, 'CHINCHA BAJA'),
(1091, 11, 2, 5, 'EL CARMEN'),
(1092, 11, 2, 6, 'GROCIO PRADO'),
(1093, 11, 2, 7, 'PUEBLO NUEVO'),
(1094, 11, 2, 8, 'SAN JUAN DE YANAC'),
(1095, 11, 2, 9, 'SAN PEDRO DE HUACARPANA'),
(1096, 11, 2, 10, 'SUNAMPE'),
(1097, 11, 2, 11, 'TAMBO DE MORA'),
(1098, 11, 3, 0, 'NAZCA'),
(1099, 11, 3, 1, 'NAZCA'),
(1100, 11, 3, 2, 'CHANGUILLO'),
(1101, 11, 3, 3, 'EL INGENIO'),
(1102, 11, 3, 4, 'MARCONA'),
(1103, 11, 3, 5, 'VISTA ALEGRE'),
(1104, 11, 4, 0, 'PALPA'),
(1105, 11, 4, 1, 'PALPA'),
(1106, 11, 4, 2, 'LLIPATA'),
(1107, 11, 4, 3, 'RIO GRANDE'),
(1108, 11, 4, 4, 'SANTA CRUZ'),
(1109, 11, 4, 5, 'TIBILLO'),
(1110, 11, 5, 0, 'PISCO'),
(1111, 11, 5, 1, 'PISCO'),
(1112, 11, 5, 2, 'HUANCANO'),
(1113, 11, 5, 3, 'HUMAY'),
(1114, 11, 5, 4, 'INDEPENDENCIA'),
(1115, 11, 5, 5, 'PARACAS'),
(1116, 11, 5, 6, 'SAN ANDRES'),
(1117, 11, 5, 7, 'SAN CLEMENTE'),
(1118, 11, 5, 8, 'TUPAC AMARU INCA'),
(1119, 12, 0, 0, 'JUNIN'),
(1120, 12, 1, 0, 'HUANCAYO'),
(1121, 12, 1, 1, 'HUANCAYO'),
(1122, 12, 1, 4, 'CARHUACALLANGA'),
(1123, 12, 1, 5, 'CHACAPAMPA'),
(1124, 12, 1, 6, 'CHICCHE'),
(1125, 12, 1, 7, 'CHILCA'),
(1126, 12, 1, 8, 'CHONGOS ALTO'),
(1127, 12, 1, 11, 'CHUPURO'),
(1128, 12, 1, 12, 'COLCA'),
(1129, 12, 1, 13, 'CULLHUAS'),
(1130, 12, 1, 14, 'EL TAMBO'),
(1131, 12, 1, 16, 'HUACRAPUQUIO'),
(1132, 12, 1, 17, 'HUALHUAS'),
(1133, 12, 1, 19, 'HUANCAN'),
(1134, 12, 1, 20, 'HUASICANCHA'),
(1135, 12, 1, 21, 'HUAYUCACHI'),
(1136, 12, 1, 22, 'INGENIO'),
(1137, 12, 1, 24, 'PARIAHUANCA'),
(1138, 12, 1, 25, 'PILCOMAYO'),
(1139, 12, 1, 26, 'PUCARA'),
(1140, 12, 1, 27, 'QUICHUAY'),
(1141, 12, 1, 28, 'QUILCAS'),
(1142, 12, 1, 29, 'SAN AGUSTIN'),
(1143, 12, 1, 30, 'SAN JERONIMO DE TUNAN'),
(1144, 12, 1, 32, 'SAÑO'),
(1145, 12, 1, 33, 'SAPALLANGA'),
(1146, 12, 1, 34, 'SICAYA'),
(1147, 12, 1, 35, 'SANTO DOMINGO DE ACOBAMBA'),
(1148, 12, 1, 36, 'VIQUES'),
(1149, 12, 2, 0, 'CONCEPCION'),
(1150, 12, 2, 1, 'CONCEPCION'),
(1151, 12, 2, 2, 'ACO'),
(1152, 12, 2, 3, 'ANDAMARCA'),
(1153, 12, 2, 4, 'CHAMBARA'),
(1154, 12, 2, 5, 'COCHAS'),
(1155, 12, 2, 6, 'COMAS'),
(1156, 12, 2, 7, 'HEROINAS TOLEDO'),
(1157, 12, 2, 8, 'MANZANARES'),
(1158, 12, 2, 9, 'MARISCAL CASTILLA'),
(1159, 12, 2, 10, 'MATAHUASI'),
(1160, 12, 2, 11, 'MITO'),
(1161, 12, 2, 12, 'NUEVE DE JULIO'),
(1162, 12, 2, 13, 'ORCOTUNA'),
(1163, 12, 2, 14, 'SAN JOSE DE QUERO'),
(1164, 12, 2, 15, 'SANTA ROSA DE OCOPA'),
(1165, 12, 3, 0, 'CHANCHAMAYO'),
(1166, 12, 3, 1, 'CHANCHAMAYO'),
(1167, 12, 3, 2, 'PERENE'),
(1168, 12, 3, 3, 'PICHANAQUI'),
(1169, 12, 3, 4, 'SAN LUIS DE SHUARO'),
(1170, 12, 3, 5, 'SAN RAMON'),
(1171, 12, 3, 6, 'VITOC'),
(1172, 12, 4, 0, 'JAUJA'),
(1173, 12, 4, 1, 'JAUJA'),
(1174, 12, 4, 2, 'ACOLLA'),
(1175, 12, 4, 3, 'APATA'),
(1176, 12, 4, 4, 'ATAURA'),
(1177, 12, 4, 5, 'CANCHAYLLO'),
(1178, 12, 4, 6, 'CURICACA'),
(1179, 12, 4, 7, 'EL MANTARO'),
(1180, 12, 4, 8, 'HUAMALI'),
(1181, 12, 4, 9, 'HUARIPAMPA'),
(1182, 12, 4, 10, 'HUERTAS'),
(1183, 12, 4, 11, 'JANJAILLO'),
(1184, 12, 4, 12, 'JULCAN'),
(1185, 12, 4, 13, 'LEONOR ORDOÑEZ'),
(1186, 12, 4, 14, 'LLOCLLAPAMPA'),
(1187, 12, 4, 15, 'MARCO'),
(1188, 12, 4, 16, 'MASMA'),
(1189, 12, 4, 17, 'MASMA CHICCHE'),
(1190, 12, 4, 18, 'MOLINOS'),
(1191, 12, 4, 19, 'MONOBAMBA'),
(1192, 12, 4, 20, 'MUQUI'),
(1193, 12, 4, 21, 'MUQUIYAUYO'),
(1194, 12, 4, 22, 'PACA'),
(1195, 12, 4, 23, 'PACCHA'),
(1196, 12, 4, 24, 'PANCAN'),
(1197, 12, 4, 25, 'PARCO'),
(1198, 12, 4, 26, 'POMACANCHA'),
(1199, 12, 4, 27, 'RICRAN'),
(1200, 12, 4, 28, 'SAN LORENZO'),
(1201, 12, 4, 29, 'SAN PEDRO DE CHUNAN'),
(1202, 12, 4, 30, 'SAUSA'),
(1203, 12, 4, 31, 'SINCOS'),
(1204, 12, 4, 32, 'TUNAN MARCA'),
(1205, 12, 4, 33, 'YAULI'),
(1206, 12, 4, 34, 'YAUYOS'),
(1207, 12, 5, 0, 'JUNIN'),
(1208, 12, 5, 1, 'JUNIN'),
(1209, 12, 5, 2, 'CARHUAMAYO'),
(1210, 12, 5, 3, 'ONDORES'),
(1211, 12, 5, 4, 'ULCUMAYO'),
(1212, 12, 6, 0, 'SATIPO'),
(1213, 12, 6, 1, 'SATIPO'),
(1214, 12, 6, 2, 'COVIRIALI'),
(1215, 12, 6, 3, 'LLAYLLA'),
(1216, 12, 6, 4, 'MAZAMARI'),
(1217, 12, 6, 5, 'PAMPA HERMOSA'),
(1218, 12, 6, 6, 'PANGOA'),
(1219, 12, 6, 7, 'RIO NEGRO'),
(1220, 12, 6, 8, 'RIO TAMBO'),
(1221, 12, 7, 0, 'TARMA'),
(1222, 12, 7, 1, 'TARMA'),
(1223, 12, 7, 2, 'ACOBAMBA'),
(1224, 12, 7, 3, 'HUARICOLCA'),
(1225, 12, 7, 4, 'HUASAHUASI'),
(1226, 12, 7, 5, 'LA UNION'),
(1227, 12, 7, 6, 'PALCA'),
(1228, 12, 7, 7, 'PALCAMAYO'),
(1229, 12, 7, 8, 'SAN PEDRO DE CAJAS'),
(1230, 12, 7, 9, 'TAPO'),
(1231, 12, 8, 0, 'YAULI'),
(1232, 12, 8, 1, 'LA OROYA'),
(1233, 12, 8, 2, 'CHACAPALPA'),
(1234, 12, 8, 3, 'HUAY-HUAY'),
(1235, 12, 8, 4, 'MARCAPOMACOCHA'),
(1236, 12, 8, 5, 'MOROCOCHA'),
(1237, 12, 8, 6, 'PACCHA'),
(1238, 12, 8, 7, 'SANTA BARBARA DE CARHUACAYAN'),
(1239, 12, 8, 8, 'SANTA ROSA DE SACCO'),
(1240, 12, 8, 9, 'SUITUCANCHA'),
(1241, 12, 8, 10, 'YAULI'),
(1242, 12, 9, 0, 'CHUPACA'),
(1243, 12, 9, 1, 'CHUPACA'),
(1244, 12, 9, 2, 'AHUAC'),
(1245, 12, 9, 3, 'CHONGOS BAJO'),
(1246, 12, 9, 4, 'HUACHAC'),
(1247, 12, 9, 5, 'HUAMANCACA CHICO'),
(1248, 12, 9, 6, 'SAN JUAN DE YSCOS'),
(1249, 12, 9, 7, 'SAN JUAN DE JARPA'),
(1250, 12, 9, 8, 'TRES DE DICIEMBRE'),
(1251, 12, 9, 9, 'YANACANCHA'),
(1252, 13, 0, 0, 'LA LIBERTAD'),
(1253, 13, 1, 0, 'TRUJILLO'),
(1254, 13, 1, 1, 'TRUJILLO'),
(1255, 13, 1, 2, 'EL PORVENIR'),
(1256, 13, 1, 3, 'FLORENCIA DE MORA'),
(1257, 13, 1, 4, 'HUANCHACO'),
(1258, 13, 1, 5, 'LA ESPERANZA'),
(1259, 13, 1, 6, 'LAREDO'),
(1260, 13, 1, 7, 'MOCHE'),
(1261, 13, 1, 8, 'POROTO'),
(1262, 13, 1, 9, 'SALAVERRY'),
(1263, 13, 1, 10, 'SIMBAL'),
(1264, 13, 1, 11, 'VICTOR LARCO HERRERA'),
(1265, 13, 2, 0, 'ASCOPE'),
(1266, 13, 2, 1, 'ASCOPE'),
(1267, 13, 2, 2, 'CHICAMA'),
(1268, 13, 2, 3, 'CHOCOPE'),
(1269, 13, 2, 4, 'MAGDALENA DE CAO'),
(1270, 13, 2, 5, 'PAIJAN'),
(1271, 13, 2, 6, 'RAZURI'),
(1272, 13, 2, 7, 'SANTIAGO DE CAO'),
(1273, 13, 2, 8, 'CASA GRANDE'),
(1274, 13, 3, 0, 'BOLIVAR'),
(1275, 13, 3, 1, 'BOLIVAR'),
(1276, 13, 3, 2, 'BAMBAMARCA'),
(1277, 13, 3, 3, 'CONDORMARCA'),
(1278, 13, 3, 4, 'LONGOTEA'),
(1279, 13, 3, 5, 'UCHUMARCA'),
(1280, 13, 3, 6, 'UCUNCHA'),
(1281, 13, 4, 0, 'CHEPEN'),
(1282, 13, 4, 1, 'CHEPEN'),
(1283, 13, 4, 2, 'PACANGA'),
(1284, 13, 4, 3, 'PUEBLO NUEVO'),
(1285, 13, 5, 0, 'JULCAN'),
(1286, 13, 5, 1, 'JULCAN'),
(1287, 13, 5, 2, 'CALAMARCA'),
(1288, 13, 5, 3, 'CARABAMBA'),
(1289, 13, 5, 4, 'HUASO'),
(1290, 13, 6, 0, 'OTUZCO'),
(1291, 13, 6, 1, 'OTUZCO'),
(1292, 13, 6, 2, 'AGALLPAMPA'),
(1293, 13, 6, 4, 'CHARAT'),
(1294, 13, 6, 5, 'HUARANCHAL'),
(1295, 13, 6, 6, 'LA CUESTA'),
(1296, 13, 6, 8, 'MACHE'),
(1297, 13, 6, 10, 'PARANDAY'),
(1298, 13, 6, 11, 'SALPO'),
(1299, 13, 6, 13, 'SINSICAP'),
(1300, 13, 6, 14, 'USQUIL'),
(1301, 13, 7, 0, 'PACASMAYO'),
(1302, 13, 7, 1, 'SAN PEDRO DE LLOC'),
(1303, 13, 7, 2, 'GUADALUPE'),
(1304, 13, 7, 3, 'JEQUETEPEQUE'),
(1305, 13, 7, 4, 'PACASMAYO'),
(1306, 13, 7, 5, 'SAN JOSE'),
(1307, 13, 8, 0, 'PATAZ'),
(1308, 13, 8, 1, 'TAYABAMBA'),
(1309, 13, 8, 2, 'BULDIBUYO'),
(1310, 13, 8, 3, 'CHILLIA'),
(1311, 13, 8, 4, 'HUANCASPATA'),
(1312, 13, 8, 5, 'HUAYLILLAS'),
(1313, 13, 8, 6, 'HUAYO'),
(1314, 13, 8, 7, 'ONGON'),
(1315, 13, 8, 8, 'PARCOY'),
(1316, 13, 8, 9, 'PATAZ'),
(1317, 13, 8, 10, 'PIAS'),
(1318, 13, 8, 11, 'SANTIAGO DE CHALLAS'),
(1319, 13, 8, 12, 'TAURIJA'),
(1320, 13, 8, 13, 'URPAY'),
(1321, 13, 9, 0, 'SANCHEZ CARRION'),
(1322, 13, 9, 1, 'HUAMACHUCO'),
(1323, 13, 9, 2, 'CHUGAY'),
(1324, 13, 9, 3, 'COCHORCO'),
(1325, 13, 9, 4, 'CURGOS'),
(1326, 13, 9, 5, 'MARCABAL'),
(1327, 13, 9, 6, 'SANAGORAN'),
(1328, 13, 9, 7, 'SARIN'),
(1329, 13, 9, 8, 'SARTIMBAMBA'),
(1330, 13, 10, 0, 'SANTIAGO DE CHUCO'),
(1331, 13, 10, 1, 'SANTIAGO DE CHUCO'),
(1332, 13, 10, 2, 'ANGASMARCA'),
(1333, 13, 10, 3, 'CACHICADAN'),
(1334, 13, 10, 4, 'MOLLEBAMBA'),
(1335, 13, 10, 5, 'MOLLEPATA'),
(1336, 13, 10, 6, 'QUIRUVILCA'),
(1337, 13, 10, 7, 'SANTA CRUZ DE CHUCA'),
(1338, 13, 10, 8, 'SITABAMBA'),
(1339, 13, 11, 0, 'GRAN CHIMU'),
(1340, 13, 11, 1, 'CASCAS'),
(1341, 13, 11, 2, 'LUCMA'),
(1342, 13, 11, 3, 'COMPIN'),
(1343, 13, 11, 4, 'SAYAPULLO'),
(1344, 13, 12, 0, 'VIRU'),
(1345, 13, 12, 1, 'VIRU'),
(1346, 13, 12, 2, 'CHAO'),
(1347, 13, 12, 3, 'GUADALUPITO'),
(1348, 14, 0, 0, 'LAMBAYEQUE'),
(1349, 14, 1, 0, 'CHICLAYO'),
(1350, 14, 1, 1, 'CHICLAYO'),
(1351, 14, 1, 2, 'CHONGOYAPE'),
(1352, 14, 1, 3, 'ETEN'),
(1353, 14, 1, 4, 'ETEN PUERTO'),
(1354, 14, 1, 5, 'JOSE LEONARDO ORTIZ'),
(1355, 14, 1, 6, 'LA VICTORIA'),
(1356, 14, 1, 7, 'LAGUNAS'),
(1357, 14, 1, 8, 'MONSEFU'),
(1358, 14, 1, 9, 'NUEVA ARICA'),
(1359, 14, 1, 10, 'OYOTUN'),
(1360, 14, 1, 11, 'PICSI'),
(1361, 14, 1, 12, 'PIMENTEL'),
(1362, 14, 1, 13, 'REQUE'),
(1363, 14, 1, 14, 'SANTA ROSA'),
(1364, 14, 1, 15, 'SAÑA'),
(1365, 14, 1, 16, 'CAYALTI'),
(1366, 14, 1, 17, 'PATAPO'),
(1367, 14, 1, 18, 'POMALCA'),
(1368, 14, 1, 19, 'PUCALA'),
(1369, 14, 1, 20, 'TUMAN'),
(1370, 14, 2, 0, 'FERREÑAFE'),
(1371, 14, 2, 1, 'FERREÑAFE'),
(1372, 14, 2, 2, 'CAÑARIS'),
(1373, 14, 2, 3, 'INCAHUASI'),
(1374, 14, 2, 4, 'MANUEL ANTONIO MESONES MURO'),
(1375, 14, 2, 5, 'PITIPO'),
(1376, 14, 2, 6, 'PUEBLO NUEVO'),
(1377, 14, 3, 0, 'LAMBAYEQUE'),
(1378, 14, 3, 1, 'LAMBAYEQUE'),
(1379, 14, 3, 2, 'CHOCHOPE'),
(1380, 14, 3, 3, 'ILLIMO'),
(1381, 14, 3, 4, 'JAYANCA'),
(1382, 14, 3, 5, 'MOCHUMI'),
(1383, 14, 3, 6, 'MORROPE'),
(1384, 14, 3, 7, 'MOTUPE'),
(1385, 14, 3, 8, 'OLMOS'),
(1386, 14, 3, 9, 'PACORA'),
(1387, 14, 3, 10, 'SALAS'),
(1388, 14, 3, 11, 'SAN JOSE'),
(1389, 14, 3, 12, 'TUCUME'),
(1390, 15, 0, 0, 'LIMA'),
(1391, 15, 1, 0, 'LIMA'),
(1392, 15, 1, 1, 'LIMA'),
(1393, 15, 1, 2, 'ANCON'),
(1394, 15, 1, 3, 'ATE'),
(1395, 15, 1, 4, 'BARRANCO'),
(1396, 15, 1, 5, 'BREÑA'),
(1397, 15, 1, 6, 'CARABAYLLO'),
(1398, 15, 1, 7, 'CHACLACAYO'),
(1399, 15, 1, 8, 'CHORRILLOS'),
(1400, 15, 1, 9, 'CIENEGUILLA'),
(1401, 15, 1, 10, 'COMAS'),
(1402, 15, 1, 11, 'EL AGUSTINO'),
(1403, 15, 1, 12, 'INDEPENDENCIA'),
(1404, 15, 1, 13, 'JESUS MARIA'),
(1405, 15, 1, 14, 'LA MOLINA'),
(1406, 15, 1, 15, 'LA VICTORIA'),
(1407, 15, 1, 16, 'LINCE'),
(1408, 15, 1, 17, 'LOS OLIVOS'),
(1409, 15, 1, 18, 'LURIGANCHO'),
(1410, 15, 1, 19, 'LURIN'),
(1411, 15, 1, 20, 'MAGDALENA DEL MAR'),
(1412, 15, 1, 21, 'MAGDALENA VIEJA'),
(1413, 15, 1, 22, 'MIRAFLORES'),
(1414, 15, 1, 23, 'PACHACAMAC'),
(1415, 15, 1, 24, 'PUCUSANA'),
(1416, 15, 1, 25, 'PUENTE PIEDRA'),
(1417, 15, 1, 26, 'PUNTA HERMOSA'),
(1418, 15, 1, 27, 'PUNTA NEGRA'),
(1419, 15, 1, 28, 'RIMAC'),
(1420, 15, 1, 29, 'SAN BARTOLO'),
(1421, 15, 1, 30, 'SAN BORJA'),
(1422, 15, 1, 31, 'SAN ISIDRO'),
(1423, 15, 1, 32, 'SAN JUAN DE LURIGANCHO'),
(1424, 15, 1, 33, 'SAN JUAN DE MIRAFLORES'),
(1425, 15, 1, 34, 'SAN LUIS'),
(1426, 15, 1, 35, 'SAN MARTIN DE PORRES'),
(1427, 15, 1, 36, 'SAN MIGUEL'),
(1428, 15, 1, 37, 'SANTA ANITA'),
(1429, 15, 1, 38, 'SANTA MARIA DEL MAR'),
(1430, 15, 1, 39, 'SANTA ROSA'),
(1431, 15, 1, 40, 'SANTIAGO DE SURCO'),
(1432, 15, 1, 41, 'SURQUILLO'),
(1433, 15, 1, 42, 'VILLA EL SALVADOR'),
(1434, 15, 1, 43, 'VILLA MARIA DEL TRIUNFO'),
(1435, 15, 2, 0, 'BARRANCA'),
(1436, 15, 2, 1, 'BARRANCA'),
(1437, 15, 2, 2, 'PARAMONGA'),
(1438, 15, 2, 3, 'PATIVILCA'),
(1439, 15, 2, 4, 'SUPE'),
(1440, 15, 2, 5, 'SUPE PUERTO'),
(1441, 15, 3, 0, 'CAJATAMBO'),
(1442, 15, 3, 1, 'CAJATAMBO'),
(1443, 15, 3, 2, 'COPA'),
(1444, 15, 3, 3, 'GORGOR'),
(1445, 15, 3, 4, 'HUANCAPON'),
(1446, 15, 3, 5, 'MANAS'),
(1447, 15, 4, 0, 'CANTA'),
(1448, 15, 4, 1, 'CANTA'),
(1449, 15, 4, 2, 'ARAHUAY'),
(1450, 15, 4, 3, 'HUAMANTANGA'),
(1451, 15, 4, 4, 'HUAROS'),
(1452, 15, 4, 5, 'LACHAQUI'),
(1453, 15, 4, 6, 'SAN BUENAVENTURA'),
(1454, 15, 4, 7, 'SANTA ROSA DE QUIVES'),
(1455, 15, 5, 0, 'CAÑETE'),
(1456, 15, 5, 1, 'SAN VICENTE DE CAÑETE'),
(1457, 15, 5, 2, 'ASIA'),
(1458, 15, 5, 3, 'CALANGO'),
(1459, 15, 5, 4, 'CERRO AZUL'),
(1460, 15, 5, 5, 'CHILCA'),
(1461, 15, 5, 6, 'COAYLLO'),
(1462, 15, 5, 7, 'IMPERIAL'),
(1463, 15, 5, 8, 'LUNAHUANA'),
(1464, 15, 5, 9, 'MALA'),
(1465, 15, 5, 10, 'NUEVO IMPERIAL'),
(1466, 15, 5, 11, 'PACARAN'),
(1467, 15, 5, 12, 'QUILMANA'),
(1468, 15, 5, 13, 'SAN ANTONIO'),
(1469, 15, 5, 14, 'SAN LUIS'),
(1470, 15, 5, 15, 'SANTA CRUZ DE FLORES'),
(1471, 15, 5, 16, 'ZUÑIGA'),
(1472, 15, 6, 0, 'HUARAL'),
(1473, 15, 6, 1, 'HUARAL'),
(1474, 15, 6, 2, 'ATAVILLOS ALTO'),
(1475, 15, 6, 3, 'ATAVILLOS BAJO'),
(1476, 15, 6, 4, 'AUCALLAMA'),
(1477, 15, 6, 5, 'CHANCAY'),
(1478, 15, 6, 6, 'IHUARI'),
(1479, 15, 6, 7, 'LAMPIAN'),
(1480, 15, 6, 8, 'PACARAOS'),
(1481, 15, 6, 9, 'SAN MIGUEL DE ACOS'),
(1482, 15, 6, 10, 'SANTA CRUZ DE ANDAMARCA'),
(1483, 15, 6, 11, 'SUMBILCA'),
(1484, 15, 6, 12, 'VEINTISIETE DE NOVIEMBRE'),
(1485, 15, 7, 0, 'HUAROCHIRI'),
(1486, 15, 7, 1, 'MATUCANA'),
(1487, 15, 7, 2, 'ANTIOQUIA'),
(1488, 15, 7, 3, 'CALLAHUANCA'),
(1489, 15, 7, 4, 'CARAMPOMA'),
(1490, 15, 7, 5, 'CHICLA'),
(1491, 15, 7, 6, 'CUENCA'),
(1492, 15, 7, 7, 'HUACHUPAMPA'),
(1493, 15, 7, 8, 'HUANZA'),
(1494, 15, 7, 9, 'HUAROCHIRI'),
(1495, 15, 7, 10, 'LAHUAYTAMBO'),
(1496, 15, 7, 11, 'LANGA'),
(1497, 15, 7, 12, 'LARAOS'),
(1498, 15, 7, 13, 'MARIATANA'),
(1499, 15, 7, 14, 'RICARDO PALMA'),
(1500, 15, 7, 15, 'SAN ANDRES DE TUPICOCHA'),
(1501, 15, 7, 16, 'SAN ANTONIO'),
(1502, 15, 7, 17, 'SAN BARTOLOME'),
(1503, 15, 7, 18, 'SAN DAMIAN'),
(1504, 15, 7, 19, 'SAN JUAN DE IRIS'),
(1505, 15, 7, 20, 'SAN JUAN DE TANTARANCHE'),
(1506, 15, 7, 21, 'SAN LORENZO DE QUINTI'),
(1507, 15, 7, 22, 'SAN MATEO'),
(1508, 15, 7, 23, 'SAN MATEO DE OTAO'),
(1509, 15, 7, 24, 'SAN PEDRO DE CASTA'),
(1510, 15, 7, 25, 'SAN PEDRO DE HUANCAYRE'),
(1511, 15, 7, 26, 'SANGALLAYA'),
(1512, 15, 7, 27, 'SANTA CRUZ DE COCACHACRA'),
(1513, 15, 7, 28, 'SANTA EULALIA'),
(1514, 15, 7, 29, 'SANTIAGO DE ANCHUCAYA'),
(1515, 15, 7, 30, 'SANTIAGO DE TUNA'),
(1516, 15, 7, 31, 'SANTO DOMINGO DE LOS OLLEROS'),
(1517, 15, 7, 32, 'SURCO'),
(1518, 15, 8, 0, 'HUAURA'),
(1519, 15, 8, 1, 'HUACHO'),
(1520, 15, 8, 2, 'AMBAR'),
(1521, 15, 8, 3, 'CALETA DE CARQUIN'),
(1522, 15, 8, 4, 'CHECRAS'),
(1523, 15, 8, 5, 'HUALMAY'),
(1524, 15, 8, 6, 'HUAURA'),
(1525, 15, 8, 7, 'LEONCIO PRADO'),
(1526, 15, 8, 8, 'PACCHO'),
(1527, 15, 8, 9, 'SANTA LEONOR'),
(1528, 15, 8, 10, 'SANTA MARIA'),
(1529, 15, 8, 11, 'SAYAN'),
(1530, 15, 8, 12, 'VEGUETA'),
(1531, 15, 9, 0, 'OYON'),
(1532, 15, 9, 1, 'OYON'),
(1533, 15, 9, 2, 'ANDAJES'),
(1534, 15, 9, 3, 'CAUJUL'),
(1535, 15, 9, 4, 'COCHAMARCA'),
(1536, 15, 9, 5, 'NAVAN'),
(1537, 15, 9, 6, 'PACHANGARA'),
(1538, 15, 10, 0, 'YAUYOS'),
(1539, 15, 10, 1, 'YAUYOS'),
(1540, 15, 10, 2, 'ALIS'),
(1541, 15, 10, 3, 'AYAUCA'),
(1542, 15, 10, 4, 'AYAVIRI'),
(1543, 15, 10, 5, 'AZANGARO'),
(1544, 15, 10, 6, 'CACRA'),
(1545, 15, 10, 7, 'CARANIA'),
(1546, 15, 10, 8, 'CATAHUASI'),
(1547, 15, 10, 9, 'CHOCOS'),
(1548, 15, 10, 10, 'COCHAS'),
(1549, 15, 10, 11, 'COLONIA'),
(1550, 15, 10, 12, 'HONGOS'),
(1551, 15, 10, 13, 'HUAMPARA'),
(1552, 15, 10, 14, 'HUANCAYA'),
(1553, 15, 10, 15, 'HUANGASCAR'),
(1554, 15, 10, 16, 'HUANTAN'),
(1555, 15, 10, 17, 'HUAÑEC'),
(1556, 15, 10, 18, 'LARAOS'),
(1557, 15, 10, 19, 'LINCHA'),
(1558, 15, 10, 20, 'MADEAN'),
(1559, 15, 10, 21, 'MIRAFLORES'),
(1560, 15, 10, 22, 'OMAS'),
(1561, 15, 10, 23, 'PUTINZA'),
(1562, 15, 10, 24, 'QUINCHES'),
(1563, 15, 10, 25, 'QUINOCAY'),
(1564, 15, 10, 26, 'SAN JOAQUIN'),
(1565, 15, 10, 27, 'SAN PEDRO DE PILAS'),
(1566, 15, 10, 28, 'TANTA'),
(1567, 15, 10, 29, 'TAURIPAMPA'),
(1568, 15, 10, 30, 'TOMAS'),
(1569, 15, 10, 31, 'TUPE'),
(1570, 15, 10, 32, 'VIÑAC'),
(1571, 15, 10, 33, 'VITIS'),
(1572, 16, 0, 0, 'LORETO'),
(1573, 16, 1, 0, 'MAYNAS'),
(1574, 16, 1, 1, 'IQUITOS'),
(1575, 16, 1, 2, 'ALTO NANAY'),
(1576, 16, 1, 3, 'FERNANDO LORES'),
(1577, 16, 1, 4, 'INDIANA'),
(1578, 16, 1, 5, 'LAS AMAZONAS'),
(1579, 16, 1, 6, 'MAZAN'),
(1580, 16, 1, 7, 'NAPO'),
(1581, 16, 1, 8, 'PUNCHANA'),
(1582, 16, 1, 9, 'PUTUMAYO'),
(1583, 16, 1, 10, 'TORRES CAUSANA'),
(1584, 16, 1, 12, 'BELEN'),
(1585, 16, 1, 13, 'SAN JUAN BAUTISTA'),
(1586, 16, 1, 14, 'TENIENTE MANUEL CLAVERO'),
(1587, 16, 2, 0, 'ALTO AMAZONAS'),
(1588, 16, 2, 1, 'YURIMAGUAS'),
(1589, 16, 2, 2, 'BALSAPUERTO'),
(1590, 16, 2, 5, 'JEBEROS'),
(1591, 16, 2, 6, 'LAGUNAS'),
(1592, 16, 2, 10, 'SANTA CRUZ'),
(1593, 16, 2, 11, 'TENIENTE CESAR LOPEZ ROJAS'),
(1594, 16, 3, 0, 'LORETO'),
(1595, 16, 3, 1, 'NAUTA'),
(1596, 16, 3, 2, 'PARINARI'),
(1597, 16, 3, 3, 'TIGRE'),
(1598, 16, 3, 4, 'TROMPETEROS'),
(1599, 16, 3, 5, 'URARINAS'),
(1600, 16, 4, 0, 'MARISCAL RAMON CASTILLA'),
(1601, 16, 4, 1, 'RAMON CASTILLA'),
(1602, 16, 4, 2, 'PEBAS'),
(1603, 16, 4, 3, 'YAVARI'),
(1604, 16, 4, 4, 'SAN PABLO'),
(1605, 16, 5, 0, 'REQUENA'),
(1606, 16, 5, 1, 'REQUENA'),
(1607, 16, 5, 2, 'ALTO TAPICHE'),
(1608, 16, 5, 3, 'CAPELO'),
(1609, 16, 5, 4, 'EMILIO SAN MARTIN'),
(1610, 16, 5, 5, 'MAQUIA'),
(1611, 16, 5, 6, 'PUINAHUA'),
(1612, 16, 5, 7, 'SAQUENA'),
(1613, 16, 5, 8, 'SOPLIN'),
(1614, 16, 5, 9, 'TAPICHE'),
(1615, 16, 5, 10, 'JENARO HERRERA'),
(1616, 16, 5, 11, 'YAQUERANA'),
(1617, 16, 6, 0, 'UCAYALI'),
(1618, 16, 6, 1, 'CONTAMANA'),
(1619, 16, 6, 2, 'INAHUAYA'),
(1620, 16, 6, 3, 'PADRE MARQUEZ'),
(1621, 16, 6, 4, 'PAMPA HERMOSA'),
(1622, 16, 6, 5, 'SARAYACU'),
(1623, 16, 6, 6, 'VARGAS GUERRA'),
(1624, 16, 7, 0, 'DATEM DEL MARAÑON'),
(1625, 16, 7, 1, 'BARRANCA'),
(1626, 16, 7, 2, 'CAHUAPANAS'),
(1627, 16, 7, 3, 'MANSERICHE'),
(1628, 16, 7, 4, 'MORONA'),
(1629, 16, 7, 5, 'PASTAZA'),
(1630, 16, 7, 6, 'ANDOAS'),
(1631, 17, 0, 0, 'MADRE DE DIOS'),
(1632, 17, 1, 0, 'TAMBOPATA'),
(1633, 17, 1, 1, 'TAMBOPATA'),
(1634, 17, 1, 2, 'INAMBARI'),
(1635, 17, 1, 3, 'LAS PIEDRAS'),
(1636, 17, 1, 4, 'LABERINTO'),
(1637, 17, 2, 0, 'MANU'),
(1638, 17, 2, 1, 'MANU'),
(1639, 17, 2, 2, 'FITZCARRALD'),
(1640, 17, 2, 3, 'MADRE DE DIOS'),
(1641, 17, 2, 4, 'HUEPETUHE'),
(1642, 17, 3, 0, 'TAHUAMANU'),
(1643, 17, 3, 1, 'IÑAPARI'),
(1644, 17, 3, 2, 'IBERIA'),
(1645, 17, 3, 3, 'TAHUAMANU'),
(1646, 18, 0, 0, 'MOQUEGUA'),
(1647, 18, 1, 0, 'MARISCAL NIETO'),
(1648, 18, 1, 1, 'MOQUEGUA'),
(1649, 18, 1, 2, 'CARUMAS'),
(1650, 18, 1, 3, 'CUCHUMBAYA'),
(1651, 18, 1, 4, 'SAMEGUA'),
(1652, 18, 1, 5, 'SAN CRISTOBAL'),
(1653, 18, 1, 6, 'TORATA'),
(1654, 18, 2, 0, 'GENERAL SANCHEZ CERRO'),
(1655, 18, 2, 1, 'OMATE'),
(1656, 18, 2, 2, 'CHOJATA'),
(1657, 18, 2, 3, 'COALAQUE'),
(1658, 18, 2, 4, 'ICHUÑA'),
(1659, 18, 2, 5, 'LA CAPILLA'),
(1660, 18, 2, 6, 'LLOQUE'),
(1661, 18, 2, 7, 'MATALAQUE'),
(1662, 18, 2, 8, 'PUQUINA'),
(1663, 18, 2, 9, 'QUINISTAQUILLAS'),
(1664, 18, 2, 10, 'UBINAS'),
(1665, 18, 2, 11, 'YUNGA'),
(1666, 18, 3, 0, 'ILO'),
(1667, 18, 3, 1, 'ILO'),
(1668, 18, 3, 2, 'EL ALGARROBAL'),
(1669, 18, 3, 3, 'PACOCHA'),
(1670, 19, 0, 0, 'PASCO'),
(1671, 19, 1, 0, 'PASCO'),
(1672, 19, 1, 1, 'CHAUPIMARCA'),
(1673, 19, 1, 2, 'HUACHON'),
(1674, 19, 1, 3, 'HUARIACA'),
(1675, 19, 1, 4, 'HUAYLLAY'),
(1676, 19, 1, 5, 'NINACACA'),
(1677, 19, 1, 6, 'PALLANCHACRA'),
(1678, 19, 1, 7, 'PAUCARTAMBO'),
(1679, 19, 1, 8, 'SAN FRANCISCO DE ASIS DE YARUSYACAN'),
(1680, 19, 1, 9, 'SIMON BOLIVAR'),
(1681, 19, 1, 10, 'TICLACAYAN'),
(1682, 19, 1, 11, 'TINYAHUARCO'),
(1683, 19, 1, 12, 'VICCO'),
(1684, 19, 1, 13, 'YANACANCHA'),
(1685, 19, 2, 0, 'DANIEL ALCIDES CARRION'),
(1686, 19, 2, 1, 'YANAHUANCA'),
(1687, 19, 2, 2, 'CHACAYAN'),
(1688, 19, 2, 3, 'GOYLLARISQUIZGA'),
(1689, 19, 2, 4, 'PAUCAR'),
(1690, 19, 2, 5, 'SAN PEDRO DE PILLAO'),
(1691, 19, 2, 6, 'SANTA ANA DE TUSI'),
(1692, 19, 2, 7, 'TAPUC'),
(1693, 19, 2, 8, 'VILCABAMBA'),
(1694, 19, 3, 0, 'OXAPAMPA'),
(1695, 19, 3, 1, 'OXAPAMPA'),
(1696, 19, 3, 2, 'CHONTABAMBA'),
(1697, 19, 3, 3, 'HUANCABAMBA'),
(1698, 19, 3, 4, 'PALCAZU'),
(1699, 19, 3, 5, 'POZUZO'),
(1700, 19, 3, 6, 'PUERTO BERMUDEZ'),
(1701, 19, 3, 7, 'VILLA RICA'),
(1702, 20, 0, 0, 'PIURA'),
(1703, 20, 1, 0, 'PIURA'),
(1704, 20, 1, 1, 'PIURA'),
(1705, 20, 1, 4, 'CASTILLA'),
(1706, 20, 1, 5, 'CATACAOS'),
(1707, 20, 1, 7, 'CURA MORI'),
(1708, 20, 1, 8, 'EL TALLAN'),
(1709, 20, 1, 9, 'LA ARENA'),
(1710, 20, 1, 10, 'LA UNION'),
(1711, 20, 1, 11, 'LAS LOMAS'),
(1712, 20, 1, 14, 'TAMBO GRANDE'),
(1713, 20, 2, 0, 'AYABACA'),
(1714, 20, 2, 1, 'AYABACA'),
(1715, 20, 2, 2, 'FRIAS'),
(1716, 20, 2, 3, 'JILILI'),
(1717, 20, 2, 4, 'LAGUNAS'),
(1718, 20, 2, 5, 'MONTERO'),
(1719, 20, 2, 6, 'PACAIPAMPA'),
(1720, 20, 2, 7, 'PAIMAS'),
(1721, 20, 2, 8, 'SAPILLICA'),
(1722, 20, 2, 9, 'SICCHEZ'),
(1723, 20, 2, 10, 'SUYO'),
(1724, 20, 3, 0, 'HUANCABAMBA'),
(1725, 20, 3, 1, 'HUANCABAMBA'),
(1726, 20, 3, 2, 'CANCHAQUE'),
(1727, 20, 3, 3, 'EL CARMEN DE LA FRONTERA'),
(1728, 20, 3, 4, 'HUARMACA'),
(1729, 20, 3, 5, 'LALAQUIZ'),
(1730, 20, 3, 6, 'SAN MIGUEL DE EL FAIQUE'),
(1731, 20, 3, 7, 'SONDOR'),
(1732, 20, 3, 8, 'SONDORILLO'),
(1733, 20, 4, 0, 'MORROPON'),
(1734, 20, 4, 1, 'CHULUCANAS'),
(1735, 20, 4, 2, 'BUENOS AIRES'),
(1736, 20, 4, 3, 'CHALACO'),
(1737, 20, 4, 4, 'LA MATANZA'),
(1738, 20, 4, 5, 'MORROPON'),
(1739, 20, 4, 6, 'SALITRAL'),
(1740, 20, 4, 7, 'SAN JUAN DE BIGOTE'),
(1741, 20, 4, 8, 'SANTA CATALINA DE MOSSA'),
(1742, 20, 4, 9, 'SANTO DOMINGO'),
(1743, 20, 4, 10, 'YAMANGO'),
(1744, 20, 5, 0, 'PAITA'),
(1745, 20, 5, 1, 'PAITA'),
(1746, 20, 5, 2, 'AMOTAPE'),
(1747, 20, 5, 3, 'ARENAL'),
(1748, 20, 5, 4, 'COLAN'),
(1749, 20, 5, 5, 'LA HUACA'),
(1750, 20, 5, 6, 'TAMARINDO'),
(1751, 20, 5, 7, 'VICHAYAL'),
(1752, 20, 6, 0, 'SULLANA'),
(1753, 20, 6, 1, 'SULLANA'),
(1754, 20, 6, 2, 'BELLAVISTA'),
(1755, 20, 6, 3, 'IGNACIO ESCUDERO'),
(1756, 20, 6, 4, 'LANCONES'),
(1757, 20, 6, 5, 'MARCAVELICA'),
(1758, 20, 6, 6, 'MIGUEL CHECA');
INSERT INTO `ubigeo` (`ubigeo_id`, `dDepartamento`, `codProvincia`, `codDistrito`, `Descripcion`) VALUES
(1759, 20, 6, 7, 'QUERECOTILLO'),
(1760, 20, 6, 8, 'SALITRAL'),
(1761, 20, 7, 0, 'TALARA'),
(1762, 20, 7, 1, 'PARIÑAS'),
(1763, 20, 7, 2, 'EL ALTO'),
(1764, 20, 7, 3, 'LA BREA'),
(1765, 20, 7, 4, 'LOBITOS'),
(1766, 20, 7, 5, 'LOS ORGANOS'),
(1767, 20, 7, 6, 'MANCORA'),
(1768, 20, 8, 0, 'SECHURA'),
(1769, 20, 8, 1, 'SECHURA'),
(1770, 20, 8, 2, 'BELLAVISTA DE LA UNION'),
(1771, 20, 8, 3, 'BERNAL'),
(1772, 20, 8, 4, 'CRISTO NOS VALGA'),
(1773, 20, 8, 5, 'VICE'),
(1774, 20, 8, 6, 'RINCONADA LLICUAR'),
(1775, 21, 0, 0, 'PUNO'),
(1776, 21, 1, 0, 'PUNO'),
(1777, 21, 1, 1, 'PUNO'),
(1778, 21, 1, 2, 'ACORA'),
(1779, 21, 1, 3, 'AMANTANI'),
(1780, 21, 1, 4, 'ATUNCOLLA'),
(1781, 21, 1, 5, 'CAPACHICA'),
(1782, 21, 1, 6, 'CHUCUITO'),
(1783, 21, 1, 7, 'COATA'),
(1784, 21, 1, 8, 'HUATA'),
(1785, 21, 1, 9, 'MAÑAZO'),
(1786, 21, 1, 10, 'PAUCARCOLLA'),
(1787, 21, 1, 11, 'PICHACANI'),
(1788, 21, 1, 12, 'PLATERIA'),
(1789, 21, 1, 13, 'SAN ANTONIO'),
(1790, 21, 1, 14, 'TIQUILLACA'),
(1791, 21, 1, 15, 'VILQUE'),
(1792, 21, 2, 0, 'AZANGARO'),
(1793, 21, 2, 1, 'AZANGARO'),
(1794, 21, 2, 2, 'ACHAYA'),
(1795, 21, 2, 3, 'ARAPA'),
(1796, 21, 2, 4, 'ASILLO'),
(1797, 21, 2, 5, 'CAMINACA'),
(1798, 21, 2, 6, 'CHUPA'),
(1799, 21, 2, 7, 'JOSE DOMINGO CHOQUEHUANCA'),
(1800, 21, 2, 8, 'MUÑANI'),
(1801, 21, 2, 9, 'POTONI'),
(1802, 21, 2, 10, 'SAMAN'),
(1803, 21, 2, 11, 'SAN ANTON'),
(1804, 21, 2, 12, 'SAN JOSE'),
(1805, 21, 2, 13, 'SAN JUAN DE SALINAS'),
(1806, 21, 2, 14, 'SANTIAGO DE PUPUJA'),
(1807, 21, 2, 15, 'TIRAPATA'),
(1808, 21, 3, 0, 'CARABAYA'),
(1809, 21, 3, 1, 'MACUSANI'),
(1810, 21, 3, 2, 'AJOYANI'),
(1811, 21, 3, 3, 'AYAPATA'),
(1812, 21, 3, 4, 'COASA'),
(1813, 21, 3, 5, 'CORANI'),
(1814, 21, 3, 6, 'CRUCERO'),
(1815, 21, 3, 7, 'ITUATA'),
(1816, 21, 3, 8, 'OLLACHEA'),
(1817, 21, 3, 9, 'SAN GABAN'),
(1818, 21, 3, 10, 'USICAYOS'),
(1819, 21, 4, 0, 'CHUCUITO'),
(1820, 21, 4, 1, 'JULI'),
(1821, 21, 4, 2, 'DESAGUADERO'),
(1822, 21, 4, 3, 'HUACULLANI'),
(1823, 21, 4, 4, 'KELLUYO'),
(1824, 21, 4, 5, 'PISACOMA'),
(1825, 21, 4, 6, 'POMATA'),
(1826, 21, 4, 7, 'ZEPITA'),
(1827, 21, 5, 0, 'EL COLLAO'),
(1828, 21, 5, 1, 'ILAVE'),
(1829, 21, 5, 2, 'CAPAZO'),
(1830, 21, 5, 3, 'PILCUYO'),
(1831, 21, 5, 4, 'SANTA ROSA'),
(1832, 21, 5, 5, 'CONDURIRI'),
(1833, 21, 6, 0, 'HUANCANE'),
(1834, 21, 6, 1, 'HUANCANE'),
(1835, 21, 6, 2, 'COJATA'),
(1836, 21, 6, 3, 'HUATASANI'),
(1837, 21, 6, 4, 'INCHUPALLA'),
(1838, 21, 6, 5, 'PUSI'),
(1839, 21, 6, 6, 'ROSASPATA'),
(1840, 21, 6, 7, 'TARACO'),
(1841, 21, 6, 8, 'VILQUE CHICO'),
(1842, 21, 7, 0, 'LAMPA'),
(1843, 21, 7, 1, 'LAMPA'),
(1844, 21, 7, 2, 'CABANILLA'),
(1845, 21, 7, 3, 'CALAPUJA'),
(1846, 21, 7, 4, 'NICASIO'),
(1847, 21, 7, 5, 'OCUVIRI'),
(1848, 21, 7, 6, 'PALCA'),
(1849, 21, 7, 7, 'PARATIA'),
(1850, 21, 7, 8, 'PUCARA'),
(1851, 21, 7, 9, 'SANTA LUCIA'),
(1852, 21, 7, 10, 'VILAVILA'),
(1853, 21, 8, 0, 'MELGAR'),
(1854, 21, 8, 1, 'AYAVIRI'),
(1855, 21, 8, 2, 'ANTAUTA'),
(1856, 21, 8, 3, 'CUPI'),
(1857, 21, 8, 4, 'LLALLI'),
(1858, 21, 8, 5, 'MACARI'),
(1859, 21, 8, 6, 'NUÑOA'),
(1860, 21, 8, 7, 'ORURILLO'),
(1861, 21, 8, 8, 'SANTA ROSA'),
(1862, 21, 8, 9, 'UMACHIRI'),
(1863, 21, 9, 0, 'MOHO'),
(1864, 21, 9, 1, 'MOHO'),
(1865, 21, 9, 2, 'CONIMA'),
(1866, 21, 9, 3, 'HUAYRAPATA'),
(1867, 21, 9, 4, 'TILALI'),
(1868, 21, 10, 0, 'SAN ANTONIO DE PUTINA'),
(1869, 21, 10, 1, 'PUTINA'),
(1870, 21, 10, 2, 'ANANEA'),
(1871, 21, 10, 3, 'PEDRO VILCA APAZA'),
(1872, 21, 10, 4, 'QUILCAPUNCU'),
(1873, 21, 10, 5, 'SINA'),
(1874, 21, 11, 0, 'SAN ROMAN'),
(1875, 21, 11, 1, 'JULIACA'),
(1876, 21, 11, 2, 'CABANA'),
(1877, 21, 11, 3, 'CABANILLAS'),
(1878, 21, 11, 4, 'CARACOTO'),
(1879, 21, 12, 0, 'SANDIA'),
(1880, 21, 12, 1, 'SANDIA'),
(1881, 21, 12, 2, 'CUYOCUYO'),
(1882, 21, 12, 3, 'LIMBANI'),
(1883, 21, 12, 4, 'PATAMBUCO'),
(1884, 21, 12, 5, 'PHARA'),
(1885, 21, 12, 6, 'QUIACA'),
(1886, 21, 12, 7, 'SAN JUAN DEL ORO'),
(1887, 21, 12, 8, 'YANAHUAYA'),
(1888, 21, 12, 9, 'ALTO INAMBARI'),
(1889, 21, 12, 10, 'SAN PEDRO DE PUTINA PUNCO'),
(1890, 21, 13, 0, 'YUNGUYO'),
(1891, 21, 13, 1, 'YUNGUYO'),
(1892, 21, 13, 2, 'ANAPIA'),
(1893, 21, 13, 3, 'COPANI'),
(1894, 21, 13, 4, 'CUTURAPI'),
(1895, 21, 13, 5, 'OLLARAYA'),
(1896, 21, 13, 6, 'TINICACHI'),
(1897, 21, 13, 7, 'UNICACHI'),
(1898, 22, 0, 0, 'SAN MARTIN'),
(1899, 22, 1, 0, 'MOYOBAMBA'),
(1900, 22, 1, 1, 'MOYOBAMBA'),
(1901, 22, 1, 2, 'CALZADA'),
(1902, 22, 1, 3, 'HABANA'),
(1903, 22, 1, 4, 'JEPELACIO'),
(1904, 22, 1, 5, 'SORITOR'),
(1905, 22, 1, 6, 'YANTALO'),
(1906, 22, 2, 0, 'BELLAVISTA'),
(1907, 22, 2, 1, 'BELLAVISTA'),
(1908, 22, 2, 2, 'ALTO BIAVO'),
(1909, 22, 2, 3, 'BAJO BIAVO'),
(1910, 22, 2, 4, 'HUALLAGA'),
(1911, 22, 2, 5, 'SAN PABLO'),
(1912, 22, 2, 6, 'SAN RAFAEL'),
(1913, 22, 3, 0, 'EL DORADO'),
(1914, 22, 3, 1, 'SAN JOSE DE SISA'),
(1915, 22, 3, 2, 'AGUA BLANCA'),
(1916, 22, 3, 3, 'SAN MARTIN'),
(1917, 22, 3, 4, 'SANTA ROSA'),
(1918, 22, 3, 5, 'SHATOJA'),
(1919, 22, 4, 0, 'HUALLAGA'),
(1920, 22, 4, 1, 'SAPOSOA'),
(1921, 22, 4, 2, 'ALTO SAPOSOA'),
(1922, 22, 4, 3, 'EL ESLABON'),
(1923, 22, 4, 4, 'PISCOYACU'),
(1924, 22, 4, 5, 'SACANCHE'),
(1925, 22, 4, 6, 'TINGO DE SAPOSOA'),
(1926, 22, 5, 0, 'LAMAS'),
(1927, 22, 5, 1, 'LAMAS'),
(1928, 22, 5, 2, 'ALONSO DE ALVARADO'),
(1929, 22, 5, 3, 'BARRANQUITA'),
(1930, 22, 5, 4, 'CAYNARACHI'),
(1931, 22, 5, 5, 'CUÑUMBUQUI'),
(1932, 22, 5, 6, 'PINTO RECODO'),
(1933, 22, 5, 7, 'RUMISAPA'),
(1934, 22, 5, 8, 'SAN ROQUE DE CUMBAZA'),
(1935, 22, 5, 9, 'SHANAO'),
(1936, 22, 5, 10, 'TABALOSOS'),
(1937, 22, 5, 11, 'ZAPATERO'),
(1938, 22, 6, 0, 'MARISCAL CACERES'),
(1939, 22, 6, 1, 'JUANJUI'),
(1940, 22, 6, 2, 'CAMPANILLA'),
(1941, 22, 6, 3, 'HUICUNGO'),
(1942, 22, 6, 4, 'PACHIZA'),
(1943, 22, 6, 5, 'PAJARILLO'),
(1944, 22, 7, 0, 'PICOTA'),
(1945, 22, 7, 1, 'PICOTA'),
(1946, 22, 7, 2, 'BUENOS AIRES'),
(1947, 22, 7, 3, 'CASPISAPA'),
(1948, 22, 7, 4, 'PILLUANA'),
(1949, 22, 7, 5, 'PUCACACA'),
(1950, 22, 7, 6, 'SAN CRISTOBAL'),
(1951, 22, 7, 7, 'SAN HILARION'),
(1952, 22, 7, 8, 'SHAMBOYACU'),
(1953, 22, 7, 9, 'TINGO DE PONASA'),
(1954, 22, 7, 10, 'TRES UNIDOS'),
(1955, 22, 8, 0, 'RIOJA'),
(1956, 22, 8, 1, 'RIOJA'),
(1957, 22, 8, 2, 'AWAJUN'),
(1958, 22, 8, 3, 'ELIAS SOPLIN VARGAS'),
(1959, 22, 8, 4, 'NUEVA CAJAMARCA'),
(1960, 22, 8, 5, 'PARDO MIGUEL'),
(1961, 22, 8, 6, 'POSIC'),
(1962, 22, 8, 7, 'SAN FERNANDO'),
(1963, 22, 8, 8, 'YORONGOS'),
(1964, 22, 8, 9, 'YURACYACU'),
(1965, 22, 9, 0, 'SAN MARTIN'),
(1966, 22, 9, 1, 'TARAPOTO'),
(1967, 22, 9, 2, 'ALBERTO LEVEAU'),
(1968, 22, 9, 3, 'CACATACHI'),
(1969, 22, 9, 4, 'CHAZUTA'),
(1970, 22, 9, 5, 'CHIPURANA'),
(1971, 22, 9, 6, 'EL PORVENIR'),
(1972, 22, 9, 7, 'HUIMBAYOC'),
(1973, 22, 9, 8, 'JUAN GUERRA'),
(1974, 22, 9, 9, 'LA BANDA DE SHILCAYO'),
(1975, 22, 9, 10, 'MORALES'),
(1976, 22, 9, 11, 'PAPAPLAYA'),
(1977, 22, 9, 12, 'SAN ANTONIO'),
(1978, 22, 9, 13, 'SAUCE'),
(1979, 22, 9, 14, 'SHAPAJA'),
(1980, 22, 10, 0, 'TOCACHE'),
(1981, 22, 10, 1, 'TOCACHE'),
(1982, 22, 10, 2, 'NUEVO PROGRESO'),
(1983, 22, 10, 3, 'POLVORA'),
(1984, 22, 10, 4, 'SHUNTE'),
(1985, 22, 10, 5, 'UCHIZA'),
(1986, 23, 0, 0, 'TACNA'),
(1987, 23, 1, 0, 'TACNA'),
(1988, 23, 1, 1, 'TACNA'),
(1989, 23, 1, 2, 'ALTO DE LA ALIANZA'),
(1990, 23, 1, 3, 'CALANA'),
(1991, 23, 1, 4, 'CIUDAD NUEVA'),
(1992, 23, 1, 5, 'INCLAN'),
(1993, 23, 1, 6, 'PACHIA'),
(1994, 23, 1, 7, 'PALCA'),
(1995, 23, 1, 8, 'POCOLLAY'),
(1996, 23, 1, 9, 'SAMA'),
(1997, 23, 1, 10, 'CORONEL GREGORIO ALBARRACIN LANCHIPA'),
(1998, 23, 2, 0, 'CANDARAVE'),
(1999, 23, 2, 1, 'CANDARAVE'),
(2000, 23, 2, 2, 'CAIRANI'),
(2001, 23, 2, 3, 'CAMILACA'),
(2002, 23, 2, 4, 'CURIBAYA'),
(2003, 23, 2, 5, 'HUANUARA'),
(2004, 23, 2, 6, 'QUILAHUANI'),
(2005, 23, 3, 0, 'JORGE BASADRE'),
(2006, 23, 3, 1, 'LOCUMBA'),
(2007, 23, 3, 2, 'ILABAYA'),
(2008, 23, 3, 3, 'ITE'),
(2009, 23, 4, 0, 'TARATA'),
(2010, 23, 4, 1, 'TARATA'),
(2011, 23, 4, 2, 'HEROES ALBARRACIN'),
(2012, 23, 4, 3, 'ESTIQUE'),
(2013, 23, 4, 4, 'ESTIQUE-PAMPA'),
(2014, 23, 4, 5, 'SITAJARA'),
(2015, 23, 4, 6, 'SUSAPAYA'),
(2016, 23, 4, 7, 'TARUCACHI'),
(2017, 23, 4, 8, 'TICACO'),
(2018, 24, 0, 0, 'TUMBES'),
(2019, 24, 1, 0, 'TUMBES'),
(2020, 24, 1, 1, 'TUMBES'),
(2021, 24, 1, 2, 'CORRALES'),
(2022, 24, 1, 3, 'LA CRUZ'),
(2023, 24, 1, 4, 'PAMPAS DE HOSPITAL'),
(2024, 24, 1, 5, 'SAN JACINTO'),
(2025, 24, 1, 6, 'SAN JUAN DE LA VIRGEN'),
(2026, 24, 2, 0, 'CONTRALMIRANTE VILLAR'),
(2027, 24, 2, 1, 'ZORRITOS'),
(2028, 24, 2, 2, 'CASITAS'),
(2029, 24, 2, 3, 'CANOAS DE PUNTA SAL'),
(2030, 24, 3, 0, 'ZARUMILLA'),
(2031, 24, 3, 1, 'ZARUMILLA'),
(2032, 24, 3, 2, 'AGUAS VERDES'),
(2033, 24, 3, 3, 'MATAPALO'),
(2034, 24, 3, 4, 'PAPAYAL'),
(2035, 25, 0, 0, 'UCAYALI'),
(2036, 25, 1, 0, 'CORONEL PORTILLO'),
(2037, 25, 1, 1, 'CALLERIA'),
(2038, 25, 1, 2, 'CAMPOVERDE'),
(2039, 25, 1, 3, 'IPARIA'),
(2040, 25, 1, 4, 'MASISEA'),
(2041, 25, 1, 5, 'YARINACOCHA'),
(2042, 25, 1, 6, 'NUEVA REQUENA'),
(2043, 25, 1, 7, 'MANANTAY'),
(2044, 25, 2, 0, 'ATALAYA'),
(2045, 25, 2, 1, 'RAYMONDI'),
(2046, 25, 2, 2, 'SEPAHUA'),
(2047, 25, 2, 3, 'TAHUANIA'),
(2048, 25, 2, 4, 'YURUA'),
(2049, 25, 3, 0, 'PADRE ABAD'),
(2050, 25, 3, 1, 'PADRE ABAD'),
(2051, 25, 3, 2, 'IRAZOLA'),
(2052, 25, 3, 3, 'CURIMANA'),
(2053, 25, 4, 0, 'PURUS'),
(2054, 25, 4, 1, 'PURUS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(36) NOT NULL,
  `firstname` varchar(40) DEFAULT NULL,
  `lastname` varchar(40) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `usersalt` varchar(8) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `userlevel` tinyint(1) UNSIGNED DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `timestamp` varchar(25) DEFAULT NULL,
  `previous_visit` varchar(25) DEFAULT NULL,
  `actkey` varchar(35) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `regdate` tinyint(3) UNSIGNED DEFAULT NULL,
  `lastip` varchar(15) DEFAULT NULL,
  `user_login_attempts` tinyint(4) DEFAULT NULL,
  `user_home_path` varchar(50) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `tiposdocumento_id` int(255) DEFAULT NULL,
  `nrodocumento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais_id` int(255) DEFAULT NULL,
  `primernombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `segundonombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primeroapellido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `segundoapellido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecnacimiento` date DEFAULT NULL,
  `paisdireccion_id` int(255) DEFAULT NULL,
  `departamento_id` int(255) DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `distrito_id` int(255) DEFAULT NULL,
  `Direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ocupacion_id` int(255) DEFAULT NULL,
  `personaexpuesta` tinyint(1) DEFAULT NULL,
  `personal` int(11) DEFAULT NULL,
  `empresa` int(11) DEFAULT NULL,
  `ruc` bigint(20) DEFAULT NULL,
  `razon_social` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `giro_negocio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo_electronico` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primernombre_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `segundonombre_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primerapellido_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `segundoapellido_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiposdocumentoc_id` int(11) DEFAULT NULL,
  `nrodocumento_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `active_guests`
--
ALTER TABLE `active_guests`
  ADD PRIMARY KEY (`ip`);

--
-- Indices de la tabla `active_users`
--
ALTER TABLE `active_users`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`banco_id`);

--
-- Indices de la tabla `banlist`
--
ALTER TABLE `banlist`
  ADD PRIMARY KEY (`ban_id`);

--
-- Indices de la tabla `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`config_name`);

--
-- Indices de la tabla `cuentabancaria`
--
ALTER TABLE `cuentabancaria`
  ADD PRIMARY KEY (`cuentabancaria_id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`departamento_id`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`distrito_id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indices de la tabla `log_table`
--
ALTER TABLE `log_table`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `mes`
--
ALTER TABLE `mes`
  ADD PRIMARY KEY (`mes_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`moneda_id`);

--
-- Indices de la tabla `ocupacion`
--
ALTER TABLE `ocupacion`
  ADD PRIMARY KEY (`ocupacion_id`);

--
-- Indices de la tabla `operacion`
--
ALTER TABLE `operacion`
  ADD PRIMARY KEY (`operacion_id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`pais_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(250));

--
-- Indices de la tabla `tipocambio`
--
ALTER TABLE `tipocambio`
  ADD PRIMARY KEY (`tipocambio_id`);

--
-- Indices de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  ADD PRIMARY KEY (`tipocuenta_id`);

--
-- Indices de la tabla `tiposdocumento`
--
ALTER TABLE `tiposdocumento`
  ADD PRIMARY KEY (`tiposdocumento_id`);

--
-- Indices de la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  ADD PRIMARY KEY (`ubigeo_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `usuario_id` (`usuario_id`) USING BTREE;

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `banco_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `banlist`
--
ALTER TABLE `banlist`
  MODIFY `ban_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentabancaria`
--
ALTER TABLE `cuentabancaria`
  MODIFY `cuentabancaria_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `departamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `distrito`
--
ALTER TABLE `distrito`
  MODIFY `distrito_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `log_table`
--
ALTER TABLE `log_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mes`
--
ALTER TABLE `mes`
  MODIFY `mes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `moneda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ocupacion`
--
ALTER TABLE `ocupacion`
  MODIFY `ocupacion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `operacion`
--
ALTER TABLE `operacion`
  MODIFY `operacion_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `pais_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipocambio`
--
ALTER TABLE `tipocambio`
  MODIFY `tipocambio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  MODIFY `tipocuenta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tiposdocumento`
--
ALTER TABLE `tiposdocumento`
  MODIFY `tiposdocumento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  MODIFY `ubigeo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2055;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
